import chai from 'chai'

import { Membership, UserPayment } from './data'
import { membershipRepository } from '../../src/data/repositories'

let membershipId

const should = chai.should()

describe('App: Memberships', () => {
  describe('Memberships 1', () => {
    it('debe registrar una membresia', (done) => {
      membershipRepository.saveMembership(Membership, (err, result) => {
        Membership.should.be.deep.equal({
          checkin: result.checkin,
          checkout: result.checkout,
          dataName: result.dataName,
          durationInDays: result.durationInDays,
          fkIdGym: result.fkIdGym,
          price: result.price,
          status: result.status,
        })
        membershipId = result._id
        done()
      })
    })

    it('debe consultar una membresia', (done) => {
      membershipRepository.getMembership(membershipId, (err, result) => {
        result.should.be.a('object')
        done()
      })
    })

    it('debe listar todas las membresias de un gimnasio', (done) => {
      membershipRepository.getMembershipsOfGym(Membership.fkIdGym, (err, result) => {
        should.equal(result.length, 1)
        done()
      })
    })

    it('debe registrar un pago', (done) => {
      membershipRepository.saveUserPayment(UserPayment, (err, result) => {
        UserPayment.should.be.deep.equal({
          amount: result.amount,
          fkIdUser: result.fkIdUser,
          fkIdMembership: result.fkIdMembership,
          paymentType: result.paymentType,
          reference: result.reference,
          status: result.status
        })
        done()
      })
    })

    it('debe listar todos los pagos de un usuario', (done) => {
      membershipRepository.getPaymentsOfUser(UserPayment.fkIdUser, (err, result) => {
        should.equal(result.length, 1)
        done()
      })
    })

    it('debe actualizar una membresia', (done) => {
      membershipRepository.updateMembership(membershipId, { dataName: 'Membership Updated' }, (err, result) => {
        Membership.dataName = 'Membership Updated'
        Membership.should.be.deep.equal({
          checkin: result.checkin,
          checkout: result.checkout,
          dataName: result.dataName,
          durationInDays: result.durationInDays,
          fkIdGym: result.fkIdGym,
          price: result.price,
          status: result.status,
        })
        done()
      })
    })

    it('debe eliminar una membresia', (done) => {
      membershipRepository.deleteMembership(membershipId, (err, result) => {
        should.equal(err, null)
        membershipRepository.getMembershipsOfGym(Membership.fkIdGym, (err2, result2) => {
          should.equal(result2.length, 0)
          done()
        })
      })
    })
  })
})
