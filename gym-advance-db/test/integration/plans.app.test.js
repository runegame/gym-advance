import chai from 'chai'

import { Plan, GymPayment } from './data'
import { planRepository } from '../../src/data/repositories'

const should = chai.should()

let planId
let gymPaymentId

describe('App: Plans', () => {
  describe('General', () => {
    it('debe registrar un plan', (done) => {
      planRepository.savePlan(Plan, (err, result) => {

        Plan.should.be.deep.equal({
          dataName: result.dataName,
          description: result.description,
          durationInDays: result.durationInDays,
          price: result.price,
          status: result.status,
          urlPicture: result.urlPicture,
        })

        planId = result._id

        done()
      })
    })

    it('debe consultar un plan', (done) => {
      planRepository.getPlan(planId, (err, result) => {
        result.should.be.a('object')
        done()
      })
    })

    it('debe listar todos los planes', (done) => {
      planRepository.getPlans((err, result) => {
        should.equal(result.length, 1)
        done()
      })
    })

    it('debe registrar un pago', (done) => {
      planRepository.saveGymPayment(GymPayment, (err, result) => {
        GymPayment.should.be.deep.equal({
          amount: result.amount,
          fkIdGym: result.fkIdGym,
          fkIdPlan: result.fkIdPlan,
          paymentType: result.paymentType,
          reference: result.reference,
          status: result.status
        })

        gymPaymentId = result._id
        done()
      })
    })

    it('debe listar todos los pagos de un gimnasios', (done) => {
      planRepository.getPaymentsOfGym(GymPayment.fkIdGym, (err, result) => {
        should.equal(result.length, 1)
        done()
      })
    })

    it('debe listar todos los pagos de planes', (done) => {
      planRepository.listGymPayments((err, result) => {
        should.equal(result.length, 1)
        done()
      })
    })

    it('debe actualizar un plan', (done) => {
      planRepository.updatePlan(planId, { dataName: 'Plan Updated' }, (err, result) => {
        Plan.dataName = 'Plan Updated'
        Plan.should.be.deep.equal({
          dataName: result.dataName,
          description: result.description,
          durationInDays: result.durationInDays,
          price: result.price,
          status: result.status,
          urlPicture: result.urlPicture
        })
        done()
      })
    })

    it('debe eliminar un plan', (done) => {
      planRepository.deletePlan(planId, (err, result) => {
        should.equal(err, null)
        planRepository.getPlans((err2, result2) => {
          should.equal(result2.length, 0)
          done()
        })
      })
    })
  })
})
