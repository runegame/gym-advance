import chai from 'chai'

import { Gym } from './data'
import { gymRepository } from '../../src/data/repositories'

let gymId

const should = chai.should()

describe('App: Gyms', () => {
  describe('Gym 1', () => {
    it('debe registrar un gimnasio', (done) => {
      gymRepository.saveGym(Gym, (err, result) => {
        Gym.should.be.deep.equal({
          capacity: result.capacity,
          dataName: result.dataName,
          email: result.email,
          expirationDay: result.expirationDay,
          fkIdPlan: result.fkIdPlan,
          location: result.location,
          main: result.main,
          phone: result.phone,
          status: result.status,
        })
        gymId = result._id
        done()
      })
    })

    it('debe consultar un gimnasio', (done) => {
      gymRepository.getGym(gymId, (err, result) => {
        result.should.be.a('object')
        done()
      })
    })

    it('debe listar todos los gimnasios', (done) => {
      gymRepository.getGyms((err, result) => {
        should.equal(result.length, 1)
        done()
      })
    })

    it('debe actualizar un gimnasio', (done) => {
      gymRepository.updateGym(gymId, { dataName: 'Gym Updated' }, (err, result) => {
        Gym.dataName = 'Gym Updated'
        Gym.should.be.deep.equal({
          capacity: result.capacity,
          dataName: result.dataName,
          email: result.email,
          expirationDay: result.expirationDay,
          fkIdPlan: result.fkIdPlan,
          location: result.location,
          main: result.main,
          phone: result.phone,
          status: result.status,
        })
        done()
      })
    })

    it('debe eliminar un gimnasio', (done) => {
      gymRepository.deleteGym(gymId, (err, result) => {
        should.equal(err, null)
        gymRepository.getGyms((err2, result2) => {
          should.equal(result2.length, 0)
          done()
        })
      })
    })
  })
})
