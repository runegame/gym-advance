/*
* Gym 1
*/
const Gym = {
  capacity: 100,
  dataName: 'Gimnasio de la Esquina',
  email: 'gimnasioEsquina@gmail.com',
  expirationDay: new Date().getTime(),
  fkIdPlan: '27d169e8-0349-11e7-a660-0800f46f34f3',
  location: 'Por aqui cerca de la esquina',
  main: true,
  phone: 2771234567,
  status: true,
}

/*
* Payment of Gym
*/
const GymPayment = {
  amount: 150,
  fkIdGym: 'f46f34f3-0349-11e7-a660-080027d169e8',
  fkIdPlan: '27d169e8-0349-11e7-a660-0800f46f34f3',
  paymentType: 'transfer',
  reference: 1234567,
  status: 'checked'
}

const Membership = {
  checkin: '06:00',
  checkout: '18:00',
  dataName: 'Diaria',
  durationInDays: 1,
  fkIdGym: 'f46f34f3-0349-11e7-a660-080027d169e8',
  price: 1.5,
  status: true,
}

/*
* Permisos
*/
const Permission = {
  dataName: 'module::Customer::access',
  roles: ['owner', 'admin'],
}

/*
* Plan 1
*/
const Plan = {
  dataName: 'Full',
  description: 'Plan completo por un año',
  durationInDays: 365,
  price: 150,
  status: true,
  urlPicture: '',
}

/*
* Roles
*/
const Role = {
  dataName: 'owner',
  description: 'Dueño del Gimnasio',
  scope: 'main',
}

const User = {
  birthday: new Date(),
  documentNumber: 22000111,
  documentType: 'DNI',
  email: 'test@gymadvance.com',
  emergencyPhone: 41278945612,
  fkIdGym: 'f46f34f3-0349-11e7-a660-080027d169e8',
  fkIdMembership: '27d169e8-0349-11e7-a660-0800f46f34f3',
  fullname: 'Gym Advance Fullname',
  haveInsurancePolicy: false,
  mainPhone: 41278945612,
  membershipExpiration: new Date(),
  observations: 'Ninguna',
  password: '12346',
  residenceAddress: 'El cliente de la esquina',
  role: 'user',
  status: true,
  username: 'gymadvance',
  workAddress: 'El cliente que trabaja en la esquina',
}

/*
* Payment of User
*/
const UserPayment = {
  amount: 150,
  fkIdUser: 'f46f34f3-0349-11e7-a660-69e8080027d1',
  fkIdMembership: '27d169e8-0349-11e7-a660-0800f46f34f3',
  paymentType: 'transfer',
  reference: 1234567,
  status: 'checked'
}

export {
  Gym, GymPayment, Membership, Permission, Plan, Role, User, UserPayment
}
