import chai from 'chai'

import { Role, Permission } from './data'
import { roleRepository, permissionRepository } from '../../src/data/repositories'

const should = chai.should()

let roleId

describe('App: Roles', () => {
  describe('General', () => {
    it('debe registrar los roles', (done) => {
      roleRepository.saveRole(Role, (err, result) => {
        Role.should.be.deep.equal({
          dataName: result.dataName,
          description: result.description,
          scope: result.scope,
        })
        roleId = result._id
        done()
      })
    })

    it('debe consultar un role', (done) => {
      roleRepository.getRole(roleId, (err, result) => {
        result.should.be.a('object')
        done()
      })
    })

    it('debe listar todos los roles', (done) => {
      roleRepository.getRoles((err, result) => {
        should.equal(result.length, 1)
        done()
      })
    })

    it('debe listar todos los permiso de un role', (done) => {
      permissionRepository.savePermission(Permission, (err, result1) => {
        roleRepository.getPermissionsOfRole(roleId, (err, result2) => {
          should.equal(result2.permissions.length, 1)
          done()
        })
      })
    })

    it('debe actualizar un role', (done) => {
      roleRepository.updateRole(roleId, { dataName: 'role_updated' }, (err, result) => {
        Role.dataName = 'role_updated'
        Role.should.be.deep.equal({
          dataName: result.dataName,
          description: result.description,
          scope: result.scope,
        })
        done()
      })
    })

    it('debe eliminar un role', (done) => {
      roleRepository.deleteRole(roleId, (err, result) => {
        should.equal(err, null)
        roleRepository.getRoles((err2, result2) => {
          should.equal(result2.length, 0)
          done()
        })
      })
    })
  })
})
