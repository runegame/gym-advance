import chai from 'chai'
import _ from 'underscore'

import { User } from './data'
import { userRepository } from '../../src/data/repositories'

let userId

const should = chai.should()

describe('App: Users', () => {
  describe('Users 1', () => {
    it('debe registrar un usuario', (done) => {
      userRepository.saveUser(User, (err, result) => {
        userId = result._id

        const tempUser = _.omit(User, 'password')

        tempUser.should.be.deep.equal({
          birthday: result.birthday,
          documentNumber: result.documentNumber,
          documentType: result.documentType,
          email: result.email,
          emergencyPhone: result.emergencyPhone,
          fkIdGym: result.fkIdGym,
          fkIdMembership: result.fkIdMembership,
          fullname: result.fullname,
          haveInsurancePolicy: result.haveInsurancePolicy,
          mainPhone: result.mainPhone,
          membershipExpiration: result.membershipExpiration,
          observations: result.observations,
          residenceAddress: result.residenceAddress,
          role: result.role,
          status: result.status,
          username: result.username,
          workAddress: result.workAddress,
        })
        done()
      })
    })

    it('debe consultar un usuario', (done) => {
      userRepository.getUser(userId, (err, result) => {
        result.should.be.a('object')
        done()
      })
    })

    it('debe listar todos los usuarios del sistema', (done) => {
      userRepository.listUsers((err, result) => {
        should.equal(result.length, 1)
        done()
      })
    })

    it('debe listar todos los usuarios de un gimnasio', (done) => {
      userRepository.getUsersOfGym(User.fkIdGym, (err, result) => {
        should.equal(result.length, 1)
        done()
      })
    })

    it('debe actualizar un usuario', (done) => {
      userRepository.updateUser(userId, { fullname: 'User Updated' }, (err, result) => {

        const tempUser = _.omit(User, 'password')
        tempUser.fullname = 'User Updated'

        tempUser.should.be.deep.equal({
          birthday: result.birthday,
          documentNumber: result.documentNumber,
          documentType: result.documentType,
          email: result.email,
          emergencyPhone: result.emergencyPhone,
          fkIdGym: result.fkIdGym,
          fkIdMembership: result.fkIdMembership,
          fullname: result.fullname,
          haveInsurancePolicy: result.haveInsurancePolicy,
          mainPhone: result.mainPhone,
          membershipExpiration: result.membershipExpiration,
          observations: result.observations,
          residenceAddress: result.residenceAddress,
          role: result.role,
          status: result.status,
          username: result.username,
          workAddress: result.workAddress,
        })
        done()
      })
    })

    it('debe eliminar un usuario', (done) => {
      userRepository.deleteUser(userId, (err, result) => {
        should.equal(err, null)
        userRepository.getUsersOfGym(User.fkIdGym, (err2, result2) => {
          should.equal(result2.length, 0)
          done()
        })
      })
    })
  })
})
