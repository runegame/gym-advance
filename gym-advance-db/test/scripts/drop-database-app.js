import mongoose from 'mongoose'
import config from '../../src/config'

if (config.app.env !== 'development') {
  throw new Error('Entorno de Produccion! Cancelado!')
}

mongoose.connect(config.app.mongo_url, {
  useMongoClient: true,
})

mongoose.connection.on('error', () => console.log(`Error al conectar a: ${config.app.mongo_url}`))

mongoose.connection.on('open', () => {
  console.log(`Conexión establecida a: ${config.app.mongo_url}`)

  mongoose.connection.db.dropDatabase((err) => {
    if (err) {
      console.log(err)
    }

    mongoose.connection.close(() => console.log('Base de datos eliminada'))
  })
})
