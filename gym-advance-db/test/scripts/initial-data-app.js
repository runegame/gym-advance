import mongoose from 'mongoose'
import moment from 'moment'

import config from '../../src/config'

import roleRepository from '../../src/data/repositories/role'
import permissionRepository from '../../src/data/repositories/permission'
import planRepository from '../../src/data/repositories/plan'
import gymRepository from '../../src/data/repositories/gym'
import userRepository from '../../src/data/repositories/user'

mongoose.connect(config.app.mongo_url, {
  useMongoClient: true,
})

mongoose.connection.on('error', () => console.log(`Error al conectar a: ${config.app.mongo_url}`))

mongoose.connection.on('open', async () => {
  console.log(`Conexión establecida a: ${config.app.mongo_url}`)
  console.log('Iniciando minado de la base de datos...')

  // Declaracion de los Roles
  const roles = [
    {
      dataName: 'admin',
      description: 'Administrador global del sistema',
      scope: 'Global',
    },
    {
      dataName: 'owner',
      description: 'Dueño de un gimnasio',
      scope: 'Gym',
    },
    {
      dataName: 'employed',
      description: 'Empleado de un gimnasio',
      scope: 'Gym',
    },
    {
      dataName: 'customer',
      description: 'Cliente de un gimnasio',
      scope: 'Gym',
    },
  ]
  // Declaracion de los Permisos
  const permissions = [
    // Modulo admin
    { dataName: 'moduleAdmin::signin', roles: ['admin'] },
    // Modulo owner
    { dataName: 'moduleOwner::signin', roles: ['owner', 'employed'] },
    // Modulo access
    { dataName: 'moduleAccess::access', roles: ['customer']},
    // Usuarios
    { dataName: 'moduleUsers::Create', roles: ['admin'] },
    { dataName: 'moduleUsers::Read', roles: ['admin'] },
    { dataName: 'moduleUsers::Update', roles: ['admin'] },
    { dataName: 'moduleUsers::Delete', roles: ['admin'] },
    { dataName: 'moduleUsers::showAllRoles', roles:['admin'] },
    { dataName: 'moduleUsers::showOwnerRoles', roles:['owner'] },
    { dataName: 'moduleUsers::showEmployedRoles', roles:['employed'] },
    { dataName: 'moduleUsers::getAllUsers', roles:['admin'] },
    { dataName: 'moduleUsers::getAllUsersOfGym', roles:['owner'] },
    { dataName: 'moduleUsers::getCustomersAndInstructors', roles:['employed'] },
    // Clientes
    { dataName: 'moduleCustomers::Create', roles: ['admin', 'owner', 'employed'] },
    { dataName: 'moduleCustomers::Read', roles: ['admin', 'owner', 'employed'] },
    { dataName: 'moduleCustomers::Update', roles: ['admin', 'owner', 'employed'] },
    { dataName: 'moduleCustomers::Delete', roles: ['admin', 'owner'] },
    // Membresia
    { dataName: 'moduleMemberships::Create', roles: ['admin', 'owner'] },
    { dataName: 'moduleMemberships::Read', roles: ['admin', 'owner'] },
    { dataName: 'moduleMemberships::Update', roles: ['admin', 'owner'] },
    { dataName: 'moduleMemberships::Delete', roles: ['admin', 'owner'] },
    // Inventario
    { dataName: 'moduleStock::Create', roles: ['admin', 'owner'] },
    { dataName: 'moduleStock::Read', roles: ['admin', 'owner', 'employed'] },
    { dataName: 'moduleStock::Update', roles: ['admin', 'owner'] },
    { dataName: 'moduleStock::Delete', roles: ['admin', 'owner'] },
    // Ventas
    { dataName: 'moduleSales::Create', roles: ['admin', 'owner', 'employed'] },
    { dataName: 'moduleSales::Read', roles: ['admin', 'owner'] },
    { dataName: 'moduleSales::Update', roles: ['admin', 'owner'] },
    { dataName: 'moduleSales::Delete', roles: ['admin', 'owner'] },
    // Reportes
    { dataName: 'moduleReports::Create', roles: ['admin', 'owner'] },
    { dataName: 'moduleReports::Read', roles: ['admin', 'owner'] },
    { dataName: 'moduleReports::Update', roles: ['admin', 'owner'] },
    { dataName: 'moduleReports::Delete', roles: ['admin', 'owner'] },
    // Sidebar
    { dataName: 'moduleSidebar::showOwner', roles: ['owner'] },
    { dataName: 'moduleSidebar::showEmployed', roles: ['employed'] },
    // Configuraciones
    { dataName: 'moduleSettings::Create', roles: ['admin', 'owner'] },
    { dataName: 'moduleSettings::Read', roles: ['admin', 'owner'] },
    { dataName: 'moduleSettings::Update', roles: ['admin', 'owner'] },
    { dataName: 'moduleSettings::Delete', roles: ['admin', 'owner'] },
  ]
  // Creacion de los roles en la db

  await roles.forEach(role => {
    roleRepository.saveRole(role, (err, rl) => {
      if (err) return console.log(`Error en la creacion del role: ${role.dataName}`)

      console.log(`Creado role: ${role.dataName}`)
    })
  })

  await permissions.forEach(permission => permissionRepository.savePermission(permission, err => err ? console.log(`Error en la creacion del permiso: ${permission.dataName}`) : console.log(`Creado permiso: ${permission.dataName}`)))// eslint-disable-line no-confusing-arrow
  // Captura long de la fecha
  const now = Date.parse(new Date())
  // Plan default
  const plan = {
    dataName: 'Free',
    description: 'Plan de uso interno',
    durationInDays: 15,
    price: 0,
    private: true,
    status: true,
  }
  await planRepository.savePlan(plan, (err, res) => {
    if (err) {
      console.log(err)
      return
    }
    console.log(`Creado plan: ${plan.dataName}`)
    // Gimnasio default
    const gym = {
      branch: false,
      capacity: 100,
      dataName: 'Gym Advance',
      email: 'administracion@gymadvance.com',
      expirationDay: moment().add(plan.durationInDays).format('x'),
      fkIdPlan: res._id,
      location: 'Venezuela',
      phone: '04140822293',
      private: true,
      status: true,
    }
    gymRepository.saveGym(gym, (err2, res2) => {
      if (err2) {
        console.log(`Error al crear gimnasio: ${gym.dataName}`)
        return
      }
      console.log(`Creado gimnasio: ${gym.dataName}`)
      // Usuarios globales

      let birthdayValue = moment('10/04/1993', 'DD/MM/YYYY')

      const user1 = {
        birthday: birthdayValue.valueOf(),
        documentNumber: 20369960,
        documentType: 'dni',
        email: 'leonardoliveros.lo@gmail.com',
        emergencyPhone: '04143748364',
        fkIdGym: res2._id,
        fullname: 'Leonardo Oliveros',
        haveInsurancePolicy: false,
        mainPhone: '04126862293',
        observations: 'Administrador del sistema',
        residenceAddress: 'Coloncito, Táchira',
        role: 'admin',
        status: true,
        urlPicture: null,
        username: 'leonardoliveros',
        workAddress: 'Medellín',
      }

      birthdayValue = moment('28/06/1993', 'DD/MM/YYYY')

      const user2 = {
        birthday: birthdayValue.valueOf(),
        documentNumber: 22036241,
        documentType: 'dni',
        email: 'alm93alm@gmail.com',
        emergencyPhone: '02922230191',
        fkIdGym: res2._id,
        fullname: 'Angel Luis Maita Pitado',
        haveInsurancePolicy: false,
        mainPhone: '04128779901',
        observations: 'Administrador del sistema',
        residenceAddress: 'El Furrial, Edo. Monagas, calle Centenario con Paez casa S/N',
        role: 'admin',
        urlPicture: 'https://scontent-mia3-1.xx.fbcdn.net/v/t1.0-9/16115043_10210913067639187_5820026290488499411_n.jpg?_nc_cat=0&oh=a5fca5163d2c00a75771f84c37ff3010&oe=5B7325FC',
        status: true,
        username: 'runegame',
        workAddress: 'En la web',
      }

      userRepository.saveUser(user1, err3 => err3 ? console.log(`Error al crear el usuario: ${user1.username}`) : console.log(`Creado usuario: ${user1.username}`))// eslint-disable-line no-confusing-arrow
      userRepository.saveUser(user2, (err4, savedUser) => {
        if (err4) console.log(`Error al crear el usuario: ${user2.username}`)
        console.log(`Creado usuario: ${user2.username}`)

        const categories = [
          {
            fkIdGym: res2._id,
            fkIdUser: savedUser._id,
            name: 'ABD Express'
          },{
            fkIdGym: res2._id,
            fkIdUser: savedUser._id,
            name: 'Aqua Fitness'
          },{
            fkIdGym: res2._id,
            fkIdUser: savedUser._id,
            name: 'Bailoterapia'
          },{
            fkIdGym: res2._id,
            fkIdUser: savedUser._id,
            name: 'Body Jump'
          },{
            fkIdGym: res2._id,
            fkIdUser: savedUser._id,
            name: 'Body Pump'
          },{
            fkIdGym: res2._id,
            fkIdUser: savedUser._id,
            name: 'Cardiobox'
          },{
            fkIdGym: res2._id,
            fkIdUser: savedUser._id,
            name: 'Chikung'
          },{
            fkIdGym: res2._id,
            fkIdUser: savedUser._id,
            name: 'Crossfit'
          },{
            fkIdGym: res2._id,
            fkIdUser: savedUser._id,
            name: 'Entrenamiento Funcional'
          },{
            fkIdGym: res2._id,
            fkIdUser: savedUser._id,
            name: 'Fisioterapia'
          },{
            fkIdGym: res2._id,
            fkIdUser: savedUser._id,
            name: 'Fit Natación'
          },{
            fkIdGym: res2._id,
            fkIdUser: savedUser._id,
            name: 'Hit'
          },{
            fkIdGym: res2._id,
            fkIdUser: savedUser._id,
            name: 'Indoor Cycling'
          },{
            fkIdGym: res2._id,
            fkIdUser: savedUser._id,
            name: 'Kick Boxing'
          },{
            fkIdGym: res2._id,
            fkIdUser: savedUser._id,
            name: 'Musculación'
          },{
            fkIdGym: res2._id,
            fkIdUser: savedUser._id,
            name: 'Paddle'
          },{
            fkIdGym: res2._id,
            fkIdUser: savedUser._id,
            name: 'Pilates'
          },{
            fkIdGym: res2._id,
            fkIdUser: savedUser._id,
            name: 'Rumba'
          },{
            fkIdGym: res2._id,
            fkIdUser: savedUser._id,
            name: 'Spinning'
          },{
            fkIdGym: res2._id,
            fkIdUser: savedUser._id,
            name: 'Taichí'
          },{
            fkIdGym: res2._id,
            fkIdUser: savedUser._id,
            name: 'Yoga'
          },{
            fkIdGym: res2._id,
            fkIdUser: savedUser._id,
            name: 'Zumba'
          }
        ]

        categories.forEach(function (category) {
          gymRepository.saveEventCategory(category, err5 => err5 ? console.log(`Error al crear la categoria: ${category.name}`) : console.log(`Creada categoria: ${category.name}`))
        })
      })
    })
  })
  setTimeout(() => mongoose.connection.close(() => console.log('Base de datos minada')), 10000)
})
