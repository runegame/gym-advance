const dotenv = require('dotenv')

dotenv.config()

const config = {
  app: {
    env: process.env.APP_ENV || 'development',
    salt_work_factor: process.env.APP_SALT_WORK_FACTOR || 10,
    mongo_url: process.env.APP_MONGO_URL || 'mongodb://127.0.0.1/gymadvance',
  },
}

module.exports = config
