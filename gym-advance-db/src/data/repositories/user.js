import bcrypt from 'bcrypt'
import db from '../models'
import gyms from './gym'
import permissions from './permission'

class userRepository {
  static saveUser(user, callback) {
    user.password = '123456'
    const userData = new db.Users(user)
    userData.save((err, savedUser) => {
      if (err) return callback(err)

      let userData = savedUser
      delete userData.deleted
      delete userData.password
      gyms.updateBasicStatistic(userData.fkIdGym, 'users', () => {
        return callback(null, userData)
      })
    })
  }

  static updateUser(userData, callback) {
    db.Users.findOne({ _id: userData._id }, (err, user) => {
      if (err) {
        return callback(new Error(err), null)
      }
      delete userData._id
      delete userData.changePassword
      Object.keys(userData).forEach((key) => {
        if (key === 'password') {
          return callback(new Error('unauthorized'), null)
        }

        if (user[key] !== userData[key]) {
          user[key] = userData[key]
        }
      })
      user.save((err2, updatedUser) => {
        if (err2) {
          return callback(new Error(err2), null)
        }
        return callback(null, updatedUser)
      })
    })
  }

  static updateMedicalConditions (data, callback) {
    db.Users.findOne({ _id: data._id }, (err, user) => {
      if (err) {
        return callback(new Error(err), null)
      }
      delete data._id
      Object.keys(data).forEach((key) => {
        if (user[key] !== data[key]) {
          user[key] = data[key]
        }
      })
      user.save((err2, updatedUser) => {
        if (err2) {
          return callback(new Error(err2), null)
        }
        return callback(null, updatedUser)
      })
    })
  }

  static saveBodyMeasurements (data, callback) {
    db.Users.findOne({ _id: data.fkIdUser }, (err, user) => {
      if (err) {
        return callback(new Error(err), null)
      }
      if (user) {
        const BodyMeasurement = new db.BodyMeasurements(data)
        BodyMeasurement.save((err, savedBodyMeasurement) => {
          if (err) {
            console.log(err)
            return callback(err)
          }
          return callback(null, savedBodyMeasurement)
        })
      } else {
        return callback(null, {status: false, response: 'User not found'})
      }
    })
  }

  static deleteBodyMeasurement (_id, callback) {
    db.BodyMeasurements.findOne({ _id }, (err, bodyMeasurement) => {
      if (err) {
        return callback(new Error(err), null)
      }
      let bodyMeasurementData = { deleted: true }
      Object.keys(bodyMeasurementData).forEach((key) => {
        if (bodyMeasurement[key] !== bodyMeasurementData[key]) {
          bodyMeasurement[key] = bodyMeasurementData[key]
        }
      })
      bodyMeasurement.save((err2, bodyMeasurementDeleted) => {
        if (err2) {
          return callback(new Error(err2), null)
        }
        return callback(null, bodyMeasurementDeleted)
      })
    })
  }

  static deleteUser(_id, callback) {
    db.Users.findOne({ _id }, (err, user) => {
      if (err) {
        return callback(new Error(err), null)
      }
      let userData = { deleted: true }
      Object.keys(userData).forEach((key) => {
        if (key === 'password') {
          return callback(new Error('unauthorized'), null)
        }

        if (user[key] !== userData[key]) {
          user[key] = userData[key]
        }
      })
      user.save((err2, updatedUser) => {
        if (err2) {
          return callback(new Error(err2), null)
        }
        return callback(null, updatedUser)
      })
    })
  }

  static getUserById(_id, callback) {
    return db.Users.findOne({ _id }).populate('fkIdGym', 'dataName').populate('fkIdMembership', 'dataName').exec((err, user) => {
      if (err) return callback(err)

      let userData = user
      delete userData.deleted
      delete userData.password

      return callback(null, userData)
    })
  }

  static getUserByUsername(username, callback) {
    return db.Users.findOne({ username }).populate('fkIdGym', 'dataName fkIdPlan expirationDay').exec((err, user) => {
      if (err) {
        return callback(new Error(err), null)
      }
      return callback(null, user)
    })
  }

  static listUsers(fkIdUser, callback) {
    return db.Users.findOne({ _id: fkIdUser }).exec((err, user) => {
      if (err) return callback(err)
      if (user) {
        permissions.havePermission('moduleUsers::getAllUsers', user.role, (err, res) => {
          if (err) return callback(err)
          if (res) {
            return db.Users.find({ deleted: false }, { changePassword: 0, deleted: 0 }).populate('fkIdGym', 'dataName').exec((err, users) => {
              if (err) return callback(err, null)
              return callback(null, users)
            })
          } else {
            return callback(new Error('unauthorized'), null)
          }
        })
      }
    })
  }

  static getBodyMeasurementsOfUser(_id, callback) {
    return db.BodyMeasurements.find({ fkIdUser: _id, deleted: false}).populate('fkIdUser', 'fullname').exec((err, bodyMeasurements) => {
      if (err) return callback(err)

      return callback(null, bodyMeasurements)
    })
  }

  static getCustomersOfGym(fkIdUser, callback) {
    return db.Users.findOne({ _id: fkIdUser }).exec((err, user) => {
      if (err) return callback(err, null)

      if (user) {
        return db.Users.find({ fkIdGym: user.fkIdGym, role: 'customer', deleted: false }, (err, users) => {
          if (err) {
            return callback(err, null)
          }
          return callback(null, users)
        })
      } else {
        return callback(null, [])
      }
    })
  }

  static getInstructorsOfGym(fkIdGym, callback) {
    return db.Users.find({ fkIdGym, role: 'instructor', deleted: false }, (err, users) => {
      if (err) {
        return callback(err, null)
      }
      return callback(null, users)
    })
  }

  static getUsersOfGym(fkIdUser, fkIdGym, callback) {
    return db.Users.findOne({ _id: fkIdUser }).exec((err, user) => {
      if (err) return callback(err, null)

      if (user) {
        permissions.havePermission('moduleUsers::getAllUsersOfGym', user.role, (err, res) => {
          if (err) return callback(err, null)

          if (res) {
            return db.Users.find({ fkIdGym, deleted: false, '$or': [{role: 'employed'}, {role: 'instructor'}, {role: 'customer'}] }, { changePassword: 0, deleted: 0 }).populate('fkIdMembership', 'dataName').exec((err, users) => {
              if (err) {
                return callback(new Error(err), null)
              }
              return callback(null, users)
            })
          } else {
            permissions.havePermission('moduleUsers::getCustomersAndInstructors', user.role, (err, res) => {
              if (res) {
                return db.Users.find({ fkIdGym, deleted: false, '$or': [{role: 'instructor'}, {role: 'customer'}] }).populate('fkIdMembership', 'dataName').exec((err, users) => {
                  if (err) {
                    return callback(new Error(err), null)
                  }
                  return callback(null, users)
                })
              } else {
                return callback(null, [])
              }
            })
          }
        })
      } else {
        return callback(null, { users: null, reason: 'unauthorized' })
      }
    })
  }

  static changePassword(id, oldPassword, newPassword, callback) {
    return db.Users.findOne({ _id: id }, (err, user) => {
      if (err) return callback(err, null)

      if (user) {
        bcrypt.compare(oldPassword, user.password, (err, res) => {
          if (err) return callback(err)

          if (res) {
            let updatedUser = user
            updatedUser.changePassword = false
            updatedUser.password = newPassword
            user.save((err2, updatedUser) => {
              if (err2) return callback(err2, null)
              return callback(null, 'password_changed')
            })
          } else {
            return callback(new Error('bad_password'), null)
          }
        })
      }
    })
  }
}

export default userRepository
