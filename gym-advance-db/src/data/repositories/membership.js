import _ from 'underscore'

import db from '../models'
import moment from 'moment'
import gyms from './gym'
import users from './user'

class membershipRepository {
  static saveMembership(membership, callback) {
    const membershipData = new db.Memberships(membership)
    membershipData.save((err, savedMembership) => {
      if (err) return callback(err)
      gyms.updateBasicStatistic(savedMembership.fkIdGym, 'memberships', () => {
        return callback(null, savedMembership)
      })
    })
  }

  static updateMembership(membershipData, callback) {
    db.Memberships.findOne({ _id: membershipData._id }, (err, membership) => {
      if (err) return callback(err)

      delete membershipData._id

      Object.keys(membershipData).forEach((key) => {
        if (membership[key] !== membershipData[key]) {
          membership[key] = membershipData[key]
        }
      })
      membership.save((err2, updatedMembership) => {
        if (err2) return callback(err2)

        return callback(null, updatedMembership)
      })
    })
  }

  static deleteMembership(_id, callback) {
    db.Memberships.findOne({ _id }, (err, membership) => {
      if (err) {
        return callback(new Error(err), null)
      }
      let membershipData = { deleted: true }
      Object.keys(membershipData).forEach((key) => {
        if (membership[key] !== membershipData[key]) {
          membership[key] = membershipData[key]
        }
      })
      membership.save((err2, membershipUser) => {
        if (err2) {
          return callback(new Error(err2), null)
        }
        return callback(null, membershipUser)
      })
    })
  }

  // static deleteMembership(_id, callback) {
  //   return db.Users.find({ fkIdMembership: _id }, (err, users) => {
  //     if (err) return callback(err)
  //
  //     if (users.length > 0) {
  //       return callback(new Error(`Esta membresia no puede ser eliminada tiene ${users.length} usuario(s)`), null)
  //     }
  //     return db.Memberships.remove({ _id }, (err2, deletedMembership) => {
  //       if (err2) return callback(err2)
  //
  //       return callback(null, deletedMembership)
  //     })
  //   })
  // }

  static getMembership(_id, callback) {
    return db.Memberships.findOne({ _id }, (err, membership) => {
      if (err) return callback(err)

      return callback(null, membership)
    })
  }

  static getMembershipsOfGym(fkIdGym, callback) {
    return db.Memberships.find({ fkIdGym: fkIdGym, deleted: false }, (err, memberships) => {
      if (err) return callback(err)

      return callback(null, memberships)
    })
  }

  static getPaymentsOfUser(fkIdUser, callback) {
    return db.UserPayments.find({ fkIdUser }, (err, payments) => {
      if (err) return callback(err)

      return callback(null, payments)
    })
  }

  static getUsersPayment(_id, callback) {
    return db.UserPayments.findOne({_id}).populate('fkIdUser', 'fullname username').populate('fkIdGym', 'dataName').populate('fkIdMembership', 'dataName').exec((err, payment) => {
      if (err) return callback(err)

      return callback(null, payment)
    })
  }

  static listAllUserPayments(callback) {
    return db.UserPayments.find().populate('fkIdUser', 'username fullname').populate('fkIdGym', 'dataName').populate('fkIdMembership', 'dataName').exec((err, payments) => {
      if (err) return callback(err)

      return callback(null, payments)
    })
  }

  static listUserPayments(fkIdGym, callback) {
    return db.UserPayments.find({fkIdGym}).populate('fkIdUser', 'fullname').populate('fkIdMembership', 'dataName').exec((err, payments) => {
      if (err) return callback(err, null)

      return callback(null, payments)
    })
  }

  static saveUserPayment(payment, callback) {
    const dataPayment = new db.UserPayments(payment)
    dataPayment.save((err, savedPayment) => {
      if (err) return callback(err)

      db.Memberships.findOne({ _id: savedPayment.fkIdMembership }, function (err, membership) {
        if (err) return callback(err)

        users.getUserById(savedPayment.fkIdUser, function (err, user) {
          if (err) return callback(err)
          let newMembershipExpiration
          let currentMembershipExpiration = moment(user.membershipExpiration)
          let expired = currentMembershipExpiration.diff(moment(), 'days')

          if (expired < 0 || user.membershipExpiration === null) {
            newMembershipExpiration = moment().add(membership.durationInDays, 'days')
          } else {
            newMembershipExpiration = currentMembershipExpiration.add(membership.durationInDays, 'days')
          }

          users.updateUser({ _id: user._id, fkIdMembership: membership._id, membershipExpiration: newMembershipExpiration.valueOf() }, function (err, updatedUser) {
            if (err) return callback(err)

            gyms.updateBasicStatistic(savedPayment.fkIdGym, 'userPayments', () => {
              return callback(null, savedPayment)
            })
          })
        })
      })
    })
  }

  static updateUsersPayment(usersPaymentData, callback) {
    // Busca el pago de gimnasio
    db.UserPayments.findOne({ _id: usersPaymentData._id }, (err, usersPayment) => {
      if (err) {
        return callback(new Error(err), null)
      }
      delete usersPaymentData._id
      // Recorre las key del model de gimnasio
      Object.keys(usersPaymentData).forEach((key) => {
        // Si el valor es diferente lo reemplaza
        if (usersPayment[key] !== usersPaymentData[key]) {
          usersPayment[key] = usersPaymentData[key]
        }
      })
      // Guarda el cambio
      usersPayment.save((err2, updatedUsersPayment) => {
        if (err2) {
          return callback(new Error(err2), null)
        }
        return callback(null, updatedUsersPayment)
      })
    })
  }
}

export default membershipRepository
