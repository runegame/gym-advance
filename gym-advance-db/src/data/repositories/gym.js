import _ from 'underscore'

import db from '../models'
import moment from 'moment'
import plans from './plan'

class gymRepository {
  static saveGym(gym, callback) {
    plans.getFreePlan((err, freePlan) => {
      let tempGym = gym
      let expiration = moment().add(freePlan.durationInDays, 'days')
      tempGym.fkIdPlan = freePlan._id
      tempGym.expirationDay = expiration.valueOf()
      const gymData = new db.Gyms(tempGym)
      gymData.save((err, savedGym) => {
        if (err) return callback(err, null)
        this.saveBasicStatistic(savedGym._id, function (err2, savedBasicStatistic) {
          if (err) return callback(err2, null)
          return callback(null, savedGym)
        })
      })
    })
  }

  static getGym(_id, callback) {
    return db.Gyms.findOne({ _id }).populate('fkIdPlan', 'dataName').exec((err, gym) => {
      if (err) {
        return callback(new Error(err), null)
      }
      return callback(null, gym)
    })
  }

  static getGyms(callback) {
    return db.Gyms.find({ private: false, deleted: false }).populate('fkIdPlan', 'dataName').exec((err, gyms) => {
      if (err) {
        return callback(new Error(err), null)
      }
      return callback(null, gyms)
    })
  }

  static deleteGym(_id, callback) {
    db.Gyms.findOne({ _id }, (err, gm) => {
      if (err) {
        return callback(new Error(err), null)
      }
      let gymData = { deleted: true }
      Object.keys(gymData).forEach((key) => {
        if (gm[key] !== gymData[key]) {
          gm[key] = gymData[key]
        }
      })
      gm.save((err2, updatedGym) => {
        if (err2) {
          return callback(new Error(err2), null)
        }
        return callback(null, updatedGym)
      })
    })
  }

  // métodos para los eventos

  static saveEvent(newEvent, callback) {
    const eventData = new db.Events(newEvent)
    eventData.save((err, savedEvent) => {
      if (err) {
        return callback(err, null)
      }
      this.updateBasicStatistic(savedEvent.fkIdGym, 'events', () => {
        return callback(null, savedEvent)
      })
    })
  }

  static getEvent(_id, callback) {
    return db.Events.findOne({ _id }).populate('fkIdCategory', 'name').populate('instructors', 'fullname').exec((err, eventData) => {
      if (err) return callback(err, null)
      return callback(null, eventData)
    })
  }

  static updateEvent(eventData, callback) {
    // Busca el gimnasio
    db.Events.findOne({ _id: eventData._id }, (err, evnt) => {
      if (err) {
        return callback(new Error(err), null)
      }
      delete eventData._id
      // Recorre las key del model de eventos
      Object.keys(eventData).forEach((key) => {
        // Si el valor es diferente lo reemplaza
        if (evnt[key] !== eventData[key]) {
          evnt[key] = eventData[key]
        }
      })
      // Guarda el cambio
      evnt.save((err2, updatedEvent) => {
        if (err2) {
          return callback(new Error(err2), null)
        }
        return callback(null, updatedEvent)
      })
    })
  }

  static deleteEvent(_id, callback) {
    db.Events.findOne({ _id }, (err, evnt) => {
      if (err) {
        return callback(new Error(err), null)
      }
      let eventData = { deleted: true }
      Object.keys(eventData).forEach((key) => {
        if (evnt[key] !== eventData[key]) {
          evnt[key] = eventData[key]
        }
      })
      evnt.save((err2, updatedEvent) => {
        if (err2) {
          return callback(new Error(err2), null)
        }
        return callback(null, updatedEvent)
      })
    })
  }

  static getEventsOfGym(fkIdGym, callback) {
    return db.Events.find({ fkIdGym, deleted: false }).populate('fkIdCategory', 'name').populate('instructors', 'fullname').exec((err, events) => {
      if (err) return callback(err)

      return callback(null, events)
    })
  }

  static updateGym(_id, gymData, callback) {
    // Busca el gimnasio
    db.Gyms.findOne({ _id }, (err, gym) => {
      if (err) {
        return callback(new Error(err), null)
      }
      delete gymData._id
      // Recorre las key del model de gimnasio
      Object.keys(gymData).forEach((key) => {
        // Si el valor es diferente lo reemplaza
        if (gym[key] !== gymData[key]) {
          gym[key] = gymData[key]
        }
      })
      // Guarda el cambio
      gym.save((err2, updatedGym) => {
        if (err2) {
          return callback(new Error(err2), null)
        }
        return callback(null, updatedGym)
      })
    })
  }

  // métodos para las categorias
  static saveEventCategory(eventCategory, callback) {
    const eventCategoryData = new db.EventCategories(eventCategory)
    eventCategoryData.save((err, savedEventCategory) => {
      if (err) {
        return callback(err, null)
      }
      return callback(null, savedEventCategory)
    })
  }

  static getEventCategories(callback) {
    return db.EventCategories.find({}).sort('name').exec((err, eventCategories) => {
      if (err) return callback(err, null)
      return callback(null, eventCategories)
    })
  }

  // métodos para las estadísticas básicas
  static saveBasicStatistic (fkIdGym, callback) {
    const basicStatisticData = new db.BasicStatistics({fkIdGym: fkIdGym})
    return basicStatisticData.save((err, savedBasicStatistic) => {
      if (err) return callback(err, null)
      return callback(null, savedBasicStatistic)
    })
  }

  static getBasicStatistics (fkIdGym, callback) {
    /*
    1.  Busca la estadistica
    2.  Si no la consigue, consulta la existencia del gimnasio
    3.  Si existe, crea la nueva estadistica
    4.  Si la consigue, retorna el resultado
    */
    return db.BasicStatistics.findOne({ fkIdGym: fkIdGym }, (err, basicStatisticData) => {
      if (err) return callback(err, null)
      if (!basicStatisticData) {
        let gymRepository = this
        return gymRepository.getGym(fkIdGym, function (err, gym) {
          if (gym) {
            return gymRepository.saveBasicStatistic(fkIdGym, function (err, savedBasicStatistic) {
              return callback(null, savedBasicStatistic)
            })
          } else {
            return callback(null, {code: 1001, message: 'Gimnasio no encontrado'})
          }
        })
      } else {
        return callback(null, basicStatisticData)
      }
    })
  }

  static updateBasicStatistic (fkIdGym, fieldToUpdate, callback) {
    return this.getBasicStatistics(fkIdGym, function (err, basicStatisticData) {
      if (err) return callback(err, null)

      switch (fieldToUpdate) {
        case 'access':
          basicStatisticData.savedAccess = basicStatisticData.savedAccess + 1
          break
        case 'events':
          basicStatisticData.savedEvents = basicStatisticData.savedEvents + 1
          break
        case 'memberships':
          basicStatisticData.savedMemberships = basicStatisticData.savedMemberships + 1
          break
        case 'userPayments':
          basicStatisticData.savedUserPayments = basicStatisticData.savedUserPayments + 1
          break
        case 'users':
          basicStatisticData.savedUsers = basicStatisticData.savedUsers + 1
          break
      }
      // Guarda el cambio
      basicStatisticData.save((err2, updatedBasicStatistic) => {
        if (err2) return callback(err2, null)
        return callback(null, updatedBasicStatistic)
      })
    })
  }
}

export default gymRepository
