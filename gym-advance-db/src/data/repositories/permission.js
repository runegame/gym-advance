import _ from 'underscore'

import db from '../models'
import users from './user'

class permissionRepository {
  static savePermission(permission, callback) {
    const permissionData = new db.Permissions(permission)
    permissionData.save((err, savedPermission) => {
      if (err) {
        callback(err, null)
      }
      return callback(null, savedPermission)
    })
  }

  static updatePermission(_id, permissionData, callback) {
    db.Permissions.findOne({ _id }, (err, permission) => {
      if (err) {
        return callback(new Error(err), null)
      }
      Object.keys(permissionData).forEach((key) => {
        if (permission[key] !== permissionData[key]) {
          permission[key] = permissionData[key]
        }
      })
      permission.save((err2, updatedPermission) => {
        if (err2) {
          return callback(new Error(err2), null)
        }
        return callback(null, updatedPermission)
      })
    })
  }

  static deletePermission(_id, callback) {
    return db.Permissions.remove({ _id }, (err, deletedPermission) => {
      if (err) {
        return callback(new Error(err), null)
      }
      return callback(null, deletedPermission)
    })
  }

  static getPermission(_id, callback) {
    return db.Permissions.findOne({ _id }, (err, permission) => {
      if (err) {
        return callback(new Error(err), null)
      }
      return callback(null, permission)
    })
  }

  static getPermissions(callback) {
    return db.Permissions.find({}).sort('dataName').exec((err, permissions) => {
      if (err) {
        return callback(new Error(err), null)
      }
      return callback(null, permissions)
    })
  }

  static havePermission(dataNamePermission, dataNameRole, callback) {
    return db.Permissions.find({dataName: dataNamePermission, roles: {'$in': [dataNameRole]}}, (err, permissions) => {
      if (err) {
        return callback(err, false)
      }

      if (permissions.length > 0) {
        return callback(null, true)
      } else {
        return callback(null, false)
      }
    })
  }

  static userHavePermission(username, dataNamePermission, callback) {
    return users.getUserByUsername(username, (err, user) => {
      if (err) return callback(err, false)

      return db.Permissions.find({dataName: dataNamePermission, roles: {'$in': [user.role]}}, (err, permissions) => {
        if (err) return callback(err, false)

        if (permissions.length > 0) {
          return callback(null, true)
        } else {
          return callback(null, false)
        }
      })
    })
  }
}

export default permissionRepository
