import _ from 'underscore'

import db from '../models'

class roleRepository {
  static saveRole(role, callback) {
    const roleData = new db.Roles(role)
    roleData.save((err, savedRole) => {
      if (err) {
        return callback(new Error(err), null)
      }
      return callback(null, savedRole)
    })
  }

  static updateRole(_id, roleData, callback) {
    db.Roles.findOne({ _id }, (err, role) => {
      if (err) {
        return callback(new Error(err), null)
      }
      Object.keys(roleData).forEach((key) => {
        if (role[key] !== roleData[key]) {
          if (key === 'dataName') {
            db.Permissions.find({ roles: { $in: [role[key]] }}, (err, result) => {
              result.forEach((permission) => {
                const indexRole = permission.roles.indexOf(role[key])
                permission.roles[indexRole] = roleData[key]
                db.Permissions.update(permission, (err, permissionUpdated) => {
                  if (err) console.log(err)
                })
              })
              role[key] = roleData[key]
            })
          } else {
            role[key] = roleData[key]
          }
        }
      })
      role.save((err2, updatedRole) => {
        if (err2) {
          return callback(new Error(err2), null)
        }
        return callback(null, updatedRole)
      })
    })
  }

  static deleteRole(_id, callback) {
    return db.Roles.findOne({ _id }, (err, role) => {
      if (err) {
        return callback(new Error(err), null)
      }

      return db.Permissions.update(
        {roles: {$in: [role.dataName] }},
        {$pull: {roles: role.dataName }},
        {multi: true}, (err2, deletedPermissions) => {
        if (err2) {
          return callback(new Error(err2), null)
        }
        db.Roles.remove({ _id }, (err3, deletedRole) => {
          if (err3) {
            return callback(new Error(err3), null)
          }
          callback(null, deletedRole)
        })
      })
    })
  }

  static getRole(_id, callback) {
    return db.Roles.findOne({ _id }, (err, role) => {
      if (err) {
        return callback(new Error(err), null)
      }
      return callback(null, role)
    })
  }

  static getRoles(callback) {
    return db.Roles.find((err, roles) => {
      if (err) {
        return callback(new Error(err), null)
      }
      return callback(null, roles)
    })
  }

  static getPermissionsOfRole(_id, callback) {
    return db.Roles.findOne({ _id }, (err, role) => {
      if (err) {
        return callback(err, null)
      }

      return db.Permissions.find({ roles: { $in: [role.dataName] } }, (err2, permissions) => {
        if (err2) {
          return callback(err2, null)
        }

        const dataRole = role
        dataRole.permissions = []

        permissions.forEach(function addPermissionToRole(permissionItem, index) {
          dataRole.permissions.push(permissionItem.dataName)
        })
        return callback(null, dataRole)
      })
    })
  }
}

export default roleRepository
