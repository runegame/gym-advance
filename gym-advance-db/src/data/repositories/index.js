import authRepository from './auth'
import gymRepository from './gym'
import membershipRepository from './membership'
import permissionRepository from './permission'
import planRepository from './plan'
import roleRepository from './role'
import userRepository from './user'

import mongoose from 'mongoose'
import config from '../../config'

/**
 * Establezca el detector de eventos en mongoose.connection
 */
mongoose.connection.on('error', error => console.log(error))

mongoose.connection.on('open', () => console.log(`Connected to ${config.app.mongo_url}`))

/**
 * Conectar a la Base de Datos
 */
mongoose.connect(config.app.mongo_url, {
  useMongoClient: true,
})

export { authRepository, gymRepository, membershipRepository, permissionRepository,
  planRepository, roleRepository, userRepository }
