import db from '../models'
import bcrypt from 'bcrypt'
import moment from 'moment'
import gyms from './gym'
import permissions from './permission'

import config from '../../config'

class authRepository {

  static authenticateAdmin (username, password, callback) {
    return db.Users.findOne({ username }).exec((err, user) => {
      if (err) {
        return callback(err, null)
      }
      if (user) {
        permissions.havePermission('moduleAdmin::signin', user.role, (err, res) => {
          if (err) return callback(err, null)
          if (res) {
            bcrypt.compare(password, user.password, (err, res) => {
              if (err) return callback(err)

              if (!res) return callback(null, { status: res, message: 'incorrect_password' })

              return callback(null, { status: res, fkIdGym: user.fkIdGym, userId: user._id })
            })
          } else {
            return callback(null, { status: false, message: 'unauthorized' })
          }
        })
      } else {
        return callback(null, { status: false, message: 'user_not_found' })
      }
    })
  }

  static authenticateUser (username, password, callback) {
    return db.Users.findOne({ username }).exec((err, user) => {
      if (err) return callback(err, null)

      if (user) {
        permissions.havePermission('moduleOwner::signin', user.role, (err, res) => {
          if (err) return callback(err, null)

          if (res) {
            gyms.getGym(user.fkIdGym, function (err, gym) {
              if (err) return callback(err, null)

              let expirationDay = moment(gym.expirationDay)
              let expired = expirationDay.diff(moment(), 'days')
              if (expired < 0) {
                return callback(null, { status: false, message: 'plan_expired' })
              }

              bcrypt.compare(password, user.password, (err, res) => {
                if (err) return callback(err)

                if (!res) return callback(null, { status: res, message: 'incorrect_password' })

                return callback(null, { status: res, fkIdGym: user.fkIdGym, userId: user._id })
              })
            })
          } else {
            return callback(null, { status: false, message: 'unauthorized' })
          }
        })
      } else {
        return callback(null, { status: false, message: 'user_not_found' })
      }
    })
  }

  static accessCustomer (documentNumber, callback) {
    return db.Users.findOne({ documentNumber: documentNumber }).exec((err, user) => {
      if (err) {
        console.log(err)
        return callback(err, null)
      }

      if (user) {
        let accessRecord = {fkIdUser: user._id, fkIdGym: user.fkIdGym, status: false}

        permissions.havePermission('moduleAccess::access', user.role, (err, res) => {
          if (err) return callback(err, null)

          if (res) {
            if (!user.membershipExpiration) {
              // guardamos un registro de acceso de mmebresia no comprada
              accessRecord.reason = 'membership_not_purchased'
              const record = new db.AccessRecords(accessRecord)
              record.save((err, access) => { if (err) return console.log(err) })
              return callback(null, { status: false, message: 'membership_not_purchased'})
            }

            let expirationDay = moment(user.membershipExpiration)
            let expired = expirationDay.diff(moment(), 'days')

            if (expired < 0) {
              // guardamos un registro de acceso de membresia expirada
              accessRecord.reason = 'membership_expired'
              const record = new db.AccessRecords(accessRecord)
              record.save((err, access) => { if (err) return console.log(err) })
              return callback(null, { status: false, message: 'membership_expired' })
            }

            // guardamos un registro de acceso autorizado
            accessRecord.status = true
            accessRecord.reason = 'guaranteed_access'
            const record = new db.AccessRecords(accessRecord)
            record.save((err, access) => {
              if (err) return console.log(err)
              gyms.updateBasicStatistic(accessRecord.fkIdGym, 'access', () => {
                return callback(null, { status: res })
              })
            })
          } else {
            // guardamos un registro de acceso no autorizado
            accessRecord.reason = 'unauthorized'
            const record = new db.AccessRecords(accessRecord)
            record.save((err, access) => { if (err) return console.log(err) })
            return callback(null, { status: false, message: 'unauthorized' })
          }
        })
      } else {
        return callback(null, { status: false, message: 'user_not_found' })
      }
    })
  }

  static getAccessRecordsByGym(fkIdGym, callback) {
    return db.AccessRecords.find({ fkIdGym }).limit(100).sort({createdAt: -1}).populate('fkIdUser', 'fullname username email').exec((err, records) => {
      if (err) return callback(err, null)
      return callback(null, records)
    })
  }
}

export default authRepository
