import _ from 'underscore'

import db from '../models'
import moment from 'moment'
import gyms from './gym'

class planRepository {
  static savePlan(plan, callback) {
    const planData = new db.Plans(plan)
    planData.save((err, savedPlan) => {
      if (err) {
        callback(err, null)
      }
      return callback(null, savedPlan)
    })
  }

  static updatePlan(_id, planData, callback) {
    db.Plans.findOne({ _id }, (err, plan) => {
      if (err) {
        return callback(new Error(err), null)
      }
      Object.keys(planData).forEach((key) => {
        if (plan[key] !== planData[key]) {
          plan[key] = planData[key]
        }
      })
      plan.save((err2, updatedPlan) => {
        if (err2) {
          return callback(new Error(err2), null)
        }
        return callback(null, updatedPlan)
      })
    })
  }

  static deletePlan(_id, callback) {
    return db.Gyms.find({ fkIdPlan: _id }, (err, gyms) => {
      if (err) {
        return callback(new Error(err), null)
      }
      if (gyms.length > 0) {
        return callback(new Error(`Este plan no puede ser eliminado tiene ${gyms.length} gimnasio(s)`), null)
      }
      return db.Plans.remove({ _id }, (err2, deletedPlan) => {
        if (err2) {
          return callback(new Error(err2), null)
        }
        return callback(null, deletedPlan)
      })
    })
  }

  static getPlan(_id, callback) {
    return db.Plans.findOne({ _id }, (err, plan) => {
      if (err) {
        return callback(new Error(err), null)
      }
      return callback(null, plan)
    })
  }

  static getFreePlan(callback) {
    return db.Plans.findOne({dataName: 'Free'}, (err, freePlan) => {
      if (err) {
        return callback(new Error(err), null)
      }
      return callback(null, freePlan)
    })
  }

  static getPlans(callback) {
    return db.Plans.find({ private: false }, (err, plans) => {
      if (err) {
        return callback(err, null)
      }
      return callback(null, plans)
    })
  }

  static getGymsPayment(_id, callback) {
    return db.GymPayments.findOne({_id}).populate('fkIdGym', 'dataName').populate('fkIdPlan', 'dataName').exec((err, gymPayment) => {
      if (err) return callback(err)

      return callback(null, gymPayment)
    })
  }

  static getPaymentsOfGym(fkIdGym, callback) {
    return db.GymPayments.find({ fkIdGym }, (err, payments) => {
      if (err) {
        return callback(err, null)
      }
      return callback(null, payments)
    })
  }

  static listGymPayments(callback) {
    return db.GymPayments.find().populate('fkIdGym', 'dataName').populate('fkIdPlan', 'dataName').exec((err, payments) => {
      return callback(null, payments)
    })
  }

  static saveGymPayment(payment, callback) {
    const gymPaymentData = new db.GymPayments(payment)
    gymPaymentData.save((err, savedPayment) => {
      if (err) callback(err, null)

      db.Plans.findOne({ _id: savedPayment.fkIdPlan }, function (err, plan) {
        if (err) return callback(err, null)

        gyms.getGym(savedPayment.fkIdGym, function (err, gym) {
          if (err) return callback(err, null)
          let newExpirationDay
          let currentExpirationDay = moment(gym.expirationDay)
          let expired = currentExpirationDay.diff(moment(), 'days')

          if (expired < 0) {
            newExpirationDay = moment().add(plan.durationInDays, 'days')
          } else {
            newExpirationDay = moment(gym.expirationDay).add(plan.durationInDays, 'days')
          }

          gyms.updateGym(gym._id, { expirationDay: newExpirationDay.valueOf(), fkIdPlan: plan._id }, function (err, updatedGym) {
            if (err) return callback(err, null)

            return callback(null, savedPayment)
          })
        })
      })
    })
  }

  static updateGymsPayment(gymsPaymentData, callback) {
    // Busca el pago de gimnasio
    db.GymPayments.findOne({ _id: gymsPaymentData._id }, (err, gymsPayment) => {
      if (err) {
        return callback(new Error(err), null)
      }
      delete gymsPaymentData._id
      // Recorre las key del model de gimnasio
      Object.keys(gymsPaymentData).forEach((key) => {
        // Si el valor es diferente lo reemplaza
        if (gymsPayment[key] !== gymsPaymentData[key]) {
          gymsPayment[key] = gymsPaymentData[key]
        }
      })
      // Guarda el cambio
      gymsPayment.save((err2, updatedGymsPayment) => {
        if (err2) {
          return callback(new Error(err2), null)
        }
        return callback(null, updatedGymsPayment)
      })
    })
  }
}

export default planRepository
