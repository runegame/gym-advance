import fs from 'fs'
import path from 'path'
import mongoose from 'mongoose'

// import config from '../../config'

const basename = path.basename(__filename)

const models = {}

/**
 * Asignar modelos al objeto 'models'
 */
fs
  .readdirSync(__dirname)
  .filter(filename =>
  // Obtener el nombre del archivo que vive en el mismo directorio sin este archivo(index.js).
    (filename.indexOf('.') !== 0) && (filename !== basename))
  .forEach((filename) => {
  // Si la extension del archivo no es '.js', salta
    if (filename.slice(-3) !== '.js') return

    const filepath = path.join(__dirname, filename)

    // Cuando se utiliza un archivo importado, 'export default', el objeto se asigna como 'default'
    const imported = (require(filepath).default) ? // eslint-disable-line global-require, import/no-dynamic-require, max-len
    require(filepath).default : // eslint-disable-line global-require, import/no-dynamic-require
      require(filepath) // eslint-disable-line global-require, import/no-dynamic-require

    if (typeof imported.modelName !== 'undefined') {
      // Se espera que el archivo de definición del modelo exporte el 'Model' de mongoose.
      models[imported.modelName] = imported
    }
  })

models._mongoose = mongoose

export default models
