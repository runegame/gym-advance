import mongoose from 'mongoose'

const basicStatisticsSchema = new mongoose.Schema({
  fkIdGym: {
    type: mongoose.Schema.ObjectId,
    ref: 'Gyms'
  },
  savedUsers: {
    type: Number,
    default: 0
  },
  savedMemberships: {
    type: Number,
    default: 0
  },
  savedEvents: {
    type: Number,
    default: 0
  },
  savedUserPayments: {
    type: Number,
    default: 0
  },
  savedAccess: {
    type: Number,
    default: 0
  }
}, {
  timestamps: true,
})

const BasicStatistic = mongoose.model('BasicStatistics', basicStatisticsSchema)

module.exports = BasicStatistic
