import mongoose from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'

const accessRecordSchema = new mongoose.Schema({
  fkIdGym: {
    type: mongoose.Schema.ObjectId,
    ref: 'Gyms',
    required: [true, 'El registro debe estar asociado a un gimnasio'],
  },
  fkIdUser: {
    type: mongoose.Schema.ObjectId,
    ref: 'Users',
    required: [true, 'El registro debe estar asociado a un usuario'],
  },
  status: {
    type: Boolean,
    required: [true, 'El registro debe tener un status'],
  },
  reason: {
    type: String,
    required: [true, 'El registro debe tener una razon']
  }
}, {
  timestamps: true,
})

const AccessRecord = mongoose.model('AccessRecords', accessRecordSchema)

module.exports = AccessRecord
