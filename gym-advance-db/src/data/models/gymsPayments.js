import mongoose from 'mongoose'

const gymPaymentSchema = new mongoose.Schema({
  amount: {
    type: Number,
    required: [true, 'Debe introducir obligatoriamente una cantidad'],
  },
  fkIdGym: {
    type: mongoose.Schema.ObjectId,
    ref: 'Gyms',
    required: [true, 'El pago debe estar asociado al ID de un gimnasio'],
  },
  fkIdPlan: {
    type: mongoose.Schema.ObjectId,
    ref: 'Plans',
    required: [true, 'El pago debe estar asociado a un plan'],
  },
  paymentType: String,
  status: {
    type: String,
    required: [true, 'Debe haber un status para el pago'],
  }
}, {
  timestamps: true,
})

const GymPayment = mongoose.model('GymPayments', gymPaymentSchema)

module.exports = GymPayment
