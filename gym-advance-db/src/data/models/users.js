import mongoose from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'
import bcrypt from 'bcrypt'

import config from '../../config'

const userSchema = new mongoose.Schema({
  birthday: {
    type: Number,
    required: [true, 'El usuario debe tener una fecha de cumpleaños']
  },
  changePassword: {
    type: Boolean,
    default: true
  },
  deleted: {
    type: Boolean,
    default: false
  },
  documentNumber: {
    type: Number,
    required: [true, 'El usuario debe tener un numero del documento de identificacion'],
    unique: true,
  },
  documentType: {
    type: String,
    required: [true, 'Debe insertar un tipo de documento de identificacion'],
  },
  email: {
    type: String,
    required: [true, 'El usuario debe tener un email'],
    unique: true,
  },
  emergencyPhone: {
    type: String,
    default: null
  },
  fkIdGym: {
    type: mongoose.Schema.ObjectId,
    ref: 'Gyms',
    required: [true, 'El usuario debe estar asociado al ID de un gimnasio'],
  },
  fkIdMembership: {
    type: mongoose.Schema.ObjectId,
    ref: 'Memberships',
    default: null
  },
  fullname: {
    type: String,
    required: [true, 'El usuario debe tener un nombre'],
  },
  haveInsurancePolicy: {
    type: Boolean,
    required: [true, 'Debe especificarse si se tiene poliza de seguro'],
  },
  mainPhone: {
    type: String,
    required: [true, 'Es obligatorio un numero de telefono principal'],
  },
  medicalConditions: {
    created: {
      type: Boolean,
      default: false
    },
    lesionOsea: {
      value: {
        type: Boolean,
        default: false
      },
      description: {
        type: String,
        default: null
      }
    },
    lesionMuscular: {
      value: {
        type: Boolean,
        default: false
      },
      description: {
        type: String,
        default: null
      }
    },
    enfermedadCardiovascular: {
      value: {
        type: Boolean,
        default: false
      },
      description: {
        type: String,
        default: null
      }
    },
    actividadDeportiva: {
      value: {
        type: Boolean,
        default: false
      },
      description: {
        type: String,
        default: null
      }
    },
    asfixiaConFacilidad: {
      type: Boolean,
      default: false
    },
    asmatico: {
      type: Boolean,
      default: false
    },
    epilectico: {
      type: Boolean,
      default: false
    },
    diabetico: {
      type: Boolean,
      default: false
    },
    fumador: {
      type: Boolean,
      default: false
    },
    embarazada: {
      type: Boolean,
      default: false
    },
    anemia: {
      type: Boolean,
      default: false
    },
    inscripcionAnterior: {
      type: Boolean,
      default: false
    },
    mareos: {
      type: Boolean,
      default: false
    },
    desmayos: {
      type: Boolean,
      default: false
    },
    nauceas: {
      type: Boolean,
      default: false
    },
    dificultadRespiratoria: {
      type: Boolean,
      default: false
    }
  },
  membershipExpiration: {
    type: Number,
    default: null
  },
  nameInsurancePolicy: {
    type: String,
    default: null
  },
  numberLifeInsurance: {
    type: Number,
    default: null
  },
  observations: {
    type: String,
    default: null
  },
  password: { type: String, default: '123456' },
  residenceAddress: {
    type: String,
    required: [true, 'Es obligatorio introducir una direccion de residencia'],
  },
  role: {
    type: String,
    required: [true, 'Es obligatorio el rol del usuario']
  },
  status: { type: Boolean, default: false },
  urlPicture: { type: String, default: null },
  username: {
    type: String,
    required: [true, 'Es obligatorio introducir un nombre de usuario'],
    unique: true,
  },
  workAddress: { type: String, default: null }
}, {
  timestamps: true,
})

/**
 * Captura el evento de guardar(save) para realizar
 * acciones sobre el registro a guardar
 */
userSchema.pre('save', function (next) { // eslint-disable-line func-names
  const user = this
  // Solo hash la contraseña si es nueva o se actualiza
  if (!user.isModified('password')) {
    return next()
  }

  // genera un salto
  return bcrypt.genSalt(config.app.salt_work_factor, (err, salt) => {
    if (err) return next(err)
    // Hash la contraseña usando nuestro nuevo salto
    return bcrypt.hash(user.password, salt, (err2, hash) => {
      if (err2) return next(err2)
      // Anular la contraseña de texto con el hash
      user.password = hash
      return next()
    })
  })
})

/**
 * Metodo para comparar contraseña en text con el hash
 * (o contraseña almacenada)
 */
userSchema.methods.comparePassword = (candidatePassword, cb) =>
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    if (err) return cb(err)
    return cb(null, isMatch)
  })

// Validacion para campos unicos https://www.npmjs.com/package/mongoose-unique-validator
userSchema.plugin(uniqueValidator)

const User = mongoose.model('Users', userSchema)
module.exports = User
