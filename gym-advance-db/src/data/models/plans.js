import mongoose from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'

const planSchema = new mongoose.Schema({
  dataName: {
    type: String,
    required: [true, 'El plan debe tener un nombre'],
    unique: true,
  },
  description: {
    type: String,
    required: [true, 'El plan debe tener una description'],
  },
  durationInDays: {
    type: Number,
    required: [true, 'Debe establecer la duracion del plan en dias'],
  },
  price: {
    type: Number,
    required: [true, 'Debe establecer un precio para el plan'],
  },
  private: {
    type: Boolean,
    required: [true, 'Debe establecer la privacidad del rol'],
  },
  status: {
    type: Boolean,
    required: [true, 'Debes indicar si está o no habilitado'],
  },
  urlPicture: String,
}, {
  timestamps: true,
})

planSchema.plugin(uniqueValidator)

const Plan = mongoose.model('Plans', planSchema)

module.exports = Plan
