import mongoose from 'mongoose'

const userPaymentSchema = new mongoose.Schema({
  amount: {
    type: Number,
    required: [true, 'Debe introducir obligatoriamente una cantidad'],
  },
  fkIdGym: {
    type: mongoose.Schema.ObjectId,
    ref: 'Gyms',
    required: [true, 'El pago debe estar asociado a un gimnasio'],
  },
  fkIdMembership: {
    type: mongoose.Schema.ObjectId,
    ref: 'Memberships',
    required: [true, 'El pago debe estar asociado a una membresia'],
  },
  fkIdUser: {
    type: mongoose.Schema.ObjectId,
    ref: 'Users',
    required: [true, 'El pago debe estar asociado al ID de un usuario'],
  },
  paymentType: String,
  status: {
    type: String,
    required: [true, 'Debe haber un status para el pago'],
  },
}, {
  timestamps: true,
})

const UserPayment = mongoose.model('UserPayments', userPaymentSchema)

module.exports = UserPayment
