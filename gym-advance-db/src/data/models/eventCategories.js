import mongoose from 'mongoose'

const eventCategoriesSchema = new mongoose.Schema({
  fkIdGym: {
    type: mongoose.Schema.ObjectId,
    ref: 'Gyms',
    required: [true, 'La categoria debe estar asociada a un gimnasio'],
  },
  fkIdUser: {
    type: mongoose.Schema.ObjectId,
    ref: 'Users',
    required: [true, 'La categoria debe estar asociada a un usuario'],
  },
  name: {
    type: String,
    required: [true, 'La categoria debe tener un nombre'],
  },
  url: { type: String, default: null }
}, {
  timestamps: true,
})

const EventCategories = mongoose.model('EventCategories', eventCategoriesSchema)

module.exports = EventCategories
