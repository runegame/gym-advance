import mongoose from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'

const gymSchema = new mongoose.Schema({
  branch: {
    type: Boolean,
    required: [true, 'Necesitamos saber si es un gimnasio principal o una sucursal'],
  },
  capacity: {
    type: Number,
    required: [true, 'Debe haber una capacidad del gimnasio'],
  },
  dataName: {
    type: String,
    required: [true, 'El gimnasio debe tener un nombre para identificarlo'],
    unique: true,
  },
  deleted: {
    type: Boolean,
    default: false
  },
  email: {
    type: String,
    required: [true, 'El correo es el medio principal de comunicacion con el gimnasio'],
    unique: true,
  },
  expirationDay: {
    type: Number,
    required: [true, 'Debe haber una fecha de expiracion del plan del gimnasio']
  },
  fkIdPlan: {
    type: mongoose.Schema.ObjectId,
    ref: 'Plans',
    required: [true, 'El gimnasio debe estar asociado al ID de un plan'],
  },
  location: {
    type: String,
    required: [true, 'Debemos saber donde estas ubicado'],
  },
  phone: {
    type: String,
    required: [true, 'Necesitamos un telefono para comunicarnos con el gimnasio'],
  },
  private: {
    type: Boolean,
    required: [true, 'Debe establecer la privacidad del gimnasio'],
  },
  status: {
    type: Boolean,
    required: [true, 'Debes indicar si está o no habilitado'],
  },
}, {
  timestamps: true,
})

gymSchema.plugin(uniqueValidator)

const Gym = mongoose.model('Gyms', gymSchema)

module.exports = Gym
