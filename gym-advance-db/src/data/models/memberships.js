import mongoose from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'

const membershipSchema = new mongoose.Schema({
  checkin: {
    type: String,
    default: 'Ninguna'
  },
  checkout: {
    type: String,
    default: 'Ninguna'
  },
  dataName: {
    type: String,
    required: [true, 'La membresia debe tener un nombre'],
    unique: true,
  },
  deleted: {
    type: Boolean,
    default: false
  },
  description: {
    type: String,
    required: [true, 'La membresia debe tener una descripcion'],
  },
  durationInDays: {
    type: Number,
    required: [true, 'La membresia debe tener una duracion en dias'],
  },
  fkIdGym: {
    type: String,
    required: [true, 'La membresia debe estar asociada a un gimnasio'],
  },
  price: {
    type: Number,
    required: [true, 'La membresia debe tener un precio'],
  },
  status: { type: Boolean, default: false },
  urlPicture: String,
}, {
  timestamps: true,
})

membershipSchema.plugin(uniqueValidator)

const Membership = mongoose.model('Memberships', membershipSchema)

module.exports = Membership
