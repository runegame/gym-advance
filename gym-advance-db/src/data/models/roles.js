import mongoose from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'

const roleSchema = new mongoose.Schema({
  dataName: {
    type: String,
    required: [true, 'El rol debe tener un nombre para identificarlo'],
    unique: true,
  },
  description: {
    type: String,
    required: [true, 'El rol debe tener una description'],
  },
  scope: String,
})

roleSchema.plugin(uniqueValidator)

const Role = mongoose.model('Roles', roleSchema)

module.exports = Role
