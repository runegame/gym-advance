import mongoose from 'mongoose'

const bodyMesaurementSchema = new mongoose.Schema({
  fkIdUser: {
    type: mongoose.Schema.ObjectId,
    ref: 'Users',
    required: [true, 'El registro debe estar asociado a un usuario'],
  },
  deleted: {
    type: Boolean,
    default: false
  },
  weight: { // peso
    type: Number,
    default: 0
  },
  height: { // altura
    type: Number,
    default: 0
  },
  chest: { // pecho
    type: Number,
    default: 0
  },
  rightBicep: { // bicep derecho
    type: Number,
    default: 0
  },
  leftBicep: { // bicep izquierdo
    type: Number,
    default: 0
  },
  back: { // espalda
    type: Number,
    default: 0
  },
  shoulder: { // hombro
    type: Number,
    default: 0
  },
  highAbdomen: { // abdomen alto
    type: Number,
    default: 0
  },
  middleAbdomen: { // abdomen medio
    type: Number,
    default: 0
  },
  lowAbdomen: { // abdomen bajo
    type: Number,
    default: 0
  },
  gluteus: { // gluteo
    type: Number,
    default: 0
  },
  rightLeg: { // pierna derecha
    type: Number,
    default: 0
  },
  leftLeg: { // pierna izquierda
    type: Number,
    default: 0
  },
  rightCalf: { // pantorrilla derecha
    type: Number,
    default: 0
  },
  leftCalf: { // pantorrilla izquierda
    type: Number,
    default: 0
  },
  rightForearm: { // antebrazo derecho
    type: Number,
    default: 0
  },
  leftForearm: { // antebrazo izquierdo
    type: Number,
    default: 0
  },
  waist: { //cintura
    type: Number,
    default: 0
  }
}, {
  timestamps: true,
})

const BodyMeasurement = mongoose.model('BodyMeasurements', bodyMesaurementSchema)

module.exports = BodyMeasurement
