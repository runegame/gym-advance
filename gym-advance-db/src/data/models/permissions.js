import mongoose from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'

const permissionSchema = new mongoose.Schema({
  dataName: {
    type: String,
    required: [true, 'El permiso debe tener un nombre'],
    unique: true,
  },
  roles: [String],
})

permissionSchema.plugin(uniqueValidator)

const Permission = mongoose.model('Permissions', permissionSchema)

module.exports = Permission
