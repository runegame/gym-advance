import mongoose from 'mongoose'

const eventSchema = new mongoose.Schema({
  capacity: Number,
  dataName: {
    type: String
  },
  deleted: {
    type: Boolean,
    default: false
  },
  description: String,
  fkIdCategory: {
    type: mongoose.Schema.ObjectId,
    ref: 'EventCategories',
    required: [true, 'El evento debe tener una categoria']
  },
  fkIdGym: {
    type: mongoose.Schema.ObjectId,
    ref: 'Gyms',
    required: [true, 'El evento debe estar asociado al ID de un gimnasio'],
  },
  fkIdUser: {
    type: mongoose.Schema.ObjectId,
    ref: 'Users',
    required: [true, 'El usuario debe estar asociado al ID de un usuario'],
  },
  instructors: [{
    type: mongoose.Schema.ObjectId,
    ref: 'Users'
  }],
  price: {
    type: Number,
    default: 0
  },
  registeredCustomers: [
    {
      type: mongoose.Schema.ObjectId,
      ref: 'Users'
    }
  ],
  startDate: {
    type: Number,
    required: [true, 'El evento debe tener una fecha de inicio']
  },
  ubication: String
}, {
  timestamps: true,
})

const Event = mongoose.model('Events', eventSchema)

module.exports = Event
