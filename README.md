# README #

### Gym Advance ###

Gym Advance es un proyecto enfocado en la administración de gimnasios, que permite ir organizando los clientes, eventos, pagos y entradas de los clientes al gimnasio de manera sencilla y automatizada.

Por el lado administrativo permite crear gimnasios, usuarios para un gimnasio, manejar pagos de licencia de uso, entre otras funciones.

### Explicación de la subdivision del proyecto ###

**gym-advance:** front-end construido con JavaScript del lado del cliente, utilizando Page, pug, express y materialize-css

**gym-advance-admin:** igual que el anterior pero la parte administrativa del sistema

**gym-advance-api:** API construido con microservicios.

**gym-advance-client:** interfaz para unir el front-end con la API

**gym-advance-customers:** nuevo servidor express construido para recibir las consultas del API solo con Vuejs

**gym-advance-db:** gestor de base de datos construido en MongoDB

**gym-advance-deploy:** contiene archivos que inician los servicios en el servidor

**gym-advance-front-end:** Front-end del proyecto construido en Vuejs

**gym-advance-landing-page:** contiene la pagina inicial que se muestra a nuevos clientes

Pagina principal del proyecto: http://app2.gymadvance.com
Usuario: runegame
Password: 123456

Landing Page: http://www.gymadvance.com

Direccion del repositorio: https://bitbucket.org/runegame/gym-advance