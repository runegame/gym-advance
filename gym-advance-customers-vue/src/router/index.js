import Vue from 'vue'
import Router from 'vue-router'
import Access from '@/components/Access'
import EditEvent from '@/components/Events/EditEvent.vue'
import EditMembership from '@/components/Memberships/EditMembership.vue'
import EditUser from '@/components/Users/EditUser.vue'
import EditUserPayment from '@/components/UserPayments/EditUserPayment.vue'
import Event from '@/components/Events/Event.vue'
import Events from '@/components/Events/Events.vue'
import Home from '@/components/Home/Home.vue'
import Main from '@/components/Main.vue'
import MedicalConditions from '@/components/Users/MedicalConditions.vue'
import Membership from '@/components/Memberships/Membership.vue'
import Memberships from '@/components/Memberships/Memberships.vue'
import NewBodyMeasurement from '@/components/Users/NewBodyMeasurement.vue'
import NewEvent from '@/components/Events/NewEvent.vue'
import NewMembership from '@/components/Memberships/NewMembership.vue'
import NewUserPayment from '@/components/UserPayments/NewUserPayment.vue'
import NewUser from '@/components/Users/NewUser.vue'
import Settings from '@/components/Settings'
import Login from '@/components/Login'
import UserPayment from '@/components/UserPayments/UserPayment.vue'
import UserPayments from '@/components/UserPayments/UserPayments.vue'
import User from '@/components/Users/User.vue'
import Users from '@/components/Users/Users.vue'
import AuthGuard from './auth-guard'
import LoginGuard from './login-guard'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: '/home',
      name: 'Main',
      component: Main,
      meta: { title: 'Inicio' },
      beforeEnter: AuthGuard,
      children: [
        { path: 'access', name: 'Access', component: Access, meta: { title: 'Registro de acceso' }, beforeEnter: AuthGuard },
        { path: 'events', name: 'Events', component: Events, meta: { title: 'Eventos especiales' }, beforeEnter: AuthGuard },
        { path: 'events/edit/:id', props: true, name: 'EditEvent', component: EditEvent, meta: { title: 'Editar Evento' }, beforeEnter: AuthGuard },
        { path: 'events/new', name: 'NewEvent', component: NewEvent, meta: { title: 'Nueva Evento' }, beforeEnter: AuthGuard },
        { path: 'events/:id', name: 'Event', props: true, component: Event, meta: { title: 'Ver Evento' }, beforeEnter: AuthGuard },
        { path: 'home', name: 'Home', component: Home, meta: { title: 'Inicio' }, beforeEnter: AuthGuard },
        { path: 'memberships', name: 'Memberships', component: Memberships, meta: { title: 'Membresías' }, beforeEnter: AuthGuard },
        { path: 'memberships/edit/:id', props: true, name: 'EditMembership', component: EditMembership, meta: { title: 'Editar Membresía' }, beforeEnter: AuthGuard },
        { path: 'memberships/new', name: 'NewMembership', component: NewMembership, meta: { title: 'Nueva Membresía' }, beforeEnter: AuthGuard },
        { path: 'memberships/:id', name: 'Membership', props: true, component: Membership, meta: { title: 'Ver Membresía' }, beforeEnter: AuthGuard },
        { path: 'settings', name: 'Settings', component: Settings, meta: { title: 'Configuraciones' }, beforeEnter: AuthGuard },
        { path: 'userpayments', name: 'UserPayments', component: UserPayments, meta: { title: 'Pagos de clientes' }, beforeEnter: AuthGuard },
        { path: 'Userpayments/edit/:id', props: true, name: 'EditUserPayment', component: EditUserPayment, meta: { title: 'Editar Pago' }, beforeEnter: AuthGuard },
        { path: 'userpayments/new', name: 'NewUserPayment', component: NewUserPayment, meta: { title: 'Nuevo pago' }, beforeEnter: AuthGuard },
        { path: 'userpayments/:id', name: 'UserPayment', props: true, component: UserPayment, meta: { title: 'Ver pago' }, beforeEnter: AuthGuard },
        { path: 'users', name: 'Users', component: Users, meta: { title: 'Usuarios' }, beforeEnter: AuthGuard },
        { path: 'users/medicalconditions/:id', props: true, name: 'MedicalConditions', component: MedicalConditions, meta: { title: 'Condiciones Médicas' }, beforeEnter: AuthGuard },
        { path: 'users/edit/:id', props: true, name: 'EditUser', component: EditUser, meta: { title: 'Editar Usuario' }, beforeEnter: AuthGuard },
        { path: 'users/new', name: 'NewUser', component: NewUser, meta: { title: 'Nuevo Usuario' }, beforeEnter: AuthGuard },
        { path: 'users/bodymeasurements/new/:id', props: true, name: 'NewBodyMeasurement', component: NewBodyMeasurement, meta: { title: 'Nuevas Medidas Corporales' }, beforeEnter: AuthGuard },
        { path: 'users/:id', name: 'User', props: true, component: User, meta: { title: 'Ver Usuario' }, beforeEnter: AuthGuard }
      ]
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: { title: 'Iniciar sesión' },
      beforeEnter: LoginGuard
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.title
  next()
})

export default router
