import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import config from '../config'
import router from '../router'
Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    deletingBodyMeasurement: false,
    deletingEvent: false,
    deletingMembership: false,
    deletingUser: false,
    loadedAccessRecords: [],
    loadedBasicStatistics: {},
    loadedBodyMeasurements: [],
    loadedCategories: [],
    loadedEvent: {},
    loadedEvents: [],
    loadedMembership: {},
    loadedMemberships: [],
    loadedUser: {
      medicalConditions: {
        created: false,
        lesionOsea: {value: false, description: null},
        lesionMuscular: {value: false, description: null},
        enfermedadCardiovascular: {value: false, description: null},
        actividadDeportiva: {value: false, description: null}
      }
    },
    loadedUserPayment: {
      fkIdUser: {}
    },
    loadedUserPayments: [],
    loadedUsers: [],
    loadedCustomers: [],
    loadedInstructors: [],
    session: {
      isLoggedIn: localStorage.getItem('token')
    },
    showToast: {},
    uploadedPicture: null,
    userSession: {
      fkIdGym: {}
    },
    // loaders
    loadingData: false,
    uploadingPicture: false,
    // toast
    loading: false,
    error: null
  },
  mutations: {
    createEvent (state, payload) {
      state.loadedEvents.push(payload)
    },
    createMembership (state, payload) {
      state.loadedMemberships.push(payload)
    },
    createUserPayment (state, payload) {
      state.loadedUserPayments.push(payload)
    },
    createUser (state, payload) {
      state.loadedUsers.push(payload)
    },
    login (state, payload) {
      state.session.pending = true
    },
    loginSuccess (state) {
      state.session.isLoggedIn = true
      state.session.pending = false
    },
    logout (state, payload) {
      state.session.isLoggedIn = false
      state.userSession = {}
    },
    setDeletingBodyMeasurement (state, payload) {
      state.deletingBodyMeasurement = payload
    },
    setDeletingEvent (state, payload) {
      state.deletingEvent = payload
    },
    setDeletingMembership (state, payload) {
      state.deletingMembership = payload
    },
    setDeletingUser (state, payload) {
      state.deletingUser = payload
    },
    setLoadedAccessRecords (state, payload) {
      state.loadedAccessRecords = payload
    },
    setLoadedBasicStatistics (state, payload) {
      state.loadedBasicStatistics = payload
    },
    setLoadedBodyMeasurements (state, payload) {
      state.loadedBodyMeasurements = payload
    },
    setLoadedCategories (state, payload) {
      state.loadedCategories = payload
    },
    setLoadedEvent (state, payload) {
      state.loadedEvent = payload
    },
    setLoadedEvents (state, payload) {
      state.loadedEvents = payload
    },
    setLoadedMembership (state, payload) {
      state.loadedMembership = payload
    },
    setLoadedMemberships (state, payload) {
      state.loadedMemberships = payload
    },
    setLoadedUser (state, payload) {
      state.loadedUser = payload
    },
    setLoadedUserPayment (state, payload) {
      state.loadedUserPayment = payload
    },
    setLoadedUserPayments (state, payload) {
      state.loadedUserPayments = payload
    },
    setLoadedUsers (state, payload) {
      state.loadedUsers = payload
    },
    setLoadedCustomers (state, payload) {
      state.loadedCustomers = payload
    },
    setLoadedInstructors (state, payload) {
      state.loadedInstructors = payload
    },
    setUserSession (state, payload) {
      state.userSession = payload
    },
    // mutaciones para los loaders
    setLoadingData (state, payload) {
      state.loadingData = payload
    },
    setUploadingPicture (state, payload) {
      state.uploadingPicture = payload
    },
    clearUploadedPicture (state, payload) {
      state.uploadedPicture = null
    },
    // mutaciones para los toast
    clearError (state, payload) {
      state.error = null
    },
    setError (state, payload) {
      state.error = payload
    },
    setLoading (state, payload) {
      state.loading = payload
    },
    setUploadedPicture (state, payload) {
      state.uploadedPicture = payload
    }
  },
  actions: {
    changePassword ({commit, state}, payload) {
      const passwordData = {
        currentPasssword: payload.currentPasssword,
        newPassword: payload.newPassword
      }

      let request = {
        method: 'POST',
        url: `${config.baseUrl}/api/v1/login/changepassword`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        data: passwordData,
        json: true
      }

      axios(request).then(function (response) {
        state.showToast = {
          status: true,
          text: 'Contraseña cambiada exitosamente',
          timeout: 5000,
          color: 'green'
        }
      }).catch(function (err) {
        switch (err.response.data.error.error) {
          case 'bad_password':
            state.showToast = {
              status: true,
              text: 'Error de contraseña actual',
              timeout: 5000,
              color: 'red'
            }
            break
          default:
            state.showToast = {
              status: true,
              text: 'Ha ocurrido un error al intentar cambiar tu contraseña',
              timeout: 5000,
              color: 'red'
            }
        }
      })
    },
    clearError ({commit}) {
      commit('clearError')
    },
    uploadPicture ({commit}, payload) {
      commit('setUploadingPicture', true)
      let formData = new FormData()
      formData.append('urlPicture', payload)
      axios.post(`${config.baseUrl}/api/v1/users/uploadpicture`, formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }).then(function (response) {
        commit('setUploadedPicture', `${config.baseUrl}/${response.data}`)
        commit('setUploadingPicture', false)
      }).catch(function (err) {
        console.log(err)
        commit('setUploadingPicture', false)
      })
    },
    createBodyMeasurement ({commit}, payload) {
      const bodyMeasurement = {
        fkIdUser: payload.fkIdUser,
        weight: payload.weight,
        height: payload.height,
        chest: payload.chest,
        rightBicep: payload.rightBicep,
        leftBicep: payload.leftBicep,
        back: payload.back,
        shoulder: payload.shoulder,
        highAbdomen: payload.highAbdomen,
        middleAbdomen: payload.middleAbdomen,
        lowAbdomen: payload.lowAbdomen,
        gluteus: payload.gluteus,
        rightLeg: payload.rightLeg,
        leftLeg: payload.leftLeg,
        rightCalf: payload.rightCalf,
        leftCalf: payload.leftCalf,
        rightForearm: payload.rightForearm,
        leftForearm: payload.leftForearm,
        waist: payload.waist
      }

      let request = {
        method: 'POST',
        url: `${config.baseUrl}/api/v1/users/bodymeasurements`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        data: bodyMeasurement,
        json: true
      }

      axios(request).then(function (response) {
        router.push(`/users/${bodyMeasurement.fkIdUser}`)
      })
    },
    createEvent ({commit}, payload) {
      const event = {
        capacity: payload.capacity,
        dataName: payload.dataName,
        description: payload.description,
        fkIdCategory: payload.fkIdCategory,
        instructors: payload.instructors,
        price: payload.price,
        startDate: payload.startDate,
        ubication: payload.ubication
      }

      let request = {
        method: 'POST',
        url: `${config.baseUrl}/api/v1/events`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        data: event,
        json: true
      }

      axios(request).then(function (response) {
        router.push('/events')
      })
    },
    createMembership ({commit}, payload) {
      const membership = {
        dataName: payload.dataName,
        description: payload.description,
        durationInDays: payload.durationInDays,
        price: payload.price,
        status: payload.status
      }

      let request = {
        method: 'POST',
        url: `${config.baseUrl}/api/v1/memberships`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        data: membership,
        json: true
      }

      axios(request).then(function (response) {
        router.push('/memberships')
      })
    },
    clearUploadedPicture ({commit}) {
      commit('clearUploadedPicture')
    },
    createUserPayment ({commit}, payload) {
      const userPayment = {
        amount: payload.amount,
        fkIdMembership: payload.fkIdMembership,
        fkIdUser: payload.fkIdUser,
        paymentType: payload.paymentType,
        status: payload.status
      }

      let request = {
        method: 'POST',
        url: `${config.baseUrl}/api/v1/userpayments`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        data: userPayment,
        json: true
      }

      axios(request).then(function (response) {
        router.push('/userpayments')
      })
    },
    createUser ({commit}, payload) {
      const user = {
        birthday: payload.birthday,
        documentNumber: payload.documentNumber,
        documentType: payload.documentType,
        email: payload.email,
        emergencyPhone: payload.emergencyPhone,
        fullname: payload.fullname,
        haveInsurancePolicy: payload.haveInsurancePolicy,
        mainPhone: payload.mainPhone,
        nameInsurancePolicy: payload.nameInsurancePolicy,
        numberLifeInsurance: payload.numberLifeInsurance,
        observations: payload.observations,
        residenceAddress: payload.residenceAddress,
        role: payload.role,
        status: payload.status,
        username: payload.username,
        workAddress: payload.workAddress
      }

      let request = {
        method: 'POST',
        url: `${config.baseUrl}/api/v1/users`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        data: user,
        json: true
      }

      axios(request).then(function (response) {
        router.push('/users')
      })
    },
    deleteBodyMeasurement ({commit, dispatch}, payload) {
      const bodyMeasurementId = payload.bodyMeasurementId

      let request = {
        method: 'DELETE',
        url: `${config.baseUrl}/api/v1/users/bodymeasurements/${bodyMeasurementId}`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }

      commit('setDeletingBodyMeasurement', true)

      axios(request).then(function (response) {
        if (response.status === 204) {
          dispatch('loadBodyMeasurements', payload.fkIdUser)
          commit('setDeletingBodyMeasurement', false)
        }
      }).catch(function (error) {
        console.log(error)
        commit('setDeletingBodyMeasurement', false)
      })
    },
    deleteEvent ({commit, dispatch}, payload) {
      const eventId = payload

      let request = {
        method: 'DELETE',
        url: `${config.baseUrl}/api/v1/events/${eventId}`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }

      commit('setDeletingEvent', true)

      axios(request).then(function (response) {
        if (response.status === 204) {
          dispatch('loadEvents')
          commit('setDeletingEvent', false)
        }
      }).catch(function (error) {
        console.log(error)
        commit('setDeletingEvent', false)
      })
    },
    deleteMembership ({commit, dispatch}, payload) {
      const membershipId = payload

      let request = {
        method: 'DELETE',
        url: `${config.baseUrl}/api/v1/memberships/${membershipId}`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }

      commit('setDeletingMembership', true)

      axios(request).then(function (response) {
        if (response.status === 204) {
          dispatch('loadMemberships')
          commit('setDeletingMembership', false)
        }
      }).catch(function (error) {
        console.log(error)
        commit('setDeletingMembership', false)
      })
    },
    deleteUser ({commit, dispatch}, payload) {
      const userId = payload

      let request = {
        method: 'DELETE',
        url: `${config.baseUrl}/api/v1/users/${userId}`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }

      commit('setDeletingUser', true)

      axios(request).then(function (response) {
        if (response.status === 204) {
          dispatch('loadUsers')
          commit('setDeletingUser', false)
        }
      }).catch(function (error) {
        console.log(error)
        commit('setDeletingUser', false)
      })
    },
    editEvent ({commit}, payload) {
      const eventData = {
        _id: payload._id,
        capacity: payload.capacity,
        dataName: payload.dataName,
        description: payload.description,
        instructors: payload.instructors,
        price: payload.price,
        ubication: payload.ubication
      }

      let request = {
        method: 'PUT',
        url: `${config.baseUrl}/api/v1/events`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        data: eventData,
        json: true
      }

      axios(request).then(function (response) {
        commit('setLoadedEvent', {})
        router.push('/events')
      }).catch(function (error) {
        console.log(error)
      })
    },
    editMembership ({commit}, payload) {
      const membershipData = {
        _id: payload._id,
        description: payload.description,
        durationInDays: payload.durationInDays,
        price: payload.price,
        status: payload.status
      }

      let request = {
        method: 'PUT',
        url: `${config.baseUrl}/api/v1/memberships`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        data: membershipData,
        json: true
      }

      axios(request).then(function (response) {
        commit('setLoadedMembership', {})
        router.push('/memberships')
      }).catch(function (error) {
        console.log(error)
      })
    },
    editUser ({commit, dispatch, state}, payload) {
      const userData = {
        _id: payload._id,
        emergencyPhone: payload.emergencyPhone,
        fullname: payload.fullname,
        haveInsurancePolicy: payload.haveInsurancePolicy,
        mainPhone: payload.mainPhone,
        nameInsurancePolicy: payload.nameInsurancePolicy,
        numberLifeInsurance: payload.numberLifeInsurance,
        observations: payload.observations,
        residenceAddress: payload.residenceAddress,
        urlPicture: (state.uploadedPicture) ? state.uploadedPicture : payload.urlPicture,
        status: payload.status,
        workAddress: payload.workAddress
      }

      let request = {
        method: 'PUT',
        url: `${config.baseUrl}/api/v1/users`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        data: userData,
        json: true
      }

      axios(request).then(function (response) {
        commit('clearUploadedPicture')
        if (state.userSession._id === userData._id) {
          dispatch('loadUserSession')
          router.push('/settings')
        } else {
          router.push('/users')
        }
      }).catch(function (error) {
        console.log(error)
      })
    },
    editUserPayment ({commit}, payload) {
      const userPaymentData = {
        _id: payload._id,
        paymentType: payload.paymentType,
        status: payload.status
      }

      let request = {
        method: 'PUT',
        url: `${config.baseUrl}/api/v1/userpayments`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        data: userPaymentData,
        json: true
      }

      axios(request).then(function (response) {
        commit('setLoadedUserPayment', {})
        router.push('/userpayments')
      }).catch(function (error) {
        console.log(error)
      })
    },
    login ({commit}, payload) {
      commit('setLoading', true)
      commit('login', null)
      // simulacion de llamada al api exitoso
      axios.post(`${config.baseUrl}/api/v1/login`, {
        username: payload.username,
        password: payload.password
      })
      .then(function (response) {
        if (response.data.status) {
          localStorage.setItem('token', response.data.token)
          localStorage.setItem('id', response.data.id)
          commit('clearError')
          commit('setLoading', false)
          commit('loginSuccess', null)
        }
      })
      .catch(function (error) {
        commit('setLoading', false)
        switch (error.response.data.error.message) {
          case 'incorrect_password':
            commit('setError', 'Contraseña incorrecta')
            break
          case 'unauthorized':
            commit('setError', 'Acceso no autorizado')
            break
          case 'user_not_found':
            commit('setError', 'Usuario no encontrado')
            break
          default:
            commit('setError', 'Ha ocurrido un error')
        }
      })
    },
    logout ({commit}) {
      localStorage.removeItem('id')
      localStorage.removeItem('token')
      commit('setLoadedCategories', [])
      commit('setLoadedCustomers', [])
      commit('setLoadedEvents', [])
      commit('setLoadedMemberships', [])
      commit('setLoadedInstructors', [])
      commit('setLoadedUserPayments', [])
      commit('setLoadedUsers', [])
      commit('logout', null)
    },
    loadAccessRecords ({commit}) {
      commit('setLoadingData', true)
      let request = {
        method: 'GET',
        url: `${config.baseUrl}/api/v1/access`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }
      axios(request).then(function (response) {
        commit('setLoadedAccessRecords', response.data)
        commit('setLoadingData', false)
      }).catch(function (err) {
        commit('setLoadingData', false)
        console.log(err)
      })
    },
    loadBasicStatistics ({commit, state}) {
      if (state.userSession.fkIdGym._id) {
        let request = {
          method: 'GET',
          url: `${config.baseUrl}/api/v1/gyms/basicstatistics/${state.userSession.fkIdGym._id}`,
          headers: {
            'Authorization': localStorage.getItem('token'),
            'X-User-Id': localStorage.getItem('id')
          },
          json: true
        }
        axios(request).then(function (response) {
          commit('setLoadedBasicStatistics', response.data)
        })
      }
    },
    loadBodyMeasurements ({commit}, payload) {
      let request = {
        method: 'GET',
        url: `${config.baseUrl}/api/v1/users/bodymeasurements/${payload}`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }
      axios(request).then(function (response) {
        commit('setLoadedBodyMeasurements', response.data)
      })
    },
    loadCategories ({commit}) {
      let request = {
        method: 'GET',
        url: `${config.baseUrl}/api/v1/events/categories`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }
      axios(request).then(function (response) {
        commit('setLoadedCategories', response.data)
      })
    },
    loadEvent ({commit}, payload) {
      let request = {
        method: 'GET',
        url: `${config.baseUrl}/api/v1/events/${payload}`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }
      axios(request).then(function (response) {
        commit('setLoadedEvent', response.data)
      }).catch(function (err) {
        console.log(err)
      })
    },
    loadEvents ({commit}) {
      commit('setLoadingData', true)
      let request = {
        method: 'GET',
        url: `${config.baseUrl}/api/v1/events`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }
      axios(request).then(function (response) {
        commit('setLoadedEvents', response.data)
        commit('setLoadingData', false)
      })
    },
    loadMembership ({commit}, payload) {
      let request = {
        method: 'GET',
        url: `${config.baseUrl}/api/v1/memberships/${payload}`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }
      axios(request).then(function (response) {
        commit('setLoadedMembership', response.data)
      }).catch(function (err) {
        console.log(err)
      })
    },
    loadMemberships ({commit}) {
      commit('setLoadingData', true)
      let request = {
        method: 'GET',
        url: `${config.baseUrl}/api/v1/memberships`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }
      axios(request).then(function (response) {
        commit('setLoadedMemberships', response.data)
        commit('setLoadingData', false)
      })
    },
    loadUser ({commit}, payload) {
      let request = {
        method: 'GET',
        url: `${config.baseUrl}/api/v1/users/${payload}`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }
      axios(request).then(function (response) {
        commit('setLoadedUser', response.data)
      }).catch(function (err) {
        console.log(err)
      })
    },
    loadUserPayment ({commit}, payload) {
      let request = {
        method: 'GET',
        url: `${config.baseUrl}/api/v1/userpayments/${payload}`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }
      axios(request).then(function (response) {
        commit('setLoadedUserPayment', response.data)
      }).catch(function (err) {
        console.log(err)
      })
    },
    loadUserPayments ({commit}) {
      commit('setLoadingData', true)
      let request = {
        method: 'GET',
        url: `${config.baseUrl}/api/v1/userpayments`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }
      axios(request).then(function (response) {
        commit('setLoadedUserPayments', response.data)
        commit('setLoadingData', false)
      })
    },
    loadUsers ({commit}) {
      commit('setLoadingData', true)
      let request = {
        method: 'GET',
        url: `${config.baseUrl}/api/v1/users`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }
      axios(request).then(function (response) {
        commit('setLoadedUsers', response.data)

        let customers = []
        response.data.forEach((user) => {
          if (user.role === 'customer') {
            customers.push(user)
          }
        })
        commit('setLoadedCustomers', customers)

        let instructors = []
        response.data.forEach((user) => {
          if (user.role === 'instructor') {
            instructors.push(user)
          }
        })
        commit('setLoadedInstructors', instructors)
        commit('setLoadingData', false)
      })
    },
    loadUserSession ({commit}) {
      let request = {
        method: 'GET',
        url: `${config.baseUrl}/api/v1/users/${localStorage.getItem('id')}`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }
      axios(request).then(function (response) {
        commit('setUserSession', response.data)
      })
    },
    updateMedicalConditions ({commit}, payload) {
      const medicalConditions = {
        _id: payload._id,
        medicalConditions: payload.medicalConditions
      }

      let request = {
        method: 'PUT',
        url: `${config.baseUrl}/api/v1/users/medicalconditions`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        data: medicalConditions,
        json: true
      }

      axios(request).then(function (response) {
        router.push('/users/' + payload._id)
      })
    }
  },
  getters: {
    // loaders de los registros de acceso
    loadedAccessRecord (state) {
      return (accessId) => {
        return state.loadedAccessRecords.find((accessRecord) => {
          return accessRecord._id === accessId
        })
      }
    },
    loadedAccessRecords (state) {
      return state.loadedAccessRecords.sort()
    },
    loadedBasicStatistics (state) {
      return state.loadedBasicStatistics
    },
    loadedBodyMeasurements (state) {
      return state.loadedBodyMeasurements
    },
    loadedCategories (state) {
      return state.loadedCategories.sort()
    },
    // loaders de los eventos
    loadedEvent (state) {
      return state.loadedEvent
    },
    loadedEvents (state) {
      return state.loadedEvents.sort()
    },
    // loaders de las membresías
    loadedMembership (state) {
      return state.loadedMembership
    },
    loadedMemberships (state) {
      return state.loadedMemberships.sort()
    },
    // loaders de los pagos de usuarios
    loadedUserPayment (state) {
      return state.loadedUserPayment
    },
    loadedUserPayments (state) {
      return state.loadedUserPayments.sort()
    },
    // loaders de los usuarios
    loadedUser (state) {
      return state.loadedUser
    },
    loadedUsers (state) {
      return state.loadedUsers.sort()
    },
    loadedCustomers (state) {
      return state.loadedCustomers.sort()
    },
    loadedInstructors (state) {
      return state.loadedInstructors.sort()
    },
    session (state) {
      return state.session
    },
    userSession (state) {
      return state.userSession
    },
    // getters de los loaders
    loadingData (state) {
      return state.loadingData
    },
    uploadingPicture (state) {
      return state.uploadingPicture
    },
    // getters de los alerts
    error (state) {
      return state.error
    },
    loading (state) {
      return state.loading
    }
  }
})
