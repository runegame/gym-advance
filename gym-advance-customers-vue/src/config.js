'use stric'

const config = {
  baseUrl: 'https://sc.gymadvance.com'
}

if (process.env.NODE_ENV === 'development') {
  config.baseUrl = 'http://localhost:5050'
}

module.exports = config
