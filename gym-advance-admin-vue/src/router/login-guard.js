import { store } from '../store'

export default (to, from, next) => {
  if (store.getters.session.isLoggedIn) {
    next('/')
  } else {
    next()
  }
}
