import Vue from 'vue'
import Router from 'vue-router'
import EditGym from '@/components/Gyms/EditGym.vue'
import EditPlan from '@/components/Plans/EditPlan.vue'
import EditUser from '@/components/Users/EditUser.vue'
import EditGymPayment from '@/components/GymPayments/EditGymPayment.vue'
import GymPayment from '@/components/GymPayments/GymPayment.vue'
import GymPayments from '@/components/GymPayments/GymPayments.vue'
import Gym from '@/components/Gyms/Gym.vue'
import Gyms from '@/components/Gyms/Gyms.vue'
import Main from '@/components/Main.vue'
import Plan from '@/components/Plans/Plan.vue'
import Plans from '@/components/Plans/Plans.vue'
import MedicalConditions from '@/components/Users/MedicalConditions.vue'
import NewGym from '@/components/Gyms/NewGym.vue'
import NewGymPayment from '@/components/GymPayments/NewGymPayment.vue'
import NewPlan from '@/components/Plans/NewPlan.vue'
import NewUser from '@/components/Users/NewUser.vue'
import Settings from '@/components/Settings'
import Login from '@/components/Login'
import UserPayment from '@/components/UserPayments/UserPayment.vue'
import UserPayments from '@/components/UserPayments/UserPayments.vue'
import User from '@/components/Users/User.vue'
import Users from '@/components/Users/Users.vue'
import AuthGuard from './auth-guard'
import LoginGuard from './login-guard'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main,
      meta: { title: 'Inicio' },
      beforeEnter: AuthGuard,
      children: [
        { path: 'gympayments', name: 'GymPayments', component: GymPayments, meta: { title: 'Pagos de gimnasios' }, beforeEnter: AuthGuard },
        { path: 'gympayments/edit/:id', props: true, name: 'EditGymPayment', component: EditGymPayment, meta: { title: 'Editar Pago' }, beforeEnter: AuthGuard },
        { path: 'gympayments/new', name: 'NewGymPayment', component: NewGymPayment, meta: { title: 'Nuevo Pago de Gimnasio' }, beforeEnter: AuthGuard },
        { path: 'gympayments/:id', name: 'GymPayment', props: true, component: GymPayment, meta: { title: 'Ver pago' }, beforeEnter: AuthGuard },
        { path: 'gyms', name: 'Gyms', component: Gyms, meta: { title: 'Gimnasios' }, beforeEnter: AuthGuard },
        { path: 'gyms/edit/:id', props: true, name: 'EditGym', component: EditGym, meta: { title: 'Editar Gimnasio' }, beforeEnter: AuthGuard },
        { path: 'gyms/new', name: 'NewGym', component: NewGym, meta: { title: 'Nuevo Gimnasio' }, beforeEnter: AuthGuard },
        { path: 'gyms/:id', name: 'Gym', props: true, component: Gym, meta: { title: 'Ver Gimnasio' }, beforeEnter: AuthGuard },
        { path: 'plans', name: 'Plans', component: Plans, meta: { title: 'Planes' }, beforeEnter: AuthGuard },
        { path: 'plans/edit/:id', props: true, name: 'EditPlan', component: EditPlan, meta: { title: 'Editar Plan' }, beforeEnter: AuthGuard },
        { path: 'plans/new', name: 'NewPlan', component: NewPlan, meta: { title: 'Nuevo Plan' }, beforeEnter: AuthGuard },
        { path: 'plans/:id', name: 'Plan', props: true, component: Plan, meta: { title: 'Ver Plan' }, beforeEnter: AuthGuard },
        { path: 'settings', name: 'Settings', component: Settings, meta: { title: 'Configuraciones' }, beforeEnter: AuthGuard },
        { path: 'userpayments', name: 'UserPayments', component: UserPayments, meta: { title: 'Pagos de clientes' }, beforeEnter: AuthGuard },
        { path: 'userpayments/:id', name: 'UserPayment', props: true, component: UserPayment, meta: { title: 'Ver pago' }, beforeEnter: AuthGuard },
        { path: 'users', name: 'Users', component: Users, meta: { title: 'Usuarios' }, beforeEnter: AuthGuard },
        { path: 'users/medicalconditions/:id', props: true, name: 'MedicalConditions', component: MedicalConditions, meta: { title: 'Condiciones Médicas' }, beforeEnter: AuthGuard },
        { path: 'users/edit/:id', props: true, name: 'EditUser', component: EditUser, meta: { title: 'Editar Usuario' }, beforeEnter: AuthGuard },
        { path: 'users/new', name: 'NewUser', component: NewUser, meta: { title: 'Nuevo Usuario' }, beforeEnter: AuthGuard },
        { path: 'users/:id', name: 'User', props: true, component: User, meta: { title: 'Ver Usuario' }, beforeEnter: AuthGuard }
      ]
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: { title: 'Iniciar sesión' },
      beforeEnter: LoginGuard
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.title
  next()
})

export default router
