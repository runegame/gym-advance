'use stric'

const config = {
  baseUrl: 'https://sa.gymadvance.com'
}

if (process.env.NODE_ENV === 'development') {
  config.baseUrl = 'http://localhost:5051'
}

module.exports = config
