import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import config from '../config'
import router from '../router'
Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    deletingGym: false,
    deletingPlan: false,
    deletingUser: false,
    loadedGym: {
      branch: true,
      fkIdPlan: ''
    },
    loadedGymPayment: {},
    loadedGymPayments: [],
    loadedGyms: [],
    loadedPlan: {},
    loadedPlans: [],
    loadedUser: {
      medicalConditions: {
        created: false,
        lesionOsea: {value: false, description: null},
        lesionMuscular: {value: false, description: null},
        enfermedadCardiovascular: {value: false, description: null},
        actividadDeportiva: {value: false, description: null}
      }
    },
    loadedUserPayment: {
      fkIdGym: '',
      fkIdMembership: '',
      fkIdUser: ''
    },
    loadedUserPayments: [],
    loadedUsers: [],
    session: {
      isLoggedIn: localStorage.getItem('token')
    },
    showToast: {},
    uploadedPicture: null,
    userSession: {},
    // loaders
    loadingData: false,
    uploadingPicture: false,
    // toast
    loading: false,
    error: null
  },
  mutations: {
    createGym (state, payload) {
      state.loadedGyms.push(payload)
    },
    createGymPayment (state, payload) {
      state.loadedGymPayments.push(payload)
    },
    createUser (state, payload) {
      state.loadedUsers.push(payload)
    },
    login (state, payload) {
      state.session.pending = true
    },
    loginSuccess (state) {
      state.session.isLoggedIn = true
      state.session.pending = false
    },
    logout (state, payload) {
      state.session.isLoggedIn = false
      state.userSession = {}
    },
    setDeletingGym (state, payload) {
      state.deletingGym = payload
    },
    setDeletingPlan (state, payload) {
      state.deletingPlan = payload
    },
    setDeletingUser (state, payload) {
      state.deletingUser = payload
    },
    setLoadedGym (state, payload) {
      state.loadedGym = payload
    },
    setLoadedGymPayments (state, payload) {
      state.loadedGymPayments = payload
    },
    setLoadedGyms (state, payload) {
      state.loadedGyms = payload
    },
    setLoadedPlan (state, payload) {
      state.loadedPlan = payload
    },
    setLoadedPlans (state, payload) {
      state.loadedPlans = payload
    },
    setLoadedUser (state, payload) {
      state.loadedUser = payload
    },
    setLoadedUserPayment (state, payload) {
      state.loadedUserPayment = payload
    },
    setLoadedUserPayments (state, payload) {
      state.loadedUserPayments = payload
    },
    setLoadedUsers (state, payload) {
      state.loadedUsers = payload
    },
    setMedicalConditions (state, payload) {
      state.loadedUser.medicalConditions = payload
    },
    setUserSession (state, payload) {
      state.userSession = payload
    },
    // mutaciones para los loaders
    setLoadingData (state, payload) {
      state.loadingData = payload
    },
    setUploadingPicture (state, payload) {
      state.uploadingPicture = payload
    },
    clearUploadedPicture (state, payload) {
      state.uploadedPicture = null
    },
    // mutaciones para los toast
    clearError (state, payload) {
      state.error = null
    },
    setError (state, payload) {
      state.error = payload
    },
    setLoading (state, payload) {
      state.loading = payload
    },
    setUploadedPicture (state, payload) {
      state.uploadedPicture = payload
    }
  },
  actions: {
    changePassword ({commit, state}, payload) {
      const passwordData = {
        currentPasssword: payload.currentPasssword,
        newPassword: payload.newPassword
      }

      let request = {
        method: 'POST',
        url: `${config.baseUrl}/api/v1/login/changepassword`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        data: passwordData,
        json: true
      }

      axios(request).then(function (response) {
        state.showToast = {
          status: true,
          text: 'Contraseña cambiada exitosamente',
          timeout: 5000,
          color: 'green'
        }
      }).catch(function (err) {
        switch (err.response.data.error.error) {
          case 'bad_password':
            state.showToast = {
              status: true,
              text: 'Error de contraseña actual',
              timeout: 5000,
              color: 'red'
            }
            break
          default:
            state.showToast = {
              status: true,
              text: 'Ha ocurrido un error al intentar cambiar tu contraseña',
              timeout: 5000,
              color: 'red'
            }
        }
      })
    },
    clearError ({commit}) {
      commit('clearError')
    },
    clearUploadedPicture ({commit}) {
      commit('clearUploadedPicture')
    },
    createGym ({commit, state}, payload) {
      const gym = {
        branch: payload.branch,
        capacity: payload.capacity,
        dataName: payload.dataName,
        email: payload.email,
        location: payload.location,
        phone: payload.phone,
        private: payload.private,
        status: payload.status
      }

      let request = {
        method: 'POST',
        url: `${config.baseUrl}/api/v1/gyms`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        data: gym,
        json: true
      }

      axios(request).then(function (response) {
        router.push('/gyms')
      })
    },
    createGymPayment ({commit, state}, payload) {
      const gymPayment = {
        amount: payload.amount,
        fkIdGym: payload.fkIdGym,
        fkIdPlan: payload.fkIdPlan,
        paymentType: payload.paymentType,
        status: payload.status
      }

      let request = {
        method: 'POST',
        url: `${config.baseUrl}/api/v1/gympayments`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        data: gymPayment,
        json: true
      }

      axios(request).then(function (response) {
        router.push('/gympayments')
      })
    },
    createPlan ({commit, state}, payload) {
      const plan = {
        dataName: payload.dataName,
        description: payload.description,
        durationInDays: payload.durationInDays,
        price: payload.price,
        private: false,
        status: payload.status
      }

      let request = {
        method: 'POST',
        url: `${config.baseUrl}/api/v1/plans`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        data: plan,
        json: true
      }

      axios(request).then(function (response) {
        router.push('/plans')
      })
    },
    uploadPicture ({commit}, payload) {
      commit('setUploadingPicture', true)
      let formData = new FormData()
      formData.append('urlPicture', payload)
      axios.post(`${config.baseUrl}/api/v1/users/uploadpicture`, formData, {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }).then(function (response) {
        commit('setUploadedPicture', `${config.baseUrl}/${response.data}`)
        commit('setUploadingPicture', false)
      }).catch(function (err) {
        console.log(err)
        commit('setUploadingPicture', false)
      })
    },
    createUser ({commit, state}, payload) {
      const user = {
        birthday: payload.birthday,
        documentNumber: payload.documentNumber,
        documentType: payload.documentType,
        email: payload.email,
        emergencyPhone: payload.emergencyPhone,
        fkIdGym: payload.fkIdGym,
        fullname: payload.fullname,
        haveInsurancePolicy: payload.haveInsurancePolicy,
        mainPhone: payload.mainPhone,
        nameInsurancePolicy: payload.nameInsurancePolicy,
        numberLifeInsurance: payload.numberLifeInsurance,
        observations: payload.observations,
        residenceAddress: payload.residenceAddress,
        role: payload.role,
        status: payload.status,
        urlPicture: state.uploadedPicture,
        username: payload.username,
        workAddress: payload.workAddress
      }

      let request = {
        method: 'POST',
        url: `${config.baseUrl}/api/v1/users`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        data: user,
        json: true
      }

      axios(request).then(function (response) {
        commit('clearUploadedPicture')
        router.push('/users')
      })
    },
    deleteGym ({commit, dispatch}, payload) {
      const gymId = payload

      let request = {
        method: 'DELETE',
        url: `${config.baseUrl}/api/v1/gyms/${gymId}`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }

      commit('setDeletingGym', true)

      axios(request).then(function (response) {
        if (response.status === 204) {
          dispatch('loadGyms')
          commit('setDeletingGym', false)
        }
      }).catch(function (error) {
        console.log(error)
        commit('setDeletingGym', false)
      })
    },
    deletePlan ({commit, dispatch}, payload) {
      const planId = payload

      let request = {
        method: 'DELETE',
        url: `${config.baseUrl}/api/v1/plans/${planId}`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }

      commit('setDeletingPlan', true)

      axios(request).then(function (response) {
        if (response.status === 204) {
          dispatch('loadPlans')
          commit('setDeletingPlan', false)
        }
      }).catch(function (error) {
        console.log(error)
        commit('setDeletingPlan', false)
      })
    },
    deleteUser ({commit, dispatch}, payload) {
      const userId = payload

      let request = {
        method: 'DELETE',
        url: `${config.baseUrl}/api/v1/users/${userId}`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }

      commit('setDeletingUser', true)

      axios(request).then(function (response) {
        if (response.status === 204) {
          dispatch('loadUsers')
          commit('setDeletingUser', false)
        }
      }).catch(function (error) {
        console.log(error)
        commit('setDeletingUser', false)
      })
    },
    editGym ({commit, dispatch, state}, payload) {
      const gymData = {
        _id: payload._id,
        branch: payload.branch,
        capacity: payload.capacity,
        dataName: payload.dataName,
        email: payload.email,
        location: payload.location,
        phone: payload.phone,
        status: payload.status
      }

      let request = {
        method: 'PUT',
        url: `${config.baseUrl}/api/v1/gyms`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        data: gymData,
        json: true
      }

      axios(request).then(function (response) {
        commit('setLoadedGym', {})
        router.push('/gyms')
      }).catch(function (error) {
        console.log(error)
      })
    },
    editPlan ({commit, dispatch, state}, payload) {
      const planData = {
        _id: payload._id,
        description: payload.description,
        durationInDays: payload.durationInDays,
        price: payload.price,
        status: payload.status
      }

      let request = {
        method: 'PUT',
        url: `${config.baseUrl}/api/v1/plans`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        data: planData,
        json: true
      }

      axios(request).then(function (response) {
        commit('setLoadedPlan', {})
        router.push('/plans')
      }).catch(function (error) {
        console.log(error)
      })
    },
    editUser ({commit, dispatch, state}, payload) {
      const userData = {
        _id: payload._id,
        emergencyPhone: payload.emergencyPhone,
        fullname: payload.fullname,
        haveInsurancePolicy: payload.haveInsurancePolicy,
        mainPhone: payload.mainPhone,
        nameInsurancePolicy: payload.nameInsurancePolicy,
        numberLifeInsurance: payload.numberLifeInsurance,
        observations: payload.observations,
        residenceAddress: payload.residenceAddress,
        urlPicture: (state.uploadedPicture) ? state.uploadedPicture : payload.urlPicture,
        status: payload.status,
        workAddress: payload.workAddress
      }

      let request = {
        method: 'PUT',
        url: `${config.baseUrl}/api/v1/users`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        data: userData,
        json: true
      }

      axios(request).then(function (response) {
        commit('clearUploadedPicture')
        if (state.userSession._id === userData._id) {
          dispatch('loadUserSession')
          router.push('/settings')
        } else {
          router.push('/users')
        }
      }).catch(function (error) {
        console.log(error)
      })
    },
    login ({commit}, payload) {
      commit('setLoading', true)
      commit('clearError')
      commit('login', null)
      axios.post(`${config.baseUrl}/api/v1/login`, {
        username: payload.username,
        password: payload.password
      })
      .then(function (response) {
        if (response.data.status) {
          localStorage.setItem('token', response.data.token)
          localStorage.setItem('id', response.data.id)
          commit('clearError')
          commit('setLoading', false)
          commit('loginSuccess', null)
        }
      })
      .catch(function (error) {
        commit('setLoading', false)
        switch (error.response.data.error.message) {
          case 'incorrect_password':
            commit('setError', 'Contraseña incorrecta')
            break
          case 'unauthorized':
            commit('setError', 'Acceso no autorizado')
            break
          case 'user_not_found':
            commit('setError', 'Usuario no encontrado')
            break
          default:
            commit('setError', 'Ha ocurrido un error')
        }
      })
    },
    logout ({commit}) {
      localStorage.removeItem('id')
      localStorage.removeItem('token')
      commit('setLoadedUserPayments', [])
      commit('setLoadedUsers', [])
      commit('logout', null)
    },
    loadGym ({commit}, payload) {
      let request = {
        method: 'GET',
        url: `${config.baseUrl}/api/v1/gyms/${payload}`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }
      axios(request).then(function (response) {
        commit('setLoadedGym', response.data)
      }).catch(function (err) {
        console.log(err)
      })
    },
    loadGymPayments ({commit}) {
      commit('setLoadingData', true)
      let request = {
        method: 'GET',
        url: `${config.baseUrl}/api/v1/gympayments`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }
      axios(request).then(function (response) {
        commit('setLoadedGymPayments', response.data)
        commit('setLoadingData', false)
      })
    },
    loadGyms ({commit}) {
      commit('setLoadingData', true)
      let request = {
        method: 'GET',
        url: `${config.baseUrl}/api/v1/gyms`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }
      axios(request).then(function (response) {
        commit('setLoadedGyms', response.data)
        commit('setLoadingData', false)
      })
    },
    loadPlan ({commit}, payload) {
      let request = {
        method: 'GET',
        url: `${config.baseUrl}/api/v1/plans/${payload}`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }
      axios(request).then(function (response) {
        commit('setLoadedPlan', response.data)
      }).catch(function (err) {
        console.log(err)
      })
    },
    loadPlans ({commit}) {
      commit('setLoadingData', true)
      let request = {
        method: 'GET',
        url: `${config.baseUrl}/api/v1/plans`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }
      axios(request).then(function (response) {
        commit('setLoadedPlans', response.data)
        commit('setLoadingData', false)
      })
    },
    loadUser ({commit}, payload) {
      let request = {
        method: 'GET',
        url: `${config.baseUrl}/api/v1/users/${payload}`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }
      axios(request).then(function (response) {
        commit('setLoadedUser', response.data)
      }).catch(function (err) {
        console.log(err)
      })
    },
    loadUserPayment ({commit}, payload) {
      let request = {
        method: 'GET',
        url: `${config.baseUrl}/api/v1/userpayments/${payload}`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }
      axios(request).then(function (response) {
        commit('setLoadedUserPayment', response.data)
      }).catch(function (err) {
        console.log(err)
      })
    },
    loadUserPayments ({commit}) {
      commit('setLoadingData', true)
      let request = {
        method: 'GET',
        url: `${config.baseUrl}/api/v1/userpayments`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }
      axios(request).then(function (response) {
        commit('setLoadedUserPayments', response.data)
        commit('setLoadingData', false)
      })
    },
    loadUsers ({commit}) {
      commit('setLoadingData', true)
      let request = {
        method: 'GET',
        url: `${config.baseUrl}/api/v1/users`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }
      axios(request).then(function (response) {
        commit('setLoadedUsers', response.data)
        commit('setLoadingData', false)
      })
    },
    loadUserSession ({commit}) {
      let request = {
        method: 'GET',
        url: `${config.baseUrl}/api/v1/users/${localStorage.getItem('id')}`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        json: true
      }
      axios(request).then(function (response) {
        commit('setUserSession', response.data)
      })
    },
    updateMedicalConditions ({commit}, payload) {
      const medicalConditions = {
        _id: payload._id,
        medicalConditions: payload.medicalConditions
      }

      let request = {
        method: 'PUT',
        url: `${config.baseUrl}/api/v1/users/medicalconditions`,
        headers: {
          'Authorization': localStorage.getItem('token'),
          'X-User-Id': localStorage.getItem('id')
        },
        data: medicalConditions,
        json: true
      }

      axios(request).then(function (response) {
        router.push('/users/' + payload._id)
      })
    }
  },
  getters: {
    // loaders de los pagos de gimnasios
    loadedGymPayment (state) {
      return state.loadedGymPayment
    },
    loadedGymPayments (state) {
      return state.loadedGymPayments.sort()
    },
    // loaders de los gimnasios
    loadedGyms (state) {
      return state.loadedGyms.sort()
    },
    // loaders de los pagos de usuarios
    loadedUserPayment (state) {
      return state.loadedUserPayment
    },
    loadedUserPayments (state) {
      return state.loadedUserPayments.sort()
    },
    loadedPlan (state) {
      return state.loadedPlan
    },
    loadedPlans (state) {
      return state.loadedPlans.sort()
    },
    // loaders de los usuarios
    loadedUser (state) {
      return state.loadedUser
    },
    loadedUsers (state) {
      return state.loadedUsers.sort()
    },
    session (state) {
      return state.session
    },
    uploadPicture (state) {
      return state.uploadedPicture
    },
    userSession (state) {
      return state.userSession
    },
    // getters de los loaders
    loadingData (state) {
      return state.loadingData
    },
    uploadingPicture (state) {
      return state.uploadingPicture
    },
    // getters de los alerts
    error (state) {
      return state.error
    },
    loading (state) {
      return state.loading
    }
  }
})
