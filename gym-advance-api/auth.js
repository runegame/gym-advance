'use strict'

import { send, json } from 'micro'
import HttpHash from 'http-hash'
import { authRepository } from 'gym-advance-db'
import config from './config'
import utils from './lib/utils'

const hash = HttpHash()

hash.set('POST /admin', async function authenticateAdm (req, res, params) {
  let credentials = await json(req)

  authRepository.authenticateAdmin(credentials.username, credentials.password, function (err, auth) {
    if (err) return (err, null)

    if (!auth.status) {
      return send(res, 401, auth)
    }

    utils.signToken({fkIdGym: auth.fkIdGym, userId: auth.userId}, config.secret, null, (err, token) => {
      if (err) return send(res, 500, err)
      send(res, 200, {status: true, id: auth.userId, token: token})
    })
  })
})

hash.set('POST /user', async function authenticateUsr (req, res, params) {
  let credentials = await json(req)

  authRepository.authenticateUser(credentials.username, credentials.password, function (err, auth) {
    if (err) return (err, null)

    if (!auth.status) {
      return send(res, 401, auth)
    }

    utils.signToken({fkIdGym: auth.fkIdGym, userId: auth.userId}, config.secret, null, (err, token) => {
      if (err) return send(res, 500, err)
      send(res, 200, {status: true, id: auth.userId, token: token})
    })
  })
})

hash.set('POST /access/:documentNumber', async function accessCustomer (req, res, params) {
  let documentNumber = params.documentNumber
  authRepository.accessCustomer(documentNumber, function (err, response) {
    if (err) return (err, null)

    if (!response.status) {
      return send(res, 401, {error: response.message})
    }
    send(res, 200, response)
  })
})

hash.set('GET /access/records', async function getAccessRecordsByGym (req, res, params) {
  let userId = req.headers['x-user-id']

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      let fkIdGym = decoded.fkIdGym
      authRepository.getAccessRecordsByGym(fkIdGym, function (err, records) {
        if (err) return (err, null)
        send(res, 200, records)
      })
    })
  })
})

export default async function main (req, res) {
  let { method, url } = req
  let match = hash.get(`${method.toUpperCase()} ${url}`)

  if (match.handler) {
    try {
      await match.handler(req, res, match.params)
    } catch (e) {
      send(res, 500, { error: e.message })
    }
  } else {
    send(res, 404, { error: 'route not found' })
  }
}
