'use strict'

import { send, json } from 'micro'
import HttpHash from 'http-hash'
import { planRepository } from 'gym-advance-db'
import utils from './lib/utils'
import config from './config'

const hash = HttpHash()

hash.set('POST /', async function savePlan (req, res, params) {
  let userId = req.headers['x-user-id']
  let plan = await json(req)

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      planRepository.savePlan(plan, function (err, result) {
        if (err) return send(res, 500, err)

        send(res, 201, result)
      })
    })
  })
})

hash.set('GET /:id', async function getPlan (req, res, params) {
  let userId = req.headers['x-user-id']
  let id = params.id

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      planRepository.getPlan(id, function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 200, result)
      })
    })
  })
})

hash.set('GET /list', async function getPlans (req, res, params) {
  let userId = req.headers['x-user-id']
  let id = params.id

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      planRepository.getPlans(function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 200, result)
      })
    })
  })
})

hash.set('PUT /:id', async function updatePlan (req, res, params) {
  let id = params.id
  let plan = await json(req)

  planRepository.updatePlan(id, plan, function (err, result) {
    if (err) return send(res, 500, { error: err.message })

    send(res, 204)
  })
})

hash.set('DELETE /:id', async function deletePlan (req, res, params) {
  let id = params.id

  planRepository.deletePlan(id, function (err, result) {
    if (err) return send(res, 500, { error: err.message })

    send(res, 204)
  })
})

// gyms payments

hash.set('POST /payments', async function saveGymPayment (req, res, params) {
  let userId = req.headers['x-user-id']
  let payment = await json(req)

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      planRepository.saveGymPayment(payment, function (err, result) {
        if (err) return send(res, 500, err)

        send(res, 201, result)
      })
    })
  })
})

hash.set('GET /payments/list', async function listGymPayments (req, res, params) {
  let userId = req.headers['x-user-id']
  let id = params.id

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      planRepository.listGymPayments(function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 200, result)
      })
    })
  })
})

hash.set('GET /payments/:id', async function getPaymentsOfGym (req, res, params) {
  let userId = req.headers['x-user-id']
  let id = params.id
  let fkIdGym = params.id

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      planRepository.getPaymentsOfGym(fkIdGym, function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 200, result)
      })
    })
  })
})

hash.set('GET /gymspayment/:id', async function getGymsPayment (req, res, params) {
  let userId = req.headers['x-user-id']
  let id = params.id
  let idGymsPayment = params.id

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      planRepository.getGymsPayment(idGymsPayment, function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 200, result)
      })
    })
  })
})

hash.set('PUT /gymspayment', async function updateGymsPayment (req, res, params) {
  let data = await json(req)

  planRepository.updateGymsPayment(data, function (err, result) {
    if (err) return send(res, 500, { error: err.message })

    send(res, 204)
  })
})

export default async function main (req, res) {
  let { method, url } = req
  let match = hash.get(`${method.toUpperCase()} ${url}`)

  if (match.handler) {
    try {
      await match.handler(req, res, match.params)
    } catch (e) {
      send(res, 500, { error: e.message })
    }
  } else {
    send(res, 404, { error: 'route not found' })
  }
}
