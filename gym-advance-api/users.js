'use strict'

import { send, json } from 'micro'
import HttpHash from 'http-hash'
import { userRepository } from 'gym-advance-db'
import utils from './lib/utils'
import config from './config'

const hash = HttpHash()

hash.set('POST /', async function saveUser (req, res, params) {
  let userId = req.headers['x-user-id']
  let user = await json(req)

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      if (!user.fkIdGym) {
        user.fkIdGym = decoded.fkIdGym
      }

      userRepository.saveUser(user, function (err, result) {
        if (err) return send(res, 500, err)

        send(res, 201, result)
      })
    })
  })
})

hash.set('POST /changePassword', async function changePassword (req, res, params) {
  let userId = req.headers['x-user-id']
  let data = await json(req)

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      userRepository.changePassword(userId, data.oldPassword, data.newPassword, function (err, result) {
        if (err) {
          return send(res, 401, {error: err.message})
        }

        send(res, 201, result)
      })
    })
  })
})

hash.set('POST /bodymeasurements', async function saveBodyMeasurements (req, res, params) {
  let userId = req.headers['x-user-id']
  let data = await json(req)

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      userRepository.saveBodyMeasurements (data, function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 201, result)
      })
    })
  })
})

hash.set('GET /:id', async function getUserById (req, res, params) {
  let userId = req.headers['x-user-id']
  let id = params.id

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      userRepository.getUserById(id, function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 200, result)
      })
    })
  })
})

hash.set('GET /bodymeasurements/:fkiduser', async function getBodyMeasurementsOfUser (req, res, params) {
  let userId = req.headers['x-user-id']
  let fkIdUser = params.fkiduser

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      userRepository.getBodyMeasurementsOfUser(fkIdUser, function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 200, result)
      })
    })
  })
})

hash.set('GET /username/:username', async function getUserByUsername (req, res, params) {
  let username = params.username

  userRepository.getUserByUsername(username, function (err, result) {
    if (err) return send(res, 500, err)

    send(res, 200, result)
  })
})

hash.set('GET /list', async function getUsers (req, res, params) {
  let userId = req.headers['x-user-id']

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      userRepository.listUsers(userId, function (err, result) {
        if (err) {
          if (err.message === 'unauthorized') return send(res, 401, { error: err.message })
          return send(res, 500, { error: err.message })
        }

        send(res, 200, result)
      })
    })
  })
})

hash.set('GET /list/users', async function getUsersOfGym (req, res, params) {
  let userId = req.headers['x-user-id']

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      let fkIdGym = decoded.fkIdGym
      userRepository.getUsersOfGym(userId, fkIdGym, function (err, result) {
        if (err) {
          return send(res, 500, { error: err.message })
        }
        send(res, 200, result)
      })
    })
  })
})

hash.set('GET /list/customers', async function getCustomersOfGym (req, res, params) {
  let userId = req.headers['x-user-id']

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      userRepository.getCustomersOfGym(userId, function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 200, result)
      })
    })
  })
})

hash.set('GET /list/instructors/:fkIdGym', async function getInstructorsOfGym (req, res, params) {
  let userId = req.headers['x-user-id']
  let fkIdGym = params.fkIdGym

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      userRepository.getInstructorsOfGym(fkIdGym, function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 200, result)
      })
    })
  })
})

hash.set('PUT /update', async function updateUser (req, res, params) {
  let userId = req.headers['x-user-id']
  let data = await json(req)

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      userRepository.updateUser(data, function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 204)
      })
    })
  })
})

hash.set('PUT /update/medicalconditions', async function updateMedicalConditions (req, res, params) {
  let userId = req.headers['x-user-id']
  let data = await json(req)

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      userRepository.updateMedicalConditions(data, function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 204)
      })
    })
  })
})

hash.set('DELETE /:id', async function deleteUser (req, res, params) {
  let userId = req.headers['x-user-id']
  let id = params.id

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      userRepository.deleteUser(id, function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 204)
      })
    })
  })
})

hash.set('DELETE /bodymeasurements/:id', async function deleteBodyMeasurement (req, res, params) {
  let userId = req.headers['x-user-id']
  let id = params.id

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      userRepository.deleteBodyMeasurement(id, function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 204)
      })
    })
  })
})

export default async function main (req, res) {
  let { method, url } = req
  let match = hash.get(`${method.toUpperCase()} ${url}`)

  if (match.handler) {
    try {
      await match.handler(req, res, match.params)
    } catch (e) {
      send(res, 500, { error: e.message })
    }
  } else {
    send(res, 404, { error: 'route not found' })
  }
}
