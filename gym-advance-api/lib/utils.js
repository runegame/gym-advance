'use strict'

import jwt from 'jsonwebtoken'
import bearer from 'token-extractor'

export default {
  signToken (payload, secret, options, callback) {
    jwt.sign(payload, secret, options, (err, token) => {
      if (err) return callback(err)

      return callback(null, token)
    })
  },

  verifyToken (token, secret, options, callback) {
    jwt.verify(token, secret, options, (err, decoded) => {
      if (err) return callback(err)
      return callback(null, decoded)
    })
  },

  extractToken (req, callback) {
    bearer(req, (err, token) => {
      if (err) return callback(err)
      return callback(null, token)
    })
  }
}
