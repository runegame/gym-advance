'use strict'

import { send, json } from 'micro'
import HttpHash from 'http-hash'
import { gymRepository } from 'gym-advance-db'
import utils from './lib/utils'
import config from './config'

const hash = HttpHash()

hash.set('POST /', async function saveGym (req, res, params) {
  let userId = req.headers['x-user-id']
  let gym = await json(req)

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      gymRepository.saveGym(gym, function (err, result) {
        if (err) return send(res, 500, err)

        send(res, 201, result)
      })
    })
  })
})

hash.set('GET /:id', async function getGym (req, res, params) {
  let userId = req.headers['x-user-id']
  let id = params.id

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      gymRepository.getGym(id, function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 200, result)
      })
    })
  })
})

hash.set('GET /basicstatistics/:fkidgym', async function getBasicStatistics (req, res, params) {
  let userId = req.headers['x-user-id']
  let fkIdGym = params.fkidgym

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      gymRepository.getBasicStatistics(fkIdGym, function (err, result) {
        if (err) return send(res, 500, { error: err.message })
        send(res, 200, result)
      })
    })
  })
})

hash.set('GET /list', async function getGyms (req, res, params) {
  let userId = req.headers['x-user-id']

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      gymRepository.getGyms(function (err, result) {
        if (err) return send(res, 500, { error: err.message })
        send(res, 200, result)
      })
    })
  })
})

hash.set('PUT /:id', async function updateGym (req, res, params) {
  let id = params.id
  let data = await json(req)

  gymRepository.updateGym(id, data, function (err, result) {
    if (err) return send(res, 500, { error: err.message })

    send(res, 204)
  })
})

hash.set('DELETE /:id', async function deleteGym (req, res, params) {
  let id = params.id

  gymRepository.deleteGym(id, function (err, result) {
    if (err) return send(res, 500, { error: err.message })

    send(res, 204)
  })
})

// rutas para los eventos

hash.set('POST /event', async function saveEvent (req, res, params) {
  let userId = req.headers['x-user-id']
  let eventData = await json(req)

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      eventData.fkIdGym = decoded.fkIdGym
      eventData.fkIdUser = decoded.userId
      gymRepository.saveEvent(eventData, function (err, result) {
        if (err) return send(res, 500, err)

        send(res, 201, result)
      })
    })
  })
})

hash.set('GET /event', async function getEventsOfGym (req, res, params) {
  let userId = req.headers['x-user-id']
  let fkIdGym = params.fkIdGym

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      let fkIdGym = decoded.fkIdGym
      gymRepository.getEventsOfGym(fkIdGym, function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 200, result)
      })
    })
  })
})

hash.set('PUT /events', async function updateEvent (req, res, params) {
  let data = await json(req)

  gymRepository.updateEvent(data, function (err, result) {
    if (err) return send(res, 500, { error: err.message })

    send(res, 204)
  })
})

// `${this.options.endpoints.gyms}/events/${id}`,
hash.set('GET /events/:id', async function getEvent (req, res, params) {
  let userId = req.headers['x-user-id']
  let id = params.id

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      gymRepository.getEvent(id, function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 200, result)
      })
    })
  })
})

// rutas para las categorias

hash.set('GET /categories/list', async function getCategories (req, res, params) {
  let userId = req.headers['x-user-id']
  let fkIdGym = params.id

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      gymRepository.getEventCategories(function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 200, result)
      })
    })
  })
})

hash.set('DELETE /events/:id', async function deleteEvent (req, res, params) {
  let userId = req.headers['x-user-id']
  let id = params.id

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      gymRepository.deleteEvent(id, function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 204)
      })
    })
  })
})


export default async function main (req, res) {
  let { method, url } = req
  let match = hash.get(`${method.toUpperCase()} ${url}`)

  if (match.handler) {
    try {
      await match.handler(req, res, match.params)
    } catch (e) {
      send(res, 500, { error: e.message })
    }
  } else {
    send(res, 404, { error: 'route not found' })
  }
}
