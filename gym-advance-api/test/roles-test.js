'use strict'

import test from 'ava'
import micro from 'micro'
import listen from 'test-listen'
import request from 'request-promise'
import roles from '../roles'
import fixtures from './fixtures'

test.beforeEach(async t => {
  let srv = micro(roles)
  t.context.url = await listen(srv)
})

test('ROLES TEST', async t => {
  // comienza el test para POST

  let role = fixtures.getRole()
  let url = t.context.url

  let optionsPOST = {
    method: 'POST',
    uri: url,
    json: true,
    body: role,
    resolveWithFullResponse: true
  }

  let responsePOST = await request(optionsPOST)

  role.__v = responsePOST.body.__v
  role._id = responsePOST.body._id

  t.is(responsePOST.statusCode, 201)
  t.deepEqual(responsePOST.body, role)

  // comienza el test para GET

  let optionsGET = {
    method: 'GET',
    uri: `${url}/${role._id}`,
    json: true,
    resolveWithFullResponse: true
  }

  let responseGET = await request(optionsGET)

  t.is(responseGET.statusCode, 200)
  t.deepEqual(responseGET.body, role)

  optionsGET = {
    method: 'GET',
    uri: `${url}/list`,
    json: true,
    resolveWithFullResponse: true
  }

  responseGET = await request(optionsGET)

  let rolesArray = []
  rolesArray.push(role)

  t.is(responseGET.statusCode, 200)
  t.deepEqual(responseGET.body, rolesArray)

  // comienza el test para PUT

  role.dataName = 'nombre_actualizado'

  let optionsPUT = {
    method: 'PUT',
    uri: `${url}/${role._id}`,
    json: true,
    body: role,
    resolveWithFullResponse: true
  }

  let responsePUT = await request(optionsPUT)

  t.is(responsePUT.statusCode, 204)

  // comienza el test para DELETE

  let optionsDELETE = {
    method: 'DELETE',
    uri: `${url}/${role._id}`,
    json: true,
    resolveWithFullResponse: true
  }

  let responseDELETE = await request(optionsDELETE)

  t.is(responseDELETE.statusCode, 204)
})
