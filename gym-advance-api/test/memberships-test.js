'use strict'

import test from 'ava'
import micro from 'micro'
import listen from 'test-listen'
import request from 'request-promise'
import memberships from '../memberships'
import fixtures from './fixtures'

test.beforeEach(async t => {
  let srv = micro(memberships)
  t.context.url = await listen(srv)
})

test('MEMBERSHIPS TEST', async t => {
  // comienza el test para POST

  let membership = fixtures.getMembership()
  let url = t.context.url

  let optionsPOST = {
    method: 'POST',
    uri: url,
    json: true,
    body: membership,
    resolveWithFullResponse: true
  }

  let responsePOST = await request(optionsPOST)
  membership.__v = responsePOST.body.__v
  membership.updatedAt = responsePOST.body.updatedAt
  membership.createdAt = responsePOST.body.createdAt
  membership._id = responsePOST.body._id

  t.is(responsePOST.statusCode, 201)
  t.deepEqual(responsePOST.body, membership)

  // comienza el test para GET

  let optionsGET = {
    method: 'GET',
    uri: `${url}/${membership._id}`,
    json: true,
    resolveWithFullResponse: true
  }

  let responseGET = await request(optionsGET)

  t.is(responseGET.statusCode, 200)
  t.deepEqual(responseGET.body, membership)

  optionsGET = {
    method: 'GET',
    uri: `${url}/list/${membership.fkIdGym}`,
    json: true,
    resolveWithFullResponse: true
  }

  responseGET = await request(optionsGET)

  let membershipsArray = []
  membershipsArray.push(membership)

  t.is(responseGET.statusCode, 200)
  t.deepEqual(responseGET.body, membershipsArray)

  // comienza el test para PUT

  membership.dataName = 'nombre_actualizado'

  let optionsPUT = {
    method: 'PUT',
    uri: `${url}/${membership._id}`,
    json: true,
    body: membership,
    resolveWithFullResponse: true
  }

  let responsePUT = await request(optionsPUT)

  t.is(responsePUT.statusCode, 204)

  // comienza el test para DELETE

  let optionsDELETE = {
    method: 'DELETE',
    uri: `${url}/${membership._id}`,
    json: true,
    resolveWithFullResponse: true
  }

  let responseDELETE = await request(optionsDELETE)

  t.is(responseDELETE.statusCode, 204)

  /**
  *   TEST PARA LOS USER PAYMENTS
  **/

  // comienza el test para POST

  let userPayment = fixtures.getUserPayment()

  optionsPOST = {
    method: 'POST',
    uri: `${url}/payments`,
    json: true,
    body: userPayment,
    resolveWithFullResponse: true
  }

  responsePOST = await request(optionsPOST)
  userPayment.__v = responsePOST.body.__v
  userPayment.updatedAt = responsePOST.body.updatedAt
  userPayment.createdAt = responsePOST.body.createdAt
  userPayment._id = responsePOST.body._id

  t.is(responsePOST.statusCode, 201)
  t.deepEqual(responsePOST.body, userPayment)

  // comienza el test para LIST PAYMENTS

  optionsGET = {
    method: 'GET',
    uri: `${url}/payments/list`,
    json: true,
    resolveWithFullResponse: true
  }

  responseGET = await request(optionsGET)

  let bodyResponse = responseGET.body
  let userPaymentsArray = []

  bodyResponse.forEach(function (element) {
    userPaymentsArray.push(element)
  })

  t.is(responseGET.statusCode, 200)
  t.deepEqual(responseGET.body, userPaymentsArray)

  // comienza el test para GET

  optionsGET = {
    method: 'GET',
    uri: `${url}/payments/${userPayment.fkIdUser}`,
    json: true,
    resolveWithFullResponse: true
  }

  responseGET = await request(optionsGET)

  userPaymentsArray = []
  userPaymentsArray.push(userPayment)

  t.is(responseGET.statusCode, 200)
  t.deepEqual(responseGET.body, userPaymentsArray)
})
