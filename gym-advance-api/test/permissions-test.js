'use strict'

import test from 'ava'
import micro from 'micro'
import listen from 'test-listen'
import request from 'request-promise'
import permissions from '../permissions'
import fixtures from './fixtures'

test.beforeEach(async t => {
  let srv = micro(permissions)
  t.context.url = await listen(srv)
})

test('PERMISSIONS TEST', async t => {
  // comienza el test para POST

  let permission = fixtures.getPermission()
  let url = t.context.url

  let optionsPOST = {
    method: 'POST',
    uri: url,
    json: true,
    body: permission,
    resolveWithFullResponse: true
  }

  let responsePOST = await request(optionsPOST)

  permission.__v = responsePOST.body.__v
  permission._id = responsePOST.body._id

  t.is(responsePOST.statusCode, 201)
  t.deepEqual(responsePOST.body, permission)

  // comienza el test para GET

  let optionsGET = {
    method: 'GET',
    uri: `${url}/${permission._id}`,
    json: true,
    resolveWithFullResponse: true
  }

  let responseGET = await request(optionsGET)

  t.is(responseGET.statusCode, 200)
  t.deepEqual(responseGET.body, permission)

  optionsGET = {
    method: 'GET',
    uri: `${url}/list`,
    json: true,
    resolveWithFullResponse: true
  }

  responseGET = await request(optionsGET)

  let permissionsArray = []
  permissionsArray.push(permission)

  t.is(responseGET.statusCode, 200)
  t.deepEqual(responseGET.body, permissionsArray)

  // comienza el test para PUT

  permission.dataName = 'nombre_actualizado'

  let optionsPUT = {
    method: 'PUT',
    uri: `${url}/${permission._id}`,
    json: true,
    body: permission,
    resolveWithFullResponse: true
  }

  let responsePUT = await request(optionsPUT)

  t.is(responsePUT.statusCode, 204)

  // comienza el test para DELETE

  let optionsDELETE = {
    method: 'DELETE',
    uri: `${url}/${permission._id}`,
    json: true,
    resolveWithFullResponse: true
  }

  let responseDELETE = await request(optionsDELETE)

  t.is(responseDELETE.statusCode, 204)
})
