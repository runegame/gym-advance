'use strict'

import test from 'ava'
import micro from 'micro'
import listen from 'test-listen'
import request from 'request-promise'
import plans from '../plans'
import fixtures from './fixtures'

test.beforeEach(async t => {
  let srv = micro(plans)
  t.context.url = await listen(srv)
})

test('PLANS TEST', async t => {
  // comienza el test para POST

  let plan = fixtures.getPlan()
  let url = t.context.url

  let optionsPOST = {
    method: 'POST',
    uri: url,
    json: true,
    body: plan,
    resolveWithFullResponse: true
  }

  let responsePOST = await request(optionsPOST)
  plan.__v = responsePOST.body.__v
  plan.updatedAt = responsePOST.body.updatedAt
  plan.createdAt = responsePOST.body.createdAt
  plan._id = responsePOST.body._id

  t.is(responsePOST.statusCode, 201)
  t.deepEqual(responsePOST.body, plan)

  // comienza el test para GET

  let optionsGET = {
    method: 'GET',
    uri: `${url}/${plan._id}`,
    json: true,
    resolveWithFullResponse: true
  }

  let responseGET = await request(optionsGET)

  t.is(responseGET.statusCode, 200)
  t.deepEqual(responseGET.body, plan)

  optionsGET = {
    method: 'GET',
    uri: `${url}/list`,
    json: true,
    resolveWithFullResponse: true
  }

  responseGET = await request(optionsGET)

  let plansArray = []
  plansArray.push(plan)

  t.is(responseGET.statusCode, 200)
  t.deepEqual(responseGET.body, plansArray)

  // comienza el test para PUT

  plan.dataName = 'nombre_actualizado'

  let optionsPUT = {
    method: 'PUT',
    uri: `${url}/${plan._id}`,
    json: true,
    body: plan,
    resolveWithFullResponse: true
  }

  let responsePUT = await request(optionsPUT)

  t.is(responsePUT.statusCode, 204)

  // comienza el test para DELETE

  let optionsDELETE = {
    method: 'DELETE',
    uri: `${url}/${plan._id}`,
    json: true,
    resolveWithFullResponse: true
  }

  let responseDELETE = await request(optionsDELETE)

  t.is(responseDELETE.statusCode, 204)

  /**
  *   TEST PARA LOS GYMS PAYMENTS
  **/

  // comienza el test para POST

  let gymPayment = fixtures.getGymPayment()

  optionsPOST = {
    method: 'POST',
    uri: `${url}/payments`,
    json: true,
    body: gymPayment,
    resolveWithFullResponse: true
  }

  responsePOST = await request(optionsPOST)
  gymPayment.__v = responsePOST.body.__v
  gymPayment.updatedAt = responsePOST.body.updatedAt
  gymPayment.createdAt = responsePOST.body.createdAt
  gymPayment._id = responsePOST.body._id

  t.is(responsePOST.statusCode, 201)
  t.deepEqual(responsePOST.body, gymPayment)

  // comienza el test para LIST PAYMENTS

  optionsGET = {
    method: 'GET',
    uri: `${url}/payments/list`,
    json: true,
    resolveWithFullResponse: true
  }

  responseGET = await request(optionsGET)

  let bodyResponse = responseGET.body
  let gymPaymentsArray = []

  bodyResponse.forEach(function (element) {
    gymPaymentsArray.push(element)
  })

  t.is(responseGET.statusCode, 200)
  t.deepEqual(responseGET.body, gymPaymentsArray)

  // comienza el test para GET PAYMENTS OF GYM

  optionsGET = {
    method: 'GET',
    uri: `${url}/payments/${gymPayment.fkIdGym}`,
    json: true,
    resolveWithFullResponse: true
  }

  responseGET = await request(optionsGET)

  gymPaymentsArray = []
  gymPaymentsArray.push(gymPayment)

  t.is(responseGET.statusCode, 200)
  t.deepEqual(responseGET.body, gymPaymentsArray)
})
