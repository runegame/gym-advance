'use strict'

import test from 'ava'
import micro from 'micro'
import listen from 'test-listen'
import request from 'request-promise'
import gyms from '../gyms'
import fixtures from './fixtures'

test.beforeEach(async t => {
  let srv = micro(gyms)
  t.context.url = await listen(srv)
})

test('GYMS TEST', async t => {
  // comienza el test para POST

  let gym = fixtures.getGym()
  let url = t.context.url

  let optionsPOST = {
    method: 'POST',
    uri: url,
    json: true,
    body: gym,
    resolveWithFullResponse: true
  }

  let responsePOST = await request(optionsPOST)
  gym.__v = responsePOST.body.__v
  gym.updatedAt = responsePOST.body.updatedAt
  gym.createdAt = responsePOST.body.createdAt

  gym._id = responsePOST.body._id

  t.is(responsePOST.statusCode, 201)
  t.deepEqual(responsePOST.body, gym)

  // comienza el test para GET

  let optionsGET = {
    method: 'GET',
    uri: `${url}/${gym._id}`,
    json: true,
    resolveWithFullResponse: true
  }

  let responseGET = await request(optionsGET)

  t.is(responseGET.statusCode, 200)
  t.deepEqual(responseGET.body, gym)

  optionsGET = {
    method: 'GET',
    uri: `${url}/list`,
    json: true,
    resolveWithFullResponse: true
  }

  responseGET = await request(optionsGET)

  let gymsArray = []
  gymsArray.push(gym)

  t.is(responseGET.statusCode, 200)
  t.deepEqual(responseGET.body, gymsArray)

  // comienza el test para PUT

  gym.dataName = 'nombre_actualizado'

  let optionsPUT = {
    method: 'PUT',
    uri: `${url}/${gym._id}`,
    json: true,
    body: gym,
    resolveWithFullResponse: true
  }

  let responsePUT = await request(optionsPUT)

  t.is(responsePUT.statusCode, 204)

  // comienza el test para DELETE

  let optionsDELETE = {
    method: 'DELETE',
    uri: `${url}/${gym._id}`,
    json: true,
    resolveWithFullResponse: true
  }

  let responseDELETE = await request(optionsDELETE)

  t.is(responseDELETE.statusCode, 204)
})
