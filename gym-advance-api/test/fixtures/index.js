import uuid from 'uuid-base62'

export default {
  getGym () {
    return {
      capacity: 100,
      dataName: `Gimnasio_${uuid.v4()}`,
      email: 'gym@gymadvance.com',
      fkIdPlan: '27d169e8-0349-11e7-a660-0800f46f34f3',
      location: 'Por aqui cerca de la esquina',
      main: true,
      phone: 2771234567,
      status: true
    }
  },
  getGymPayment () {
    return {
      amount: 150,
      fkIdGym: uuid.uuid(),
      fkIdPlan: uuid.uuid(),
      paymentType: 'transfer',
      reference: 1234567,
      status: 'checked'
    }
  },
  getMembership () {
    return {
      checkin: '06:00',
      checkout: '18:00',
      dataName: `memberhip_${uuid.v4()}`,
      durationInDays: 1,
      fkIdGym: 'f46f34f3-0349-11e7-a660-080027d169e8',
      price: 1.5,
      status: true
    }
  },
  getPermission () {
    return {
      dataName: 'module::Customer::access',
      roles: ['owner', 'admin']
    }
  },
  getPlan () {
    return {
      dataName: `plan_${uuid.v4()}`,
      description: 'Plan completo por un año',
      durationInDays: 365,
      price: 150,
      status: true,
      urlPicture: ''
    }
  },
  getRole () {
    return {
      dataName: `role_${uuid.v4()}`,
      description: 'Dueño del Gimnasio',
      scope: 'main'
    }
  },
  getUser () {
    return {
      birthday: new Date(),
      documentNumber: 22000111,
      documentType: 'DNI',
      email: `user_${uuid.v4()}()@gymadvance.com`,
      emergencyPhone: 41278945612,
      fkIdGym: 'f46f34f3-0349-11e7-a660-080027d169e8',
      fkIdMembership: '27d169e8-0349-11e7-a660-0800f46f34f3',
      fullname: `fullname_${uuid.v4()}`,
      haveInsurancePolicy: false,
      mainPhone: 41278945612,
      membershipExpiration: new Date(),
      observations: 'Ninguna',
      password: '12346',
      residenceAddress: 'El cliente de la esquina',
      role: 'user',
      status: true,
      username: `gymadvance_${uuid.v4()}`,
      workAddress: 'El cliente que trabaja en la esquina'
    }
  },
  getUserPayment () {
    return {
      amount: 150,
      fkIdUser: uuid.uuid(),
      fkIdMembership: uuid.uuid(),
      paymentType: 'transfer',
      reference: 1234567,
      status: 'checked'
    }
  }
}
