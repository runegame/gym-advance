'use strict'

import test from 'ava'
import micro from 'micro'
import listen from 'test-listen'
import request from 'request-promise'
import users from '../users'
import fixtures from './fixtures'

test.beforeEach(async t => {
  let srv = micro(users)
  t.context.url = await listen(srv)
})

test('USERS TEST', async t => {
  // comienza el test para POST

  let user = fixtures.getUser()
  let url = t.context.url

  let optionsPOST = {
    method: 'POST',
    uri: url,
    json: true,
    body: user,
    resolveWithFullResponse: true
  }

  let responsePOST = await request(optionsPOST)
  user.__v = responsePOST.body.__v
  user.updatedAt = responsePOST.body.updatedAt
  user.createdAt = responsePOST.body.createdAt
  user._id = responsePOST.body._id
  user.password = responsePOST.body.password
  user.birthday = responsePOST.body.birthday
  user.membershipExpiration = responsePOST.body.membershipExpiration

  t.is(responsePOST.statusCode, 201)
  t.deepEqual(responsePOST.body, user)

  // comienza el test para GET

  let optionsGET = {
    method: 'GET',
    uri: `${url}/${user._id}`,
    json: true,
    resolveWithFullResponse: true
  }

  let responseGET = await request(optionsGET)

  t.is(responseGET.statusCode, 200)
  t.deepEqual(responseGET.body, user)

  optionsGET = {
    method: 'GET',
    uri: `${url}/list`,
    json: true,
    resolveWithFullResponse: true
  }

  responseGET = await request(optionsGET)

  let usersArray = []
  usersArray.push(user)

  t.is(responseGET.statusCode, 200)
  t.deepEqual(responseGET.body, usersArray)

  optionsGET = {
    method: 'GET',
    uri: `${url}/list/${user.fkIdGym}`,
    json: true,
    resolveWithFullResponse: true
  }

  responseGET = await request(optionsGET)

  usersArray = []
  usersArray.push(user)

  t.is(responseGET.statusCode, 200)
  t.deepEqual(responseGET.body, usersArray)

  // comienza el test para PUT

  user.dataName = 'nombre_actualizado'

  let optionsPUT = {
    method: 'PUT',
    uri: `${url}/${user._id}`,
    json: true,
    body: user,
    resolveWithFullResponse: true
  }

  let responsePUT = await request(optionsPUT)

  t.is(responsePUT.statusCode, 204)

  // comienza el test para DELETE

  let optionsDELETE = {
    method: 'DELETE',
    uri: `${url}/${user._id}`,
    json: true,
    resolveWithFullResponse: true
  }

  let responseDELETE = await request(optionsDELETE)

  t.is(responseDELETE.statusCode, 204)
})
