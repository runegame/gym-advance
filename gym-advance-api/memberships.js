'use strict'

import { send, json } from 'micro'
import HttpHash from 'http-hash'
import { membershipRepository } from 'gym-advance-db'
import utils from './lib/utils'
import config from './config'

const hash = HttpHash()

hash.set('POST /', async function saveMembership (req, res, params) {
  let userId = req.headers['x-user-id']
  let membership = await json(req)

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      membership.fkIdGym = decoded.fkIdGym
      membershipRepository.saveMembership(membership, function (err, result) {
        if (err) return send(res, 500, err)

        send(res, 201, result)
      })
    })
  })
})

hash.set('GET /:id', async function getMembership (req, res, params) {
  let userId = req.headers['x-user-id']
  let id = params.id

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      membershipRepository.getMembership(id, function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 200, result)
      })
    })
  })
})

hash.set('GET /list', async function getMembershipsOfGym (req, res, params) {
  let userId = req.headers['x-user-id']

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      let fkIdGym = decoded.fkIdGym
      membershipRepository.getMembershipsOfGym(fkIdGym, function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 200, result)
      })
    })
  })
})

hash.set('PUT /', async function updateMembership (req, res, params) {
  let id = params.id
  let membership = await json(req)

  membershipRepository.updateMembership(membership, function (err, result) {
    if (err) return send(res, 500, { error: err.message })

    send(res, 204)
  })
})

hash.set('DELETE /:id', async function deleteMembership (req, res, params) {
  let userId = req.headers['x-user-id']
  let id = params.id

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      membershipRepository.deleteMembership(id, function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 204)
      })
    })
  })
})

// user payments

hash.set('POST /payments', async function saveUserPayment (req, res, params) {
  let userId = req.headers['x-user-id']
  let payment = await json(req)

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      payment.fkIdGym = decoded.fkIdGym
      membershipRepository.saveUserPayment(payment, function (err, result) {
        if (err) return send(res, 500, err)

        send(res, 201, result)
      })
    })
  })
})

hash.set('GET /payments/:fkIdUsersPayment', async function getUsersPayment (req, res, params) {
  let userId = req.headers['x-user-id']
  let fkIdUsersPayment = params.fkIdUsersPayment

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      membershipRepository.getUsersPayment(fkIdUsersPayment, function (err, result) {
        if (err) return send(res, 500, err)

        send(res, 200, result)
      })
    })
  })
})

hash.set('GET /payments/all/', async function listUserPayments (req, res, params) {
  let userId = req.headers['x-user-id']

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      membershipRepository.listAllUserPayments(function (err, result) {
        if (err) return send(res, 500, err)

        send(res, 200, result)
      })
    })
  })
})

hash.set('GET /payments/list', async function listUserPayments (req, res, params) {
  let userId = req.headers['x-user-id']

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      let fkIdGym = decoded.fkIdGym
      membershipRepository.listUserPayments(fkIdGym, function (err, result) {
        if (err) return send(res, 500, err)

        send(res, 200, result)
      })
    })
  })
})

hash.set('GET /payments/user/:id', async function getPaymentsOfUser (req, res, params) {
  let userId = req.headers['x-user-id']
  let fkIdUser = params.id

  utils.extractToken(req, function (err, token) {
    if (err) return send(res, 401, {error: 'invalid token'})

    utils.verifyToken(token, config.secret, null, function (err, decoded) {
      if (err) return send(res, 401, {error: 'invalid token'})

      if (decoded && decoded.userId !== userId) {
        return send(res, 401, {error: 'invalid token'})
      }

      membershipRepository.getPaymentsOfUser(fkIdUser, function (err, result) {
        if (err) return send(res, 500, { error: err.message })

        send(res, 200, result)
      })
    })
  })
})

hash.set('PUT /userspayment', async function updateUsersPayment (req, res, params) {
  let data = await json(req)

  membershipRepository.updateUsersPayment(data, function (err, result) {
    if (err) return send(res, 500, { error: err.message })

    send(res, 204)
  })
})

export default async function main (req, res) {
  let { method, url } = req
  let match = hash.get(`${method.toUpperCase()} ${url}`)

  if (match.handler) {
    try {
      await match.handler(req, res, match.params)
    } catch (e) {
      send(res, 500, { error: e.message })
    }
  } else {
    send(res, 404, { error: 'route not found' })
  }
}
