'use strict'

const uuid = require('uuid-base62')

const fixtures = {
  getGym () {
    return {
      capacity: 100,
      dataName: `Gimnasio_${uuid.v4()}`,
      email: `user_${uuid.v4()}()@gymadvance.com`,
      fkIdPlan: '27d169e8-0349-11e7-a660-0800f46f34f3',
      location: 'Por aqui cerca de la esquina',
      main: true,
      phone: 2771234567,
      status: true
    }
  },
  getGyms (n) {
    let gyms = []
    while (n-- > 0) {
      gyms.push(this.getGym())
    }
    return gyms
  },
  getGymPayment () {
    return {
      amount: 150,
      fkIdGym: uuid.uuid(),
      fkIdPlan: uuid.uuid(),
      paymentType: 'transfer',
      reference: 1234567,
      status: 'checked'
    }
  },
  getGymPayments (n) {
    let gymPayments = []
    while (n-- > 0) {
      gymPayments.push(this.getGym())
    }
    return gymPayments
  },
  getMembership () {
    return {
      checkin: '06:00',
      checkout: '18:00',
      dataName: `memberhip_${uuid.v4()}`,
      durationInDays: 1,
      fkIdGym: 'f46f34f3-0349-11e7-a660-080027d169e8',
      price: 1.5,
      status: true
    }
  },
  getMemberships (n) {
    let memberships = []
    while (n-- > 0) {
      memberships.push(this.getGym())
    }
    return memberships
  },
  getPermission () {
    return {
      dataName: 'module::Customer::access',
      roles: ['owner', 'admin']
    }
  },
  getPermissionsOfRole () {
    return [
      'module::Customer::access1',
      'module::Customer::access2'
    ]
  },
  getPermissions (n) {
    let permissions = []
    while (n-- > 0) {
      permissions.push(this.getGym())
    }
    return permissions
  },
  getPlan () {
    return {
      dataName: `plan_${uuid.v4()}`,
      description: 'Plan completo por un año',
      durationInDays: 365,
      price: 150,
      status: true,
      urlPicture: ''
    }
  },
  getPlans (n) {
    let plans = []
    while (n-- > 0) {
      plans.push(this.getGym())
    }
    return plans
  },
  getRole () {
    return {
      dataName: `role_${uuid.v4()}`,
      description: 'Dueño del Gimnasio',
      scope: 'main'
    }
  },
  getRoles (n) {
    let roles = []
    while (n-- > 0) {
      roles.push(this.getGym())
    }
    return roles
  },
  getUser () {
    return {
      documentNumber: 22000111,
      documentType: 'DNI',
      email: 'test@gymadvance.com',
      emergencyPhone: 41278945612,
      fkIdGym: 'f46f34f3-0349-11e7-a660-080027d169e8',
      fkIdMembership: '27d169e8-0349-11e7-a660-0800f46f34f3',
      fullname: `fullname_${uuid.v4()}`,
      haveInsurancePolicy: false,
      mainPhone: 41278945612,
      observations: 'Ninguna',
      password: '12346',
      residenceAddress: 'El cliente de la esquina',
      role: 'user',
      status: true,
      username: `gymadvance_${uuid.v4()}`,
      workAddress: 'El cliente que trabaja en la esquina'
    }
  },
  getUsers (n) {
    let users = []
    while (n-- > 0) {
      users.push(this.getGym())
    }
    return users
  },
  getUserPayment () {
    return {
      amount: 150,
      fkIdUser: uuid.uuid(),
      fkIdMembership: uuid.uuid(),
      paymentType: 'transfer',
      reference: 1234567,
      status: 'checked'
    }
  },
  getUserPayments (n) {
    let userPayments = []
    while (n-- > 0) {
      userPayments.push(this.getGym())
    }
    return userPayments
  }
}

module.exports = fixtures
