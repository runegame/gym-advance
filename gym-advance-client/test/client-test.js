'use strict'

const test = require('ava')
const gymAdvance = require('../')
const fixtures = require('./fixtures')
const nock = require('nock')
const uuid = require('uuid-base62')

let options = {
  endpoints: {
    auth: 'http://gymadvance.test/auth',
    gyms: 'http://gymadvance.test/gym',
    memberships: 'http://gymadvance.test/membership',
    permissions: 'http://gymadvance.test/permission',
    plans: 'http://gymadvance.test/plan',
    roles: 'http://gymadvance.test/role',
    users: 'http://gymadvance.test/user'
  }
}

test.beforeEach(t => {
  t.context.client = gymAdvance.createClient(options)
})

test('client', t => {
  const client = t.context.client

  // GYMS
  t.is(typeof client.saveGym, 'function')
  t.is(typeof client.getGym, 'function')
  t.is(typeof client.listGyms, 'function')
  t.is(typeof client.updateGym, 'function')
  t.is(typeof client.deleteGym, 'function')

  // MEMBERSHIPS
  t.is(typeof client.saveMembership, 'function')
  t.is(typeof client.getMembership, 'function')
  t.is(typeof client.getMembershipsOfGym, 'function')
  t.is(typeof client.updateMembership, 'function')
  t.is(typeof client.deleteMembership, 'function')
  t.is(typeof client.saveUserPayment, 'function')
  t.is(typeof client.getPaymentsOfUser, 'function')
  t.is(typeof client.listUserPayments, 'function')

  // PERMISSIONS
  t.is(typeof client.savePermission, 'function')
  t.is(typeof client.getPermission, 'function')
  t.is(typeof client.listPermissions, 'function')
  t.is(typeof client.updatePermission, 'function')
  t.is(typeof client.deletePermission, 'function')

  // PLANS
  t.is(typeof client.listPlans, 'function')
})

// TESTS PARA GYMS

test('saveGym', async t => {
  const client = t.context.client
  let gym = fixtures.getGym()

  nock(options.endpoints.gyms)
    .post('/', gym)
    .reply(201, gym)

  let result = await client.saveGym(gym)
  t.deepEqual(result, gym)
})

test('getGym', async t => {
  const client = t.context.client
  let gym = fixtures.getGym()
  gym._id = uuid.uuid()

  nock(options.endpoints.gyms)
    .get(`/${gym._id}`)
    .reply(200, gym)

  let result = await client.getGym(gym._id)

  t.deepEqual(result, gym)
})

test('listGyms', async t => {
  const client = t.context.client

  let gyms = fixtures.getGyms(3)

  nock(options.endpoints.gyms)
    .get('/list')
    .reply(200, gyms)

  let result = await client.listGyms()

  t.deepEqual(gyms, result)
})

test('updateGym', async t => {
  const client = t.context.client

  let id = uuid.v4()
  let response = {'statusMessage': 'Register updated', 'id': id}

  nock(options.endpoints.gyms)
    .put(`/${id}`)
    .reply(204, response)

  let result = await client.updateGym(id)

  t.deepEqual(response, result)
})

test('deleteGym', async t => {
  const client = t.context.client

  let id = uuid.v4()
  let response = {'statusMessage': 'Register deleted', 'id': id}

  nock(options.endpoints.gyms)
    .delete(`/${id}`)
    .reply(204, response)

  let result = await client.deleteGym(id)

  t.deepEqual(response, result)
})

// TESTS PARA MEMBERSHIPS

test('saveMembership', async t => {
  const client = t.context.client
  let membership = fixtures.getMembership()

  nock(options.endpoints.memberships)
    .post('/', membership)
    .reply(201, membership)

  let result = await client.saveMembership(membership)
  t.deepEqual(result, membership)
})

test('getMembership', async t => {
  const client = t.context.client
  let membership = fixtures.getMembership()
  membership._id = uuid.uuid()

  nock(options.endpoints.memberships)
    .get(`/${membership._id}`)
    .reply(200, membership)

  let result = await client.getMembership(membership._id)

  t.deepEqual(result, membership)
})

test('getMembershipsOfGym', async t => {
  const client = t.context.client

  let memberships = fixtures.getMemberships(3)
  let fkIdGym = uuid.v4()

  nock(options.endpoints.memberships)
    .get(`/list/${fkIdGym}`)
    .reply(200, memberships)

  let result = await client.getMembershipsOfGym(fkIdGym)

  t.deepEqual(memberships, result)
})

test('updateMembership', async t => {
  const client = t.context.client

  let id = uuid.v4()
  let response = {'statusMessage': 'Register updated', 'id': id}

  nock(options.endpoints.memberships)
    .put(`/${id}`)
    .reply(204, response)

  let result = await client.updateMembership(id)

  t.deepEqual(response, result)
})

test('deleteMembership', async t => {
  const client = t.context.client

  let id = uuid.v4()
  let response = {'statusMessage': 'Register deleted', 'id': id}

  nock(options.endpoints.memberships)
    .delete(`/${id}`)
    .reply(204, response)

  let result = await client.deleteMembership(id)

  t.deepEqual(response, result)
})

test('saveUserPayment', async t => {
  const client = t.context.client

  let userPayment = fixtures.getUserPayment()

  nock(options.endpoints.memberships)
    .post(`/payments`)
    .reply(201, userPayment)

  let result = await client.saveUserPayment(userPayment)

  t.deepEqual(userPayment, result)
})

test('getPaymentsOfUser', async t => {
  const client = t.context.client

  let fkIdUser = uuid.v4()
  let userPayments = fixtures.getUserPayments(3)

  nock(options.endpoints.memberships)
    .get(`/payments/${fkIdUser}`)
    .reply(200, userPayments)

  let result = await client.getPaymentsOfUser(fkIdUser)

  t.deepEqual(userPayments, result)
})

test('listUserPayments', async t => {
  const client = t.context.client

  let userPayments = fixtures.getUserPayments(3)

  nock(options.endpoints.memberships)
    .get(`/payments/list`)
    .reply(200, userPayments)

  let result = await client.listUserPayments()

  t.deepEqual(userPayments, result)
})

// TESTS PARA PERMISSIONS

test('savePermission', async t => {
  const client = t.context.client
  let permission = fixtures.getPermission()

  nock(options.endpoints.permissions)
    .post('/', permission)
    .reply(201, permission)

  let result = await client.savePermission(permission)
  t.deepEqual(result, permission)
})

test('getPermission', async t => {
  const client = t.context.client
  let permission = fixtures.getPermission()
  permission._id = uuid.uuid()

  nock(options.endpoints.permissions)
    .get(`/${permission._id}`)
    .reply(200, permission)

  let result = await client.getPermission(permission._id)

  t.deepEqual(result, permission)
})

test('listPermissions', async t => {
  const client = t.context.client

  let permissions = fixtures.getPermissions(3)

  nock(options.endpoints.permissions)
    .get('/list')
    .reply(200, permissions)

  let result = await client.listPermissions()

  t.deepEqual(permissions, result)
})

test('updatePermission', async t => {
  const client = t.context.client

  let id = uuid.v4()
  let response = {'statusMessage': 'Register updated', 'id': id}

  nock(options.endpoints.permissions)
    .put(`/${id}`)
    .reply(204, response)

  let result = await client.updatePermission(id)

  t.deepEqual(response, result)
})

test('deletePermission', async t => {
  const client = t.context.client

  let id = uuid.v4()
  let response = {'statusMessage': 'Register deleted', 'id': id}

  nock(options.endpoints.permissions)
    .delete(`/${id}`)
    .reply(204, response)

  let result = await client.deletePermission(id)

  t.deepEqual(response, result)
})

// TESTS PARA PLANS

test('savePlan', async t => {
  const client = t.context.client
  let plan = fixtures.getPlan()

  nock(options.endpoints.plans)
    .post('/', plan)
    .reply(201, plan)

  let result = await client.savePlan(plan)
  t.deepEqual(result, plan)
})

test('getPlan', async t => {
  const client = t.context.client
  let plan = fixtures.getPlan()
  plan._id = uuid.uuid()

  nock(options.endpoints.plans)
    .get(`/${plan._id}`)
    .reply(200, plan)

  let result = await client.getPlan(plan._id)

  t.deepEqual(result, plan)
})

test('getPlansOfGym', async t => {
  const client = t.context.client

  let plans = fixtures.getPlans(3)
  let fkIdGym = uuid.v4()

  nock(options.endpoints.plans)
    .get(`/list/${fkIdGym}`)
    .reply(200, plans)

  let result = await client.getPlansOfGym(fkIdGym)

  t.deepEqual(plans, result)
})

test('listPlans', async t => {
  const client = t.context.client

  let plans = fixtures.getPlans(3)

  nock(options.endpoints.plans)
    .get(`/list`)
    .reply(200, plans)

  let result = await client.listPlans()

  t.deepEqual(plans, result)
})

test('updatePlan', async t => {
  const client = t.context.client

  let id = uuid.v4()
  let response = {'statusMessage': 'Register updated', 'id': id}

  nock(options.endpoints.plans)
    .put(`/${id}`)
    .reply(204, response)

  let result = await client.updatePlan(id)

  t.deepEqual(response, result)
})

test('deletePlan', async t => {
  const client = t.context.client

  let id = uuid.v4()
  let response = {'statusMessage': 'Register deleted', 'id': id}

  nock(options.endpoints.plans)
    .delete(`/${id}`)
    .reply(204, response)

  let result = await client.deletePlan(id)

  t.deepEqual(response, result)
})

test('saveGymPayment', async t => {
  const client = t.context.client

  let gymPayment = fixtures.getGymPayment()

  nock(options.endpoints.plans)
    .post(`/payments`)
    .reply(201, gymPayment)

  let result = await client.saveGymPayment(gymPayment)

  t.deepEqual(gymPayment, result)
})

test('getPaymentsOfGym', async t => {
  const client = t.context.client

  let fkIdGym = uuid.v4()
  let gymPayments = fixtures.getGymPayments(3)

  nock(options.endpoints.plans)
    .get(`/payments/${fkIdGym}`)
    .reply(200, gymPayments)

  let result = await client.getPaymentsOfGym(fkIdGym)

  t.deepEqual(gymPayments, result)
})

test('listGymPayments', async t => {
  const client = t.context.client

  let gymPayments = fixtures.getGymPayments(3)

  nock(options.endpoints.plans)
    .get(`/payments/list`)
    .reply(200, gymPayments)

  let result = await client.listGymPayments()

  t.deepEqual(gymPayments, result)
})

// TEST PARA roles

test('saveRole', async t => {
  const client = t.context.client
  let role = fixtures.getRole()

  nock(options.endpoints.roles)
    .post('/', role)
    .reply(201, role)

  let result = await client.saveRole(role)
  t.deepEqual(result, role)
})

test('getRole', async t => {
  const client = t.context.client
  let role = fixtures.getRole()
  role._id = uuid.uuid()

  nock(options.endpoints.roles)
    .get(`/${role._id}`)
    .reply(200, role)

  let result = await client.getRole(role._id)

  t.deepEqual(result, role)
})

test('getPermissionsOfRole', async t => {
  const client = t.context.client
  const permissions = fixtures.getPermissionsOfRole()
  const _id = uuid.uuid()

  nock(options.endpoints.roles)
    .get(`/permissions/${_id}`)
    .reply(200, permissions)

  let result = await client.getPermissionsOfRole(_id)

  t.deepEqual(result, permissions)
})

test('listRoles', async t => {
  const client = t.context.client

  let roles = fixtures.getRoles(3)

  nock(options.endpoints.roles)
    .get('/list')
    .reply(200, roles)

  let result = await client.listRoles()

  t.deepEqual(roles, result)
})

test('updateRole', async t => {
  const client = t.context.client

  let id = uuid.v4()
  let response = {'statusMessage': 'Register updated', 'id': id}

  nock(options.endpoints.roles)
    .put(`/${id}`)
    .reply(204, response)

  let result = await client.updateRole(id)

  t.deepEqual(response, result)
})

test('deleteRole', async t => {
  const client = t.context.client

  let id = uuid.v4()
  let response = {'statusMessage': 'Register deleted', 'id': id}

  nock(options.endpoints.roles)
    .delete(`/${id}`)
    .reply(204, response)

  let result = await client.deleteRole(id)

  t.deepEqual(response, result)
})

// TESTS PARA USERS

test('saveUser', async t => {
  const client = t.context.client
  let user = fixtures.getUser()

  nock(options.endpoints.users)
    .post('/', user)
    .reply(201, user)

  let result = await client.saveUser(user)
  t.deepEqual(result, user)
})

test('getUser', async t => {
  const client = t.context.client
  let user = fixtures.getUser()
  user._id = uuid.uuid()

  nock(options.endpoints.users)
    .get(`/${user._id}`)
    .reply(200, user)

  let result = await client.getUser(user._id)

  t.deepEqual(result, user)
})

test('listUsers', async t => {
  const client = t.context.client

  let users = fixtures.getUsers(3)

  nock(options.endpoints.users)
    .get(`/list`)
    .reply(200, users)

  let result = await client.listUsers()

  t.deepEqual(result, users)
})

test('listUsersOfGym', async t => {
  const client = t.context.client

  const fkIdGym = uuid.v4()
  let users = fixtures.getUsers(3)

  nock(options.endpoints.users)
    .get(`/list/${fkIdGym}`)
    .reply(200, users)

  let result = await client.listUsersOfGym(fkIdGym)

  t.deepEqual(users, result)
})

test('updateUser', async t => {
  const client = t.context.client

  let id = uuid.v4()
  let response = {'statusMessage': 'Register updated', 'id': id}

  nock(options.endpoints.users)
    .put(`/${id}`)
    .reply(204, response)

  let result = await client.updateUser(id)

  t.deepEqual(response, result)
})

test('deleteUser', async t => {
  const client = t.context.client

  let id = uuid.v4()
  let response = {'statusMessage': 'Register deleted', 'id': id}

  nock(options.endpoints.users)
    .delete(`/${id}`)
    .reply(204, response)

  let result = await client.deleteUser(id)

  t.deepEqual(response, result)
})
