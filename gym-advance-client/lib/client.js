'use strict'

const request = require('request-promise')
const Promise = require('bluebird')

class Client {
  constructor (options) {
    this.options = options || {
      endpoints: {
        auth: 'http://gymadvance.test/auth',
        gyms: 'http://gymadvance.test/gym', // LISTO
        memberships: 'http://gymadvance.test/membership', // LISTO
        plans: 'http://gymadvance.test/plan',
        users: 'http://gymadvance.test/user'
      }
    }
  }

  // CLIENTE PARA AUTH

  authAdmin (username, password, callback) {
    let opts = {
      method: 'POST',
      uri: `${this.options.endpoints.auth}/admin`,
      body: {
        username,
        password
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  authUser (username, password, callback) {
    let opts = {
      method: 'POST',
      uri: `${this.options.endpoints.auth}/user`,
      body: {
        username,
        password
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  accessCustomer (documentNumber, callback) {
    let opts = {
      method: 'POST',
      uri: `${this.options.endpoints.auth}/access/${documentNumber}`,
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  getAccessRecordsByGym (token, userId, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.auth}/access/records`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  // CLIENTE PARA GYMS

  saveGym (token, userId, gym, callback) {
    let opts = {
      method: 'POST',
      uri: `${this.options.endpoints.gyms}/`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      body: gym,
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  getGym (token, userId, id, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.gyms}/${id}`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  getBasicStatistics (token, userId, fkIdGym, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.gyms}/basicstatistics/${fkIdGym}`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  listGyms (token, userId, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.gyms}/list`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  updateGym (token, userId, data, callback) {
    let opts = {
      method: 'PUT',
      uri: `${this.options.endpoints.gyms}/${data._id}`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      body: data,
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  deleteGym (token, userId, id, callback) {
    let opts = {
      method: 'DELETE',
      uri: `${this.options.endpoints.gyms}/${id}`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  saveEvent (token, userId, eventData, callback) {
    let opts = {
      method: 'POST',
      uri: `${this.options.endpoints.gyms}/event`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      body: eventData,
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  getEvent (token, userId, id, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.gyms}/events/${id}`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  getEventsOfGym (token, userId, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.gyms}/event`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  updateEvent (token, userId, data, callback) {
    let opts = {
      method: 'PUT',
      uri: `${this.options.endpoints.gyms}/events`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      body: data,
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  listEventCategories (token, userId, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.gyms}/categories/list`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  deleteEvent (token, userId, id, callback) {
    let opts = {
      method: 'DELETE',
      uri: `${this.options.endpoints.gyms}/events/${id}`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  // CLIENTE PARA MEMBERSHIPS

  saveMembership (token, userId, membership, callback) {
    let opts = {
      method: 'POST',
      uri: `${this.options.endpoints.memberships}/`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      body: membership,
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  getMembership (token, userId, id, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.memberships}/${id}`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  getMembershipsOfGym (token, userId, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.memberships}/list`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  updateMembership (token, userId, data, callback) {
    let opts = {
      method: 'PUT',
      uri: `${this.options.endpoints.memberships}/`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      body: data,
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  deleteMembership (token, userId, id, callback) {
    let opts = {
      method: 'DELETE',
      uri: `${this.options.endpoints.memberships}/${id}`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  saveUserPayment (token, userId, payment, callback) {
    let opts = {
      method: 'POST',
      uri: `${this.options.endpoints.memberships}/payments`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      body: payment,
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  getPaymentsOfUser (token, userId, fkIdUser, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.memberships}/payments/${fkIdUser}`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  getUsersPayment (token, userId, fkIdUsersPayment, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.memberships}/payments/${fkIdUsersPayment}`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  listUserPayments (token, userId, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.memberships}/payments/list`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  listAllUserPayments (token, userId, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.memberships}/payments/all`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  updateUsersPayment (token, userId, data, callback) {
    let opts = {
      method: 'PUT',
      uri: `${this.options.endpoints.memberships}/userspayment`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      body: data,
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  // CLIENTE PARA PLANS

  savePlan (token, userId, plan, callback) {
    let opts = {
      method: 'POST',
      uri: `${this.options.endpoints.plans}/`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      body: plan,
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  getPlan (token, userId, id, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.plans}/${id}`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  getPlansOfGym (token, userId, fkIdGym, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.plans}/list/${fkIdGym}`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  listPlans (token, userId, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.plans}/list`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  updatePlan (token, userId, data, callback) {
    let opts = {
      method: 'PUT',
      uri: `${this.options.endpoints.plans}/${data._id}`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      body: data,
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  deletePlan (token, userId, id, callback) {
    let opts = {
      method: 'DELETE',
      uri: `${this.options.endpoints.plans}/${id}`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  saveGymPayment (token, userId, payment, callback) {
    let opts = {
      method: 'POST',
      uri: `${this.options.endpoints.plans}/payments`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      body: payment,
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  getPaymentsOfGym (token, userId, fkIdUser, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.plans}/payments/${fkIdUser}`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  getGymsPayment (token, userId, id, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.plans}/gymspayment/${id}`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  updateGymsPayment (token, userId, data, callback) {
    let opts = {
      method: 'PUT',
      uri: `${this.options.endpoints.plans}/gymspayment`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      body: data,
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  listGymPayments (token, userId, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.plans}/payments/list`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  // CLIENTE PARA USERS

  saveUser (token, userId, user, callback) {
    let opts = {
      method: 'POST',
      uri: `${this.options.endpoints.users}/`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      body: user,
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  changePassword (token, userId, oldPasswordUser, newPasswordUser, callback) {
    let data = {
      oldPassword: oldPasswordUser,
      newPassword: newPasswordUser
    }

    let opts = {
      method: 'POST',
      uri: `${this.options.endpoints.users}/changePassword`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      body: data,
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  saveBodyMeasurements (token, userId, bodyMeasurements, callback) {
    let opts = {
      method: 'POST',
      uri: `${this.options.endpoints.users}/bodymeasurements`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      body: bodyMeasurements,
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  getUserById (token, userId, id, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.users}/${id}`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  getUserByUsername (username, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.users}/username/${username}`,
      // headers: {
      //   'Authorization': `Bearer ${token}`,
      //   'X-User-Id': userId
      // },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  listUsers (token, userId, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.users}/list`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  listUsersOfGym (token, userId, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.users}/list/users`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  listCustomersOfGym (token, userId, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.users}/list/customers`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  listInstructorsOfGym (token, userId, fkIdGym, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.users}/list/instructors/${fkIdGym}`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  listBodyMeasurementsOfUser (token, userId, fkIdUser, callback) {
    let opts = {
      method: 'GET',
      uri: `${this.options.endpoints.users}//bodymeasurements/${fkIdUser}`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  updateUser (token, userId, data, callback) {
    let opts = {
      method: 'PUT',
      uri: `${this.options.endpoints.users}/update`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      body: data,
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  updateMedicalConditions (token, userId, data, callback) {
    let opts = {
      method: 'PUT',
      uri: `${this.options.endpoints.users}/update/medicalconditions`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      body: data,
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  deleteBodyMeasurement (token, userId, id, callback) {
    let opts = {
      method: 'DELETE',
      uri: `${this.options.endpoints.users}/bodymeasurements/${id}`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }

  deleteUser (token, userId, id, callback) {
    let opts = {
      method: 'DELETE',
      uri: `${this.options.endpoints.users}/${id}`,
      headers: {
        'Authorization': `Bearer ${token}`,
        'X-User-Id': userId
      },
      json: true
    }

    return Promise.resolve(request(opts)).asCallback(callback)
  }
}

module.exports = Client
