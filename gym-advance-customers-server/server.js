const bodyParser = require('body-parser')
const config = require('./config')
const cookieParser = require('cookie-parser')
const cors = require('cors')
const express = require ('express')
const ext = require('file-extension')
const gymAdvance = require('gym-advance-client')
const multer = require('multer')
const port = process.env.PORT || 5050

const client = gymAdvance.createClient(config.client)

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, req.file + '.png')
  }
})

const upload = multer({ storage: storage }).single('urlPicture')

let app = express()

app.use(express.static('uploads'))

app.use(cors())
app.use(bodyParser.json()) // para obtener de las request informacion que venga en formato json
app.use(bodyParser.urlencoded({ extended: false })) // permite obtener los parametros que vienen desde un request desde un formulario
app.use(cookieParser()) // permite a express utilizar las cookies

// 01. GET: /api/v1/access -----> retorna todos los accesos al gimnasio
app.get('/api/v1/access', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  client.getAccessRecordsByGym(token, userId, function (err, response) {
    if (err) {
      if (err.statusCode === 401) {
        return res.status(401).send({error: 'unauthorized'})
      }
      return res.send(err.message)
    }
    res.send(response)
  })
})

// 02. GET: /api/v1/events -----> retorna todos los eventos de un gimnasio
app.get('/api/v1/events', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  client.getEventsOfGym(token, userId, function (err, events) {
    if (err) {
      if (err.statusCode === 401) {
        return res.status(401).send({error: 'unauthorized'})
      }
      return res.send([])
    }
    res.send(events)
  })
})

// 03. GET: /api/v1/events/categories -----> retorna todas las categorias de eventos
app.get('/api/v1/events/categories', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  client.listEventCategories(token, userId, function (err, categories) {
    if (err) {
      if (err.statusCode === 401) {
        return res.status(401).send({error: 'unauthorized'})
      }
      console.log(err)
      return res.send([])
    }
    res.send(categories)
  })
})

// 04. GET: /api/v1/events/:fkidevent  -----> retorna el evento solicitado en el ID
app.get('/api/v1/events/:fkidevent', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  var fkidevent = req.params.fkidevent

  client.getEvent(token, userId, fkidevent, function (err, response) {
    if (err) {
      console.log(err)
      if (err.statusCode === 401) return res.status(401).send({error: 'unauthorized'})
      return res.send([])
    }
    res.send(response)
  })
})

// 10. GET: /api/v1/gyms/basicstatistics/:fkidgym      retorna el gimnasio solicitado en el ID
app.get('/api/v1/gyms/basicstatistics/:fkidgym', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  var fkidgym = req.params.fkidgym

  client.getBasicStatistics(token, userId, fkidgym, function (err, response) {
    if (err) {
      if (err.statusCode === 401) return res.status(401).send({error: 'unauthorized'})
      console.log(err)
      return res.send({})
    }
    res.send(response)
  })
})

// 05. GET: /api/v1/memberships -----> retorna todas las membresias del gimnasio
app.get('/api/v1/memberships', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  client.getMembershipsOfGym(token, userId, function (err, memberships) {
    if (err) {
      if (err.statusCode === 401) {
        return res.status(401).send({error: 'unauthorized'})
      }
      console.log(err)
      return res.send([])
    }
    res.send(memberships)
  })
})

// 06. GET: /api/v1/memberships/:fkidmembership        retorna la membresia solititada en el ID
app.get('/api/v1/memberships/:fkidmembership', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  var fkidmembership = req.params.fkidmembership

  client.getMembership(token, userId, fkidmembership, function (err, response) {
    if (err) {
      if (err.statusCode === 401) return res.status(401).send({error: 'unauthorized'})
      console.log(err)
      return res.send({})
    }
    res.send(response)
  })
})

// 07. GET: /api/v1/plans/:fkidplan                    retorna el plan solicitado en el ID
app.get('/api/v1/plans/:fkidplan', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  let fkidplan = req.params.fkidplan

  client.getPlan(token, userId, fkidplan, function (err, plan) {
    if (err) {
      if (err.statusCode === 401) {
        return res.status(401).send({error: 'unauthorized'})
      }
      return res.send(err.message)
    }
    res.send(plan)
  })
})

// 09. GET: /api/v1/userpayments -----> retorna los pagos realizados a un gimnasio
app.get('/api/v1/userpayments', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  client.listUserPayments(token, userId, function (err, userPayments) {
    if (err) {
      if (err.statusCode === 401) {
        return res.status(401).send({error: 'unauthorized'})
      }
      console.log(err)
      return res.send([])
    }
    res.send(userPayments)
  })
})

// 10. GET: /api/v1/userpayments/:fkiduserpayment      retorna el pago solicitado en el ID
app.get('/api/v1/userpayments/:fkiduserpayment', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  var fkiduserpayment = req.params.fkiduserpayment

  client.getUsersPayment(token, userId, fkiduserpayment, function (err, response) {
    if (err) {
      if (err.statusCode === 401) return res.status(401).send({error: 'unauthorized'})
      console.log(err)
      return res.send({})
    }
    res.send(response)
  })
})

// 11. GET: /api/v1/users?type='role' -----> retorna todos los usuarios de un gimnasio
app.get('/api/v1/users', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  client.listUsersOfGym(token, userId, function (err, response) {
    if (err) {
      if (err.statusCode === 401) {
        return res.status(401).send({error: 'unauthorized'})
      }
      console.log(err)
      return res.send([])
    }
    res.send(response)
  })
})

// 12. GET: /api/v1/users/:fkiduser -----> retorna el usuario solicitado en el ID
app.get('/api/v1/users/:fkiduser', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  var fkiduser = req.params.fkiduser

  client.getUserById(token, userId, fkiduser, function (err, response) {
    if (err) {
      if (err.statusCode === 401) return res.status(401).send({error: 'unauthorized'})
      return res.send([])
    }
    res.send(response)
  })
})

// 12. GET: /api/v1/users/bodymeasurements/:fkiduser -----> retorna el usuario solicitado en el ID
app.get('/api/v1/users/bodymeasurements/:fkiduser', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  var fkIdUser = req.params.fkiduser

  client.listBodyMeasurementsOfUser(token, userId, fkIdUser, function (err, response) {
    if (err) {
      if (err.statusCode === 401) return res.status(401).send({error: 'unauthorized'})
      return res.send([])
    }
    res.send(response)
  })
})

// 13. POST: /api/v1/access/:documentnumber            agrega un registro de acceso al usuario del numero de documento
app.post('/api/v1/access/:documentnumber', function (req, res) {
  let documentnumber = req.params.documentnumber
  client.accessCustomer(documentnumber, function (err, response) {
    if (err) {
      return res.send(err.message)
    }
    res.send(response)
  })
})

// 14. POST: /api/v1/login  ----->  permite hacer login en el sistema
app.post('/api/v1/login', function(req, res, next) {
  var data = req.body
  client.authUser(data.username, data.password, (err, response) => {
    if (err) {
      return res.status(401).send(err)
    }
    res.send(response)
  })
})

// 15. POST: /api/v1/login/changepassword              permite cambiar la contraseña
app.post('/api/v1/login/changepassword', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  var data = req.body

  client.changePassword(token, userId, data.currentPasssword, data.newPassword, function (err, response) {
    if (err) {
      if (err.statusCode === 401) {
        return res.status(401).send(err)
      }
      console.log(err)
      return res.status(500).send(err)
    }

    res.send(response)
  })
})

// 16. POST: /api/v1/events
app.post('/api/v1/events', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  var eventData = req.body

  client.saveEvent(token, userId, eventData, function (err, response) {
    if (err) {
      console.log(err)
      if (err.statusCode === 401) return res.status(401).send({error: 'unauthorized'})

      let fields = []

      if (fields !== []) {
        return res.status(500).send(fields)
      }
      return res.status(500).send(err.message)
    }

    res.send(response)
  })
})

// 17. POST: /api/v1/memberships                       agrega una membresia al gimnasio
app.post('/api/v1/memberships', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  var membership = req.body

  client.saveMembership(token, userId, membership, function (err, response) {
    if (err) {
      console.log(err)
      if (err.statusCode === 401) return res.status(401).send({error: 'unauthorized'})

      let fields = []

      if (err.hasOwnProperty('response')) {
        if (err.response !== undefined && err.response.hasOwnProperty('body')) {
          if (err.response.body.errors.hasOwnProperty('dataName')) {
            fields.push('Nombre de la membresía')
          }
        }
      }

      if (fields !== []) {
        return res.status(500).send(fields)
      }
      return res.status(500).send(err.message)
    }

    res.send(response)
  })
})

// 18. POST: /api/v1/users/bodymeasurements                       agrega medidas a un usuario del gimnasio
app.post('/api/v1/users/bodymeasurements', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  var bodyMeasurements = req.body

  client.saveBodyMeasurements(token, userId, bodyMeasurements, function (err, response) {
    if (err) {
      if (err.statusCode === 401) return res.status(401).send({error: 'unauthorized'})
      return res.status(500).send(err.message)
    }

    res.send(response)
  })
})

// 18. POST: /api/v1/users                             agrega un usuario al gimnasio
app.post('/api/v1/users', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  var user = req.body

  client.saveUser(token, userId, user, function (err, response) {
    if (err) {
      if (err.statusCode === 401) return res.status(401).send({error: 'unauthorized'})

      let fields = []

      if (err.hasOwnProperty('response')) {
        if (err.response != undefined && err.response.hasOwnProperty('body')) {
          if (err.response.body.errors.hasOwnProperty('username')) {
            fields.push('Nombre de usuario')
          }

          if (err.response.body.errors.hasOwnProperty('documentNumber')) {
            fields.push('Numero de documento')
          }

          if (err.response.body.errors.hasOwnProperty('email')) {
            fields.push('Correo')
          }
        }
      }

      if (fields !== []) {
        return res.status(500).send(fields)
      }
      return res.status(500).send(err.message)
    }

    res.send(response)
  })
})

app.post('/api/v1/users/uploadpicture', function (req, res) {
  req.file = Date.now()
  upload(req, res, function (err) {
    if (err) {
      console.log(err)
      return res.status(status).send(body)
    }
    res.send(req.file.filename)
  })
})

// 19. POST: /api/v1/userpayments                      agrega un pago de cliente al gimnasio
app.post('/api/v1/userpayments', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  var userPayment = req.body

  client.saveUserPayment(token, userId, userPayment, function (err, response) {
    if (err) {
      console.log(err)
      if (err.statusCode === 401) return res.status(401).send({error: 'unauthorized'})

      let fields = []

      if (err.hasOwnProperty('response')) {
        if (err.response !== undefined && err.response.hasOwnProperty('body')) {
          if (err.response.body.errors.hasOwnProperty('reference')) {
            fields.push('Referencia')
          }
        }
      }

      if (fields !== []) {
        return res.status(500).send(fields)
      }
      return res.status(500).send(err.message)
    }

    res.send(response)
  })
})

// 20. PUT: /api/v1/events                  edita el evento con el ID correspondiente
app.put('/api/v1/events', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  var data = req.body

  client.updateEvent(token, userId, data, function (err, response) {
    if (err) {
      if (err.statusCode === 401) return res.status(401).send({error: 'unauthorized'})
      console.log(err)
      res.status(err.statusCode).send(err.message)
    }

    res.send(response)
  })
})

// 21. PUT: /api/v1/memberships        edita la membresia con el ID correspondiente
app.put('/api/v1/memberships', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  var data = req.body

  client.updateMembership(token, userId, data, function (err, response) {
    if (err) {
      if (err.statusCode === 401) return res.status(401).send({error: 'unauthorized'})
      console.log(err)
      res.status(err.statusCode).send(err.message)
    }

    res.send(response)
  })
})
// 22. PUT: /api/v1/users                    edita un usuario con el ID correspondiente
app.put('/api/v1/users', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  var data = req.body

  client.updateUser(token, userId, data, function (err, response) {
    if (err) {
      if (err.statusCode === 401) return res.status(401).send({error: 'unauthorized'})
      console.log(err)
      res.status(err.statusCode).send(err.message)
    }

    res.send(response)
  })
})

// 22. PUT: /api/v1/users                    edita un usuario con el ID correspondiente
app.put('/api/v1/users/medicalconditions', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  var data = req.body

  client.updateMedicalConditions(token, userId, data, function (err, response) {
    if (err) {
      if (err.statusCode === 401) return res.status(401).send({error: 'unauthorized'})
      console.log(err)
      res.status(err.statusCode).send(err.message)
    }

    res.send(response)
  })
})

// 23. PUT: /api/v1/userpayments      edita un pago con el ID correspondiente
app.put('/api/v1/userpayments', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  var data = req.body

  client.updateUsersPayment(token, userId, data, function (err, response) {
    if (err) {
      if (err.statusCode === 401) return res.status(401).send({error: 'unauthorized'})
      console.log(err)
      res.status(err.statusCode).send(err.message)
    }

    res.send(response)
  })
})

// 24. DELETE: /api/v1/events/:fkidevent               elimina un evento con el ID correspondiente
app.delete('/api/v1/events/:fkidevent', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  let fkidevent = req.params.fkidevent
  client.deleteEvent(token, userId, fkidevent, function (err, response) {
    if (err) {
      if (err.statusCode === 401) {
        return res.status(401).send({error: 'unauthorized'})
      }
      console.log(err)
      return res.send({})
    }
    res.status(204).send()
  })
})

// 25. DELETE: /api/v1/memberships/:fkidmembership     elimina una membresia con el ID correspondiente
app.delete('/api/v1/memberships/:fkidmembership', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  let fkidmembership = req.params.fkidmembership
  client.deleteMembership(token, userId, fkidmembership, function (err, response) {
    if (err) {
      if (err.statusCode === 401) {
        return res.status(401).send({error: 'unauthorized'})
      }
      console.log(err)
      return res.send({})
    }
    res.status(204).send()
  })
})

// 26. DELETE: /api/v1/users/:fkiduser                 elimina un usuario con el ID correspondiente
app.delete('/api/v1/users/:fkiduser', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  let fkiduser = req.params.fkiduser
  client.deleteUser(token, userId, fkiduser, function (err, response) {
    if (err) {
      if (err.statusCode === 401) {
        return res.status(401).send({error: 'unauthorized'})
      }
      return res.send({})
    }
    res.status(204).send()
  })
})

// 26. DELETE: /api/v1/users/:fkiduser                 elimina un usuario con el ID correspondiente
app.delete('/api/v1/users/bodymeasurements/:fkidbodymeasurement', function (req, res) {
  let token = req.get('Authorization')
  let userId = req.get('X-User-Id')
  let fkiIdBodyMeasurement = req.params.fkidbodymeasurement
  client.deleteBodyMeasurement(token, userId, fkiIdBodyMeasurement, function (err, response) {
    if (err) {
      if (err.statusCode === 401) {
        return res.status(401).send({error: 'unauthorized'})
      }
      return res.send({})
    }
    res.status(204).send()
  })
})

app.listen(port, function (err) {
  if (err) return console.log('Hubo un error'), process.exit(1)

  console.log(`Gym Advance escuchando en el puerto ${port}`)
})
