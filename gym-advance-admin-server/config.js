'use strict'

const config = {
  aws: {
    accessKey: process.env.AWS_ACCESS_KEY,
    secretKey: process.env.AWS_SECRET_KEY
  },
  client: {
    endpoints: {
      auth: 'http://api.gymadvance.com/auth',
      gyms: 'http://api.gymadvance.com/gym',
      memberships: 'http://api.gymadvance.com/membership',
      plans: 'http://api.gymadvance.com/plan',
      users: 'http://api.gymadvance.com/user'
    }
  },
  secret: process.env.GYM_ADVANCE_SECRET || 'GYM_ADVANCE_SECRET',
  url: 'http://gymadvance.com/'
}

if (process.env.NODE_ENV !== 'production') {
  config.client.endpoints = {
    auth: 'http://localhost:5000',
    gyms: 'http://localhost:5001', // LISTO
    memberships: 'http://localhost:5002', // LISTO
    plans: 'http://localhost:5003',
    users: 'http://localhost:5004'
  }

  config.url = 'http://localhost:5050/'
}

module.exports = config
